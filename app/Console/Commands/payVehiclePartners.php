<?php

namespace App\Console\Commands;

use App\customer;
use App\Mail\adminTransferNotification;
use App\withdrawal;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class payVehiclePartners extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pay:vehiclepartners';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pays vehicle partners their earnings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

    	$transfers = array();

    	$customers = customer::where('role','Vehicle Partner')->get();

	    $client = new Client([
		    // Base URI is used with relative requests
		    'base_uri' => 'https://api.paystack.co/',
		    // You can set any number of default request options.
		    'timeout'  => 30,
	    ]);

    	foreach($customers as $customer){
    		$accountName   = $customer->accountName;
    		$accountNumber = $customer->accountNumber;
    		$bankCode      = $customer->bankCode;
		    foreach($customer->Vehicles as $vehicle){

		    	if($vehicle->earnings > 500
			       && !empty($accountName)
			       && !empty($accountNumber)
			       && !empty($bankCode)
				   && $customer->dailyPayout == 1
			    ) { // perform checks before paying

				    $response = $client->request( 'POST', 'transferrecipient', [
					    'form_params' => [
						    'type'           => 'nuban',
						    'name'           => $accountName,
						    'descripton'     => 'Vehicle Partner',
						    'account_number' => $accountNumber,
						    'bank_code'      => $bankCode,
						    'currency'       => 'NGN',
					    ],
					    'headers'     => [
						    'Accept'        => 'application/json',
						    'Authorization' => "Bearer sk_test_b3eb18c646ae9226e2233192a6e5e23a85a5a001"
					    ]
				    ] );


				    $response      = json_decode( $response->getBody() );
				    $recepientCode = $response->data->recipient_code;

				    // create item for transfer that would be added to the bulk transfer that would be
				    // sent to paystack

				    $transferItem['amount']    = $vehicle->earnings * 100;
				    $transferItem['recipient'] = $recepientCode;
				    array_push( $transfers, $transferItem );

				    $withdrawal = new withdrawal();
				    $withdrawal->cid = $customer->cid;
				    $withdrawal->accountName = $accountName;
				    $withdrawal->accountNumber = $accountNumber;
				    $withdrawal->bankCode = $bankCode;
				    $withdrawal->amount = $vehicle->earnings;
				    $withdrawal->accountBank = $customer->accountBank;
				    $withdrawal->save(); // add the record to withdrawals

				    $vehicle->earnings = 0;
				    $vehicle->save(); // reset earnings

			    }

		    }
	    }

	    if(count($transfers) > 0 ) {

		    $response = $client->request( 'POST', 'transfer/bulk', [
			    'form_params' => [
				    'currency'  => 'NGN',
				    'source'    => 'balance',
				    'transfers' => $transfers
			    ],
			    'headers'     => [
				    'Accept'        => 'application/json',
				    'Authorization' => "Bearer sk_test_b3eb18c646ae9226e2233192a6e5e23a85a5a001"
			    ]
		    ] );

		    $response = json_decode( $response->getBody() );

		    if ( $response->status == true ) {
		    	echo $response->message;
			    Mail::to("theworldofmamchika@gmail.com")->send(new adminTransferNotification("Transfer Successful"));
			    Mail::to("theworldofmamchika@gmail.com")->send(new adminTransferNotification($response->message));
			    echo 1;
		    } else {
			    echo 0;

			    Mail::to("theworldofmamchika@gmail.com")->send(new adminTransferNotification("An error occured when trying the message - "
			                                                                         . $response->message));
		    }
	    } else {
    		echo "No transfers to be made";
    		Mail::to("theworldofmamchika@gmail.com")->send(new adminTransferNotification("No transfers to be made"));
    	}
    }
}
