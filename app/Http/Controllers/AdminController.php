<?php

namespace App\Http\Controllers;

use App\coupon;
use App\customer;
use App\deliveries;
use App\faq;
use App\item_category;
use App\Mail\incomingDeliveryNotification;
use App\Mail\newDeliveryErrorNotification;
use App\Mail\paymentReceipt;
use App\notifications;
use App\privacy;
use App\terms;
use App\User;
use App\vehicle;
use App\vehiclecateogry;
use App\vpas;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Bugsnag\Client;
use Bugsnag\Handler;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AdminController extends Controller
{


	public function __construct()
	{

		$this->middleware('auth');
		$this->middleware('admin');
	}


	public function viewStaff()
	{

		if(Input::has('term')){

			$term = Input::get('term');
			$by = Input::get('by');
			$staffs = User::where($by,'like',"%$term%")->get();
		} else {


			$staffs = User::all();

		}

		return view( 'staff.viewStaff',[
			'staffs' => $staffs] );
	}


	public function addCoupon() {
		return view('coupons.addCoupon');
	}

	public function manageCoupons() {
		$coupons = coupon::all();

		return view('coupons.manageCoupons',[
			'coupons' => $coupons
		]);
	}

	public function postAddCoupon( Request $request) {
		$days = $request->input('days');
		$maxUsages = $request->input('maxUsages');
		$value = $request->input('value');
		$couponCode = $request->input('code');

		$coupon = new coupon();
		$coupon->maxUsages = $maxUsages;
		$coupon->status = 'Available';
		$coupon->code = $couponCode;
		$coupon->value = $value;
		$coupon->uid = Auth::user()->uid;
		$coupon->expiryDate = Carbon::now()->addDays($days);
		$status = $coupon->save();

		if($status)
		    $request->session()->flash('success','Coupon Added');
		else
		    $request->session()->flash('error','Sorry an error occurred');

		return redirect('/add-coupon');
	}



	public function viewStaffDetail($uid)
	{
		$staff = User::find($uid);
		return view('staff.staffDetail',
			['staff' => $staff]);

	}

	public function editStaffDetail($uid)
	{
		$staff = User::find($uid);
		return view('staff.editStaffDetail',
			['staff' => $staff]);
	}

	public function updateStaff(Request $request, $uid)
	{
		$staff  =  User::find($uid);
		$status = $staff->update($request->all());
		if($request->hasFile('image')){
			$fileName = $request->file('image')->getClientOriginalName();
			$request->file('image')->move('profileImages',$fileName);
			$imageUrl = url('profileImages/' . $fileName);
			$staff->image = $imageUrl;
			$status = $staff->save();
		}

		if($status)
		    $request->session()->flash('success','Staff Updated');
		else
		    $request->session()->flash('error','Sorry an error occurred');

		return redirect('/staff/'. $uid . '/edit');
	}


	public function deleteStaff(Request $request, $uid)
	{
		$staff = User::find($uid);
		$status = $staff->delete();

		if($status)
		    $request->session()->flash('success','Staff Deleted.');
		else
		    $request->session()->flash('error','Sorry an error occurred');


		redirect('/register');
	}

	public function payVehiclePartners( Request $request) {
		echo "Dispatched";
		Artisan::queue('pay:vehiclepartners')->onQueue('transfers');

	    $request->session()->flash('success','Payments Initiated');

		return redirect('/');
	}

	public function terms() {
		$terms = terms::all()->last();
		return view('admin.terms',[
			'terms' => $terms
		]);
	}

	public function addFAQ() {
		return view('admin.addFAQ');
	}

	public function faqs() {
		$faqs = faq::all();
		return view('admin.faqs',[
			'faqs' => $faqs
		]);
	}

	public function vpas() {

		$vpas = vpas::all()->last();
		return view('admin.vpas',[
			'vpas' => $vpas
		]);
	}

	public function privacyPolicy() {
		$privacy = privacy::all()->last();
		return view('admin.privacy',[
			'privacy' => $privacy
		]);
	}

	public function postTerms( Request $request ) {
		try {

			$terms = new terms();
			$terms->content = $request->input('content');
			$terms->save();

			$request->session()->flash('success','Terms Updated');
			return redirect('terms');
		} catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			$request->session()->flash('error','Sorry something went wrong. Please try again.');
			return redirect('terms');
		}
	}

	public function postFaq( Request $request ) {
		try{
			$faq = new faq();
			$faq->question = $request->input('question');
			$faq->answer = $request->input('answer');
			$faq->save();

			$request->session()->flash('success','Faq Added');

			return redirect('faqs');
		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			$request->session()->flash('error','Sorry something went wrong. Please try again.');
			return redirect('faqs');
		}

	}

	public function editFaq( $fid ) {
		$faq = faq::find($fid);
		return view('admin.editFaq',[
			'faq' => $faq
		]);
	}

	public function postEditFaq( Request $request, $fid ) {
		try{
			$faq = faq::find($fid);
			$faq->question = $request->input('question');
			$faq->answer = $request->input('answer');
			$faq->save();

			$request->session()->flash('success','Faq Updated');

			return redirect('faqs');
		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			$request->session()->flash('error','Sorry something went wrong. Please try again.');
			return redirect('faqs');
		}

	}


	public function deleteFaq( Request $request, $fid ) {
		try{
			faq::destroy($fid);
			$request->session()->flash('success','FAQ Deleted');
			return redirect('faqs');
		} catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			$request->session()->flash('error','Sorry something went wrong. Please try again.');
			return redirect('faqs');
		}
	}

	public function postVpas( Request $request ) {
		try{
			$vpas = new vpas();
			$vpas->content = $request->input('content');
			$vpas->save();

			$request->session()->flash('success','Agreement Updated');
			return redirect('vpas');
		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			$request->session()->flash('error','Sorry something went wrong. Please try again.');
			return redirect('vpas');
		}

	}

	public function postPrivacyPolicy( Request $request ) {
		try{
			$privacy = new privacy();
			$privacy->content = $request->input('content');
			$privacy->save();

			$request->session()->flash('success','Policy Updated');
			return redirect('privacy');
		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			$request->session()->flash('error','Sorry something went wrong. Please try again.');
			return redirect('privacy');
		}

	}

	public function sendNotification() {
		return view('admin.sendNotification');
	}

//push	persist and redirect  
	public function postSendNotification( Request $request ) {
		$message = $request->input('message');

		$this->sendMessage($message);

		$notification = new notifications();
		$notification->message = $message;
		$notification->uid = Auth::user()->uid;
		$notification->save();
		$request->session()->flash('success','Message sent to all users.');

		return redirect('send-notification');
	}

	//persist
	public function sentNotifications() {
		$notifications = notifications::all()->reverse();
		return view('admin.notifications',[
			'notifications' => $notifications
		]);
	}

	function sendMessage($message){
		$content = array(
			"en" => $message
		);

		$fields = array(
			'app_id' => "9589c6fe-4df9-4fbd-9701-d85bc78ea105",
			'included_segments' => array('All'),
			'contents' => $content
		);

		$fields = json_encode($fields);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
			'Authorization: Basic OGE5OGRiMTYtYzMzZC00NmY5LThhMDYtNjUyYTg5MzE4NDQ0'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);

		return $response;
	}

	function sendSms($phone,$Message){

		/* Variables with the values to be sent. */
		$owneremail="tobennaa@gmail.com";
		$subacct="dropster";
		$subacctpwd="dropster";
		$sendto= $phone; /* destination number */
		$sender="DROPSTER"; /* sender id */

		$message= $Message;  /* message to be sent */

		/* create the required URL */
		$url = "http://www.smslive247.com/http/index.aspx?"  . "cmd=sendquickmsg"  . "&owneremail=" . UrlEncode($owneremail)
		       . "&subacct=" . UrlEncode($subacct)
		       . "&subacctpwd=" . UrlEncode($subacctpwd)
		       . "&message=" . UrlEncode($message)
		       . "&sender=" . UrlEncode($sender)
		       ."&sendto=" . UrlEncode($sendto)
		       ."&msgtype=0";


		/* call the URL */
		if ($f = @fopen($url, "r"))  {

			$answer = fgets($f, 255);

			if (substr($answer, 0, 1) == "+") {

				return 1;
			}
			else  {
				return 0;
			}
		}

		else  {  return 0;  }
	}

}
