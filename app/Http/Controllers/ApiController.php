<?php

namespace App\Http\Controllers;

use App\confirmation;
use App\coupon;
use App\couponUse;
use App\customer;
use App\customerLocations;
use App\deliveries;
use App\delivery_image;
use App\delivery_item;
use App\dropper;
use App\favorites;
use App\item_category;
use App\location;
use App\Mail\adminDeliveryNotification;
use App\Mail\adminTransferNotification;
use App\Mail\cardPaymentSuccessful;
use App\Mail\confirmEmail;
use App\Mail\incomingDeliveryNotification;
use App\Mail\newDeliveryErrorNotification;
use App\Mail\paymentReceipt;
use App\passwordReset;
use App\payment;
use App\priceupdate;
use App\setting;
use App\User;
use App\vehicle;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use SebastianBergmann\CodeCoverage\Report\BuilderTest;

class ApiController extends Controller
{

	public function __construct() {
	//	$this->middleware('apiauth');
	}


	public function signUp(Request $request) {

		$customerEmailCount = customer::where("email",$request->input('email'))->count();
		if($customerEmailCount > 0) return array('message' => "Email already exists");

		$customerPhoneCount = customer::where("phone",$request->input('phone'))->count();
		if($customerPhoneCount > 0) return array('message' => "Phone already exists");


		try{
			DB::transaction( function () {
				global $request;

				$signup = new customer();
				$signup->fname = $request->input('fname');
				$signup->sname = $request->input('sname');
				$signup->username = $request->input('username');
				$signup->password = bcrypt($request->input('password'));
				$signup->email = $request->input('email');

				$signup->image = url('/img/default-profile.png');
				$signup->gender = $request->input('gender');
				$signup->phone = $request->input('phone');
				$signup->role = 'Customer';
				$signup->referralCode = Str::random(6);

				$referredBy = $request->input('referredBy');


				if(isset($referredBy)){

					$signup->referredBy = $referredBy;

					$referralBonus = setting::where('name', 'referralBonus')->get()->last();

					$customer = customer::where('referralCode',$referredBy)->first();

					if($customer !== null){
						$customer->balance += $referralBonus;
						$customer->save(); // add referral bonus to customer
					}
				}

				$status = $signup->save();

				if ($status){
					echo 1;

					$code = Str::random('10');
					$confirmation = new confirmation();
					$confirmation->email = $signup->email;
					$confirmation->code = $code;
					$saved = $confirmation->save();

					// if($saved) Mail::to($signup->email)->send(new confirmEmail($code,$signup));

				} else {
					die('here');
					echo 0;
				}

			},2);

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			die($exception);

			return 0;
		}



	}


	public function socialSignUp(Request $request) {

		try{
			DB::transaction( function () {
				global $request;
				$image = $request->input('image');
				$signup = new customer();
				$signup->fname = $request->input('fname');
				$signup->sname = $request->input('sname');
				$signup->username = $request->input('username');
				$signup->password = $request->input('password');
				$signup->email = $request->input('email');


				if(isset($image))
				$signup->image = $image;
				else
				$signup->image = url('/img/default-profile.png');
				$signup->gender = $request->input('gender');
				$signup->phone = $request->input('phone');
				$signup->role = 'Customer';
				$signup->referralCode = Str::random(6);

				$referredBy = $request->input('referredBy');


				if(isset($referredBy)){

					$signup->referredBy = $referredBy;

					$referralBonus = setting::where('name', 'referralBonus')->get()->last();

					$customer = customer::where('referralCode',$referredBy)->first();
					$customer->balance += $referralBonus;
					$customer->save(); // add referral bonus to customer
				}

				$status = $signup->save();

				if ($status){
					echo 1;

					$code = Str::random('10');
					$confirmation = new confirmation();
					$confirmation->email = $signup->email;
					$confirmation->code = $code;
					$saved = $confirmation->save();

					if($saved) Mail::to($signup->email)->send(new confirmEmail($code,$signup));

				} else {
					echo 0;
				}

			},2);

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}



	}


	public function newDelivery(Request $request) {
		try {

			$cid           = $request->input( 'cid' );
			$paymentMethod = $request->input( 'paymentMethod' );
			$amount        = $request->input( 'amount' );
			$customer          = customer::find( $cid );

			if ( $paymentMethod == 'balance' ) {

				if($customer->balance < $amount)
					return response("Insufficient balance. Please add funds with your card or select cash as your payment method.",500);
			}

			$customer          = customer::find( $cid );

			if ($customer->balance < 0) {

				$amount += ( $customer->balance * - 1 );

				return response("You owe $amount naira. Please top up your balance before making a request or contact support",500);
			}

			DB::transaction( function () {
				global $request;

				$cid           = $request->input( 'cid' );
				$ditid         = $request->input( 'ditid' );
				$vcid          = $request->input( 'vcid' );
				$fromLat       = $request->input( 'fromLat' );
				$fromLng       = $request->input( 'fromLng' );
				$toLat         = $request->input( 'toLat' );
				$toLng         = $request->input( 'toLng' );
				$amount        = $request->input( 'amount' );
				$paymentMethod = $request->input( 'paymentMethod' );

				$delivery                = new deliveries();
				$delivery->from          = $request->input( 'from' );
				$delivery->fromAddress   = $request->input( 'fromAddress' );
				$delivery->to            = $request->input( 'to' );
				$delivery->toAddress     = $request->input( 'toAddress' );
				$delivery->phone         = $request->input( 'phone' );
				$delivery->fname         = $request->input( 'fname' );
				$delivery->sname         = $request->input( 'sname' );
				$delivery->email         = $request->input( 'email' );
				$delivery->companyName   = $request->input( 'companyName' );
				$delivery->description   = $request->input('description');
				$delivery->paymentMethod = $paymentMethod;
				$delivery->paymentStatus = 'unpaid';
				$delivery->deliveryType  = $request->input('deliveryType');


				$delivery->cid                = $cid;
				$delivery->ditid              = $ditid;
				$delivery->vcid               = $vcid;
				$delivery->status             = 'Available';
				$delivery->fromLng            = $fromLng;
				$delivery->fromLat            = $fromLat;
				$delivery->toLng              = $toLng;
				$delivery->toLat              = $toLat;
				$delivery->amount             = $amount;
				$delivery->isDropperConfirmed = 0;
				$delivery->isClientConfirmed  = 0;


				$status = $delivery->save();
//
//					$customer          = customer::find( $cid );
//					if ($customer->balance < 0){
//
//						$amount += ($customer->balance * -1 );
//
//						$delivery->amount = $amount;
//						$delivery->save();
//
//						$priceUpdate = new priceupdate();
//						$priceUpdate->did = $delivery->did;
//						$priceUpdate->description = "Bill you owe.";
//						$priceUpdate->uid = 1;
//						$priceUpdate->save();
//
//					}


					if ( $status && $paymentMethod == 'balance' ) {
					// deduct balance and mark delivery as paid

						$customer          = customer::find( $cid );
						$customer->balance = $customer->balance - $delivery->amount;
						$customer->save();
						$delivery->paymentStatus = 'paid';
						$delivery->save();

						Mail::to( $customer->email )->send( new paymentReceipt( $delivery ) );

						$senderName   = $delivery->Customer->fname . " " . $delivery->Customer->sname;
						$receiverName = $delivery->fname;




						$customer = customer::where( 'phone', $delivery->phone )->first();

						// send notification
						if ( empty( $customer ) ) {
							$this->sendSms( $delivery->phone, "Hi $receiverName, you have an incoming delivery from $senderName. It was sent using dropster. Download our app to track your delivery.\n Visit www.dropsterng.com" );
						} else {

							Mail::to( $customer->email )->send( new incomingDeliveryNotification( $delivery ) );
						}

				}

				echo $delivery->did;
			}, 5 );


		}  catch (\Exception $exception){
			$cid           = $request->input( 'cid' );
			$customer = customer::find($cid);
			Bugsnag::notifyException($exception);
			Mail::to("theworldofmamchika@gmail.com")->send(new newDeliveryErrorNotification($customer, $exception));
			return response("An error occurred while making your request. Please try again, if this persists, please contact support.",500);
		}
	}

	public function addDeliveryImage( Request $request ) {

		$did = $request->input('did');

		$image = $request->file('image');

		$inputFileName = Carbon::now()->timestamp . $image->getClientOriginalName();
		$image->move("uploads",$inputFileName);

		$image = new delivery_image();
		$image->url = url('uploads/' . $inputFileName);
		$image->did = $did;
	    $status = $image->save();

		if($status) echo 1; else echo 0;

	}


	public function paymentCollected( Request $request ) {

		try {

			DB::transaction( function () {

				global $request;

				$drid          = $request->input( 'drid' );
				$did           = $request->input( 'did' );
				$paymentMethod = $request->input( 'paymentMethod' );

				$delivery                = deliveries::find( $did );
				$delivery->paymentMethod = $paymentMethod;
				$delivery->paymentStatus = 'paid';
				$delivery->save();

				$dropper = dropper::find( $drid );

				if ( $paymentMethod == 'pos' ) {
					$dropper->unremittedPOS += $delivery->amount;
				} else if ( $paymentMethod == 'cash' ) {
					$dropper->unremittedCash += $delivery->amount;
				}

				$dropper->save();


				// when payment has been collected, increase vehicles earnings
				if ( $delivery->paymentStatus == 'paid' ) {

					$partnerPercent = setting::where( 'name', 'partnerPercent' )->get()->last();

					if($delivery->vcid  ==  2){ // if its a car no maintenance fee
						$adminPercent   = setting::where( 'name', 'adminPercent' )->get()->last()->value;
						$partnerPercent = 100 - $adminPercent;

						$vehicle            = vehicle::find( $delivery->Dropper->Vehicle->vid );
						$vehicle->earnings += $delivery->amount * ( $partnerPercent / 100 );
						$vehicle->save();

					} else {
						$vehicle            = vehicle::find( $delivery->Dropper->Vehicle->vid );
						$vehicle->earnings += $delivery->amount * ( $partnerPercent->value / 100 );
						$vehicle->save();

					}


				}
				// end

				$senderName   = $delivery->Customer->fname . " " . $delivery->Customer->sname;
				$receiverName = $delivery->fname;

				$customer = customer::where( 'phone', $delivery->phone )->first();


				// send notification
				if ( empty( $customer ) ) {
					$this->sendSms( $delivery->phone, "Hi $receiverName, you have an incoming delivery from $senderName. He sent it using dropster. Download our app to track your delivery.\n Visit www.dropsterng.com" );
				} else {

					Mail::to( $customer->email )->send( new incomingDeliveryNotification( $delivery ) );
				}

				Mail::to( $delivery->Customer->email )->send( new paymentReceipt( $delivery ) );

				echo 1;

			}, 2 );



		} catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return response("An error occurred",500);
		}


	}


	public function availableRequests( ) {
		$deliveries = deliveries::where('drid',null)->get()->sortByDesc('created_at');

		$deliveryArray = array();

		try{
			foreach($deliveries as $delivery){
				$delivery->Items;
				$delivery->VehicleCategory;
				array_push($deliveryArray,$delivery);
			}

		} catch (\Exception $exception){

		}


		return $deliveryArray;
	}

	public function calculatePrice(Request $request) {

		try{
			$fromLat = $request->input('fromLat');
			$fromLng = $request->input('fromLng');
			$toLat   = $request->input('toLat');
			$toLng   = $request->input('toLng');
			$vcid    = $request->input('vcid');

			$baseFare = setting::where('name','baseFare')->get()->last();

			if($vcid == 2){
				$costPerKm = setting::where('name','carCostPerKm')->get()->last();
			} else
			{
				$costPerKm = setting::where('name','costPerKm')->get()->last();
			}



			if(!empty($fromLng) &&
			   !empty($fromLat) &&
			   !empty($toLng) &&
			   !empty($toLat)
			){

				// check to ensure all requirements are met for the call before making it

				$url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=$fromLat,$fromLng&destinations=$toLat,$toLng&key=AIzaSyAdF15SNgcJieAOMWztw-W5LEOaPQV0NrM";

				$response = json_decode(file_get_contents($url));

				foreach($response->rows as $row){
					foreach($row->elements as $item){

						$price = $baseFare->value + ($costPerKm->value * ($item->distance->value / 1000)) ;

						$price = ceil($price / 10) * 10; // eliminate change issues. Round to nearest 10.

						return $price;
					}
				}

			}

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return response('An error occurred.',500);
		}

	}


	public function pickupDelivery( $did ) {
		try{
			$delivery = deliveries::find($did);

			$delivery->pickedUpAt = Carbon::now();
			$status = $delivery->save();
			if ($status) echo 1; else echo 0;

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}

	}



	public function dropoffDelivery( $did ) {

		try{
			$delivery = deliveries::find($did);
			$delivery->droppedOffAt = Carbon::now();
			$status = $delivery->save();

			$this->sendMessage("Delivery $did has been dropped off");

			if ($status) echo 1; else echo 0;
		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}

	}

	function sendMessage($message){
		$content = array(
			"en" => $message
		);

		$fields = array(
			'app_id' => "9589c6fe-4df9-4fbd-9701-d85bc78ea105",
			'included_segments' => array('All'),
			'contents' => $content
		);

		$fields = json_encode($fields);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
			'Authorization: Basic OGE5OGRiMTYtYzMzZC00NmY5LThhMDYtNjUyYTg5MzE4NDQ0'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);

		return $response;
	}

	public function returnDelivery( $did ) {

		try{
			$delivery = deliveries::find($did);
			$delivery->returnedAt = Carbon::now();
			$status = $delivery->save();

			if ($status) echo 1; else echo 0;
		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}

	}

	public function customers(Request $request) {
		$type = $request->input('type');
		$name = $request->input('data');

		if(isset($name)){
			$customers = customer::where($type, 'like', "%$name%" )->get();
		} else {
			$customers = customer::all();
		}

		$customers->makeHidden(['accountName',"address", "accountNumber", "bankCode","accountBank"]);

		return $customers;
	}


	//find a delivery and change its status to  Accepted

	public function acceptDelivery(Request $request) {

		try{

			DB::transaction( function () {

				global $request;
				$did = $request->input('did');
				$cid = $request->input('cid');

				$customer = customer::find($cid);
				$dropper = $customer->Dropper->drid;
				$vid = $customer->Dropper->Vehicle->vid;

				$delivery = deliveries::find($did);

				$delivery->vid =  $vid;
				$delivery->drid = $dropper;
				$delivery->status = 'Pending';

				$delivery->save();

				// once accepted, if paid, give earning to vehicle partner

				if ( $delivery->paymentStatus == 'paid' ) {

					$partnerPercent = setting::where( 'name', 'partnerPercent' )->get()->last();

					if($delivery->vcid  ==  2){ // if its a car no maintenance fee
						$adminPercent   = setting::where( 'name', 'adminPercent' )->get()->last()->value;
						$partnerPercent = 100 - $adminPercent;

						$vehicle            = vehicle::find( $delivery->Dropper->Vehicle->vid );
						$vehicle->earnings += $delivery->amount * ( $partnerPercent / 100 );
						$vehicle->save();

					} else {
						$vehicle            = vehicle::find( $delivery->Dropper->Vehicle->vid );
						$vehicle->earnings += $delivery->amount * ( $partnerPercent->value / 100 );
						$vehicle->save();

					}


				}

				// end

				echo 1;


			},3);

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}

	}

	public function socialSignIn( Request $request ) {
		try {
			$email = $request->input( 'email' );
			$type  = $request->input( 'type' );

			$customer = customer::where( 'email', $email )->first();

			if ( count( $customer ) <= 0 ) {
				return 0;
			}

			if ( $customer->password == $type ) {
				return $customer;
			} else {
				return 0;
			}
		} catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}
	}

	public function signIn( Request $request ) {
		$username = $request->input('username');
		$password = $request->input('password');

		$customer = customer::where('email',$username)->first();

		if(count($customer) > 0) {
			if ( password_verify( $password, $customer->password ) ) {

				return $customer;
			} else {
				return 0;
			}

		} else {
			$customer = customer::where('phone',$username)->first();

			if(count($customer) > 0) {
				if ( password_verify( $password, $customer->password ) ) {

					return $customer;
				} else {
					return 0;
				}

			} else {
				return 0;
			}
		}


	}



	public function deliveryCompleted($did) {

		$delivery = deliveries::find($did);
		$delivery->status = 'Complete';
		$status = $delivery->save();

		if ($status){

			echo 1;
		} else {
			echo 0;
		}
		
	}

	public function addFavorite( Request $request ) {

		try{
			$favorite = new favorites();
			$favorite->cid = $request->input('cid');
			$favorite->fname = $request->input('fname');
			$favorite->sname = $request->input('sname');
			$favorite->phone = $request->input('phone');
			$favorite->email = $request->input('email');
			$favorite->save();
			return 1;
		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}
	}

	public function editFavorite( Request $request ) {
		try{
			$cfid = $request->input('cfid');
			$favorite = favorites::find($cfid);
			$favorite->fname = $request->input('fname');
			$favorite->sname = $request->input('sname');
			$favorite->phone = $request->input('phone');
			$favorite->email = $request->input('email');
			$favorite->cid = $request->input('cid');
			$favorite->save();
			return 1;
		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}
	}

	public function deleteFavorite( Request $request ) {
		try{
			$cfid = $request->input('cfid');
			$favorite = favorites::find($cfid);
			$favorite->delete();
			return 1;
		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}
	}


	public function getCustomerLocations( $cid ) {
		try{
			$customerLocations = customerLocations::where('cid',$cid)->get();
			return $customerLocations;

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}
	}

	public function getFavorites( $cid ) {
		try{
			$favorites = favorites::where('cid',$cid)->get();
			return $favorites;
		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}
	}

	public function addCustomerLocation( Request $request ) {
		try{
			$customerLocation = new customerLocations();
			$customerLocation->cid = $request->input('cid');
			$customerLocation->name = $request->input('name');
			$customerLocation->lat = $request->input('lat');
			$customerLocation->lng = $request->input('lng');
			$customerLocation->save();
			return 1;
		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}
	}

	public function deliveryIssue($did) {

		$delivery = deliveries::find($did);
		$delivery->status = 'Issue';
		$status = $delivery->save();
		if ($status) echo 1; else echo 0;
	}

	//for returning the details about delivery to customer

	public function deliveryInProgress( Request $request,$did) {

		try{
			$delivery = deliveries::find($did);
			$delivery->Images;
			$delivery->Customer;
			$delivery->Dropper;
			$delivery->Dropper->Customer;
			$delivery->Dropper->Vehicle;
			return $delivery;
		} catch ( \Exception $exception){
			return 0;
		}

	}

	public function customerPayments( $cid ) {
		$payments = payment::where('cid',$cid)->get();

		try{
			foreach($payments as $payment){
				$payment->Customer;
			}
		}catch (\Exception $exception){}

		return $payments;
	}

	public function customerReceived( $cid) {

		$customer = customer::find($cid);
		$deliveries = deliveries::where('status',"<>",'Available')->where('phone', $customer->phone)->get()->sortByDesc('created_at');

		$deliveriesArray = array();

		foreach($deliveries as $delivery){
			try{
				$delivery->Dropper;
				$delivery->Images;
				$delivery->Dropper->Customer;
				$delivery->Dropper->Vehicle;
			} catch (\Exception $e){

			}
			array_push($deliveriesArray,$delivery);
		}

	    return $deliveriesArray;
	}

	public function customerSent( $cid  ) {

		$deliveries = deliveries::where('cid', $cid)->get()->sortByDesc('created_at');
		$deliveriesArray = array();

		foreach($deliveries as $delivery){
			try{
			$delivery->Dropper;
			$delivery->Images;
			$delivery->Dropper->Customer;
			$delivery->Dropper->Vehicle;
			array_push($deliveriesArray,$delivery);
			} catch (\Exception $e){
				array_push($deliveriesArray,$delivery);
			}
		}

		return $deliveriesArray;
	}

	public function deliveriesInProgress( $cid ) {
		$customer = customer::find($cid);
		$deliveries = deliveries::where('drid', $customer->Dropper->drid)->where('status','Pending')->get();
		foreach($deliveries as $item){
			$item->Dropper;
			$item->Customer;
			$item->Dropper->Customer;
			$item->Dropper->Vehicle;
			$item->Images;
		}

		return $deliveries;
	}

	public function deliveryItems() {
		$categories = item_category::all();
		foreach($categories as $category){
			$category->Items;
		}

		return $categories;
	}

	public function forgotPassword( Request $request ) {

		try{
			$email = $request->input('email');
			$customer = customer::where('email',$email)->get()->first();
			$code = Str::random(6);

			$passwordReset = new passwordReset();

			$passwordReset->email = $email;
			$passwordReset->token = $code;
			$status = $passwordReset->save();

			if($status) {

				Mail::to($email)->send(new \App\Mail\passwordReset($code,$customer));

				echo 1;
			} else echo 0;

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return response("An error occurred.",500);
		}

	}

	public function confirmVerification( Request $request ) {
		try{
			$email = $request->input('email');
			$confirmCode = $request->input('code');
			$newPassword = $request->input('newPassword');

			$resetData = passwordReset::where('email',$email)->get()->last();

			if($confirmCode == $resetData->token) {

				$customer = customer::where('email',$email)->first();
				$customer->password = bcrypt( $newPassword );
				$status             = $customer->save();

				passwordReset::destroy($resetData->prid);

				if ( $status ) {
					return 1;
				} else {
					return 0;
				}

			} else return 0;

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return response("An error occurred.",500);
		}
	}


	public function changeUserPassword( Request $request ) {

		try{
			$email = $request->input('email');
			$password = $request->input('password');
			$newPassword = $request->input('newPassword');

			$customer = customer::where('email',$email)->first();

			if(count($customer) > 0) {
				if ( password_verify( $password, $customer->password ) ) {

					$customer->password = bcrypt( $newPassword );
					$status             = $customer->save();

					if ( $status ) {
						return 1;
					} else {
						return 0;
					}

				} else {
					return 0;
				}

			}

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return response("An error occurred.",500);
		}
	}


	public function updateUserProfile( Request $request ) {
		try{
			$cid = $request->input('cid');
			$customer = customer::find($cid);
			$status = $customer->update($request->all());

			if($status) return 1; else return 0;
		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);

		}

	}

	public function dropperDeliveryHistory( $cid ) {
		$customer = customer::find($cid);
		$drid = $customer->Dropper->drid;
		$deliveries = deliveries::where('drid',$drid)->get();

		return $deliveries;
	}

	public function vehicleInfo($cid) {
		$customer = customer::find($cid);

		return $customer->Dropper->Vehicle;
	}

	public function postDropperLocation( Request $request ) {
		try{
			$cid = $request->input('cid');
			$lat = round($request->input('lat'),14);
			$lng = round($request->input('lng'),14);

			$customer = customer::find($cid);

			$location = new location();
			$location->lat = $lat;
			$location->lng = $lng;
			$location->drid = $customer->Dropper->drid;
			$status = $location->save();

			if($status) echo 1; else echo 0;

		}catch (\Exception $exception){
			return 0;
		}

	}

	public function getDropperLocation( $drid ) {

		try{
			$location = location::where('drid',$drid)->get()->last();

			$response['lat'] = $location->lat;
			$response['lng'] = $location->lng;

			return $response;

		} catch (\Exception $exception) { return 0; }

	}

	public function customersUpdatedDetails( $cid ) {
		try{

			$customer = customer::find($cid);
			if($customer->role == "Dropper") {
				$customer->Dropper;
			}
			return $customer;

		} catch (\Exception $exception) {
			Bugsnag::notifyException($exception);
			return 0;
		}
	}

	public function addCoupon( Request $request ) {

		try{
			return DB::transaction(function(){
				global $request;

				$cid = $request->input('cid');
				$couponCode = $request->input('couponCode');
				$customer = customer::find($cid);

				$coupon = coupon::where('code',$couponCode)->get()->first();


				if(empty($coupon)){ return array('message'=> "Invalid Coupon"); }
				else if(Carbon::createFromFormat("Y-m-d H:i:s",$coupon->expiryDate)->lessThan(Carbon::now()) ){
					return array('message'=> "Coupon has expired");
				}
				else {

					if($coupon->status == "Available"){

						if($coupon->maxUsages > count($coupon->Usages)){
							$usages = couponUse::where('cid',$cid)->get();
							foreach ($usages as $item){
								if($item->coupid == $coupon->coupid) return json_encode(array('message' => "You have already used this coupon")); // ensure its used only once by each user
							}

							$customer->balance += $coupon->value;
							$status = $customer->save();

							$couponUse = new couponUse();
							$couponUse->cid = $cid;
							$couponUse->coupid = $coupon->coupid;
							$couponUse->save();

							$payment         = new payment();
							$payment->amount = $coupon->value;
							$payment->method = 'coupon';
							$payment->cid    = $customer->cid;
							$payment->others = "";
							$payment->save();


							if($status) return 1; else return  0;
						}

					}

				}


			},3);

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}

	}

	public function updateBankDetails( Request $request ) {
		$cid = $request->input('cid');
		$customer = customer::find($cid);
		$customer->accountName = $request->input('accountName');
		$customer->accountNumber = $request->input('accountNumber');
		$customer->bankCode = $request->input('bankCode');
		$customer->accountBank = $request->input('accountBank');
		$status = $customer->save();

		if($status) echo 1; else echo 0;
	}

	public function handleCardPayment( Request $request ) {

		try {
			DB::transaction( function () {

				global $request;

				$response = json_decode( json_encode( $request->all() ) );

				if ( $response->event == "charge.success" ) {

					$data = $response->data;

					$email     = $data->customer->email;
					$amount    = $data->amount / 100;
					$reference = $data->reference;

					$customer = customer::where( 'email', $email )->first();

					$previousPayment = payment::where( 'cid', $customer->cid )->get()->last();

					if ( isset( $previousPayment ) ) {
						if ( $previousPayment->others != $reference ) { // check if user has been credited for transaction already

							$customer->balance += $amount;
							$customer->save();

							$payment         = new payment();
							$payment->amount = $amount;
							$payment->method = 'card';
							$payment->cid    = $customer->cid;
							$payment->others = $reference;
							$payment->save();
							Mail::to( $email )->send( new cardPaymentSuccessful( $data, $customer ) );


						} // end previous payment check

					} else {

						$customer->balance += $amount;
						$customer->save();

						$payment         = new payment();
						$payment->amount = $amount;
						$payment->method = 'card';
						$payment->cid    = $customer->cid;
						$payment->others = $reference;
						$payment->save();

						Mail::to( $email )->send( new cardPaymentSuccessful( $data, $customer ) );

					}
				}

				return http_response_code( 200 );

			}, 3 );

		} catch ( \Exception $exception ) {
			Bugsnag::notifyException( $exception );
			return response("An error occurred.",500);
		}

	}

	public function changeProfilePhoto( Request $request ) {

		try{
			$cid = $request->input('cid');
			$image = $request->file('image');

			$inputFileName = Carbon::now()->timestamp . $image->getClientOriginalName();
			$image->move("uploads",$inputFileName);

			$customer = customer::find($cid);

			$customer->image = url('uploads/' . $inputFileName);
			$status = $customer->save();

			if($status) echo 1; else echo 0;

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}


	}

	public function cancelDelivery( Request $request ) {

		try{
			$did = $request->input('did');
			$delivery = deliveries::find($did);

			if ($delivery->status == 'Cancelled') return 2;

			if($delivery->status == "Available"){

				$delivery->status = 'Cancelled';
				$delivery->save();

			} else if( $delivery->created_at > Carbon::now()->subMinutes(setting::where('name', 'cancelMinutes')->get()->last()->value) ) {

				//bill the customer if the customer is cancelling late

				$customer = customer::find($delivery->Customer->cid);
				$customer->balance -= setting::where('name', 'cancellationFee')->get()->last()->value;
				$customer->save();

				$delivery->status = 'Cancelled';
				$delivery->save();

			} else {

				$delivery->status = 'Cancelled';
				$delivery->save();

			}

			return 1;

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}
	}


	function sendSms($phone,$Message){

		/* Variables with the values to be sent. */
		$owneremail="tobennaa@gmail.com";
		$subacct="dropster";
		$subacctpwd="dropster";
		$sendto= $phone; /* destination number */
		$sender="DROPSTER"; /* sender id */

		$message= $Message;  /* message to be sent */

		/* create the required URL */
		$url = "http://www.smslive247.com/http/index.aspx?"  . "cmd=sendquickmsg"  . "&owneremail=" . UrlEncode($owneremail)
		       . "&subacct=" . UrlEncode($subacct)
		       . "&subacctpwd=" . UrlEncode($subacctpwd)
		       . "&message=" . UrlEncode($message)
		       . "&sender=" . UrlEncode($sender)
		       ."&sendto=" . UrlEncode($sendto)
		       ."&msgtype=0";


		/* call the URL */
		if ($f = @fopen($url, "r"))  {

			$answer = fgets($f, 255);

			if (substr($answer, 0, 1) == "+") {

				return 1;
			}
			else  {
				return 0;
			}
		}

		else  {  return 0;  }
	}


	public function earnings($cid) {
		$customer = customer::find($cid);
		$customer->Vehicles;
		return $customer;
	}

	public function withdrawals( $cid ) {
		$customer = customer::find($cid);
		$customer->Withdrawals;
		return $customer;
	}

	public function referred( $cid ) {
		$customer = customer::find($cid);
		$referred = customer::where('referredBy',$customer->referralCode)->get();
		return $referred;
	}

	public function changePayout( Request $request ) {
		$cid = $request->input('cid');
		$dailyPayout = $request->input('dailyPayout');
		$customer = customer::find($cid);

		if($dailyPayout == true){
			$customer->dailyPayout = 1;
			$status = $customer->save();
			if($status) return 1; else return 0;
		} else {
			$customer->dailyPayout = 0;
			$status = $customer->save();
			if($status) return 1; else return 0;
		}

	}

	public function settings( $cid ) {
		$response = array();
		$customer = customer::find($cid);
		$response['dailyPayout'] = $customer->dailyPayout;

		return $response;
	}

	public function sendPhoneVerification( Request $request ) {

		try{
		$phone = $request->input('phone');

		$code = self::random_str(6);
		
		$confirmation = new confirmation();
		$confirmation->email = $phone;
		$confirmation->code = $code;
		$confirmation->save();

		$this->sendSms($phone,"Your phone verification code is " .$code );

		return 1;
		}catch(\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}

	}

	public function confirmPhoneVerification( Request $request ) {
		try{
		$phone = $request->input('phone');
		$code = $request->input('code');

		$confirmation = confirmation::where('email',$phone)->get()->last();

		if($confirmation->code == $code) return 1; else return 0;

		}catch(\Exception $exception){
			Bugsnag::notifyException($exception);
			return 0;
		}

	}

	static  function random_str($length, $keyspace = '0123456789')
	{
		$str = '';
		$max = mb_strlen($keyspace, '8bit') - 1;
		for ($i = 0; $i < $length; ++$i) {
			$str .= $keyspace[random_int(0, $max)];
		}
		return $str;
	}

}
