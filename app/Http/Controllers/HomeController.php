<?php

namespace App\Http\Controllers;

use App\customer;
use App\deliveries;
use App\dropper;
use App\User;
use App\vehicle;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{

    public function __construct()
    {
	    $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

	    if(Input::has('by')) {

		    $by      = Input::get( 'by' );

		    $recentdelivery = deliveries::where('did', 'like', "%$by%" )->paginate(5);
	    }else{
		    $recentdelivery = deliveries::latest()->paginate(5);

	    }


    	$deliveries = deliveries::where('status', 'Complete')->get();
    	$inprogress = deliveries::where('status', 'Pending')->get();
    	$issues = deliveries::where('status', 'Issue')->get();
        $available = deliveries::where('status', 'Available')->get();
    	$recentissues = deliveries::where('status','Issue')->paginate(5);


	    $customers = customer::where('role', 'Customer')->get();
	    $partners = customer::where('role', 'Partner')->get();
	    $vehicles = vehicle::all();
	    $droppers = dropper::all();

	    $allDeliveries = deliveries::all();
	    $allRealDeliveries = deliveries::where('status','<>','Cancelled')->get();

	    $allTransactionsValue = 0;
	    $todaysTransactionsValue = 0;

	    foreach($allRealDeliveries as $item){
	    	$allTransactionsValue += $item->amount;

	    	if($item->created_at >= Carbon::today()){
	    		$todaysTransactionsValue += $item->amount;
		    }
	    }




        return view('welcome',[

        	'deliveries' => $deliveries,
            'available' => $available,
	        'inprogress' => $inprogress,
	        'droppers' => $droppers,
	        'vehicles' => $vehicles,
	        'issues' => $issues,
	        'customers' => $customers,
            'partners' => $partners,
	        'recentdeliveries' => $recentdelivery,
	        'recentissues' => $recentissues,
	        'allTransactionsValue' => $allTransactionsValue,
	        'todaysTransactionsValue' => $todaysTransactionsValue
        ]);
    }

	public function getStatus() {

		$deliveries = deliveries::where('status', 'Complete')->count();
		$inprogress = deliveries::where('status', 'Pending')->count();
		$issues = deliveries::where('status', 'Issue')->count();
		$available = deliveries::where('status', 'Available')->count();

		$customers = customer::where('role', 'Customer')->count();
		$partners = customer::where('role', 'Partner')->count();
		$vehicles = vehicle::all()->count();
		$droppers = dropper::all()->count();

		$allRealDeliveries = deliveries::where('status','<>','Cancelled')->get();

		$allTransactionsValue = 0;
		$todaysTransactionsValue = 0;

		foreach($allRealDeliveries as $item){
			$allTransactionsValue += $item->amount;

			if($item->created_at >= Carbon::today()){
				$todaysTransactionsValue += $item->amount;
			}
		}


		return [

			'deliveries' => $deliveries,
			'available' => $available,
			'inprogress' => $inprogress,
			'droppers' => $droppers,
			'vehicles' => $vehicles,
			'issues' => $issues,
			'customers' => $customers,
			'partners' => $partners,
			'allTransactionsValue' => $allTransactionsValue,
			'todaysTransactionsValue' => $todaysTransactionsValue
		];
	}


	public function logout() {
		Auth::logout();
		return redirect('/');
    }


}