<?php

namespace App\Http\Controllers;

use App\confirmation;
use App\customer;
use App\faq;
use App\privacy;
use App\terms;
use App\vpas;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PublicController extends Controller
{

	public function confirmEmail( $code , $email ) {

		$confirmation = confirmation::where('email',$email)->first();

		if($code == $confirmation->code) {
			$customer = customer::where('email',$email)->first();
			$customer->isEmailConfirmed = 1;
			$customer->save();
		} else {
			return redirect('http://dropster.com.ng');
		}

		return view('emailConfirmationPage');
	}

	public function activeTerms() {
		try{
			if(terms::all()->count() > 0)
				return terms::all()->last();
			else {
				$data =	[
					'content' => 'Will be available Shortly',
					'created_at' => Carbon::now()->toDateTimeString()
				];
				return json_encode($data);
			}
		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);

			$data =	[
				'content' => 'Will be available Shortly',
				'created_at' => Carbon::now()->toDateTimeString()
			];
			return json_encode($data);
		}

	}

	public function activePrivacy() {
		try{
			if(privacy::all()->count() > 0)
				return privacy::all()->last();
			else
			{
				$data =	[
					'content' => 'Will be available Shortly',
				    'created_at' => Carbon::now()->toDateTimeString()
				];
				return json_encode($data);
			}

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			$data =	[
				'content' => 'Will be available Shortly',
				'created_at' => Carbon::now()->toDateTimeString()
			];
			return json_encode($data);
		}

	}

	public function activeVpas() {
		try{
			if(vpas::all()->count() > 0)
				return vpas::all()->last();
			else
			{
				$data =	[
					'content' => 'Will be available Shortly',
					'created_at' => Carbon::now()->toDateTimeString()
				];
				return json_encode($data);
			}
		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);

			$data =	[
				'content' => 'Will be available Shortly',
				'created_at' => Carbon::now()->toDateTimeString()
			];
			return json_encode($data);
		}

	}


	public function activeFaqs() {
		try{
			if(faq::all()->count() > 0)
				return faq::all();
			else
			{
				$data = array([
					'question' => 'Will be available Shortly',
					'answer' => 'Will be available Shortly',
					'created_at' => Carbon::now()->toDateTimeString()
				]);
				return json_encode($data);
			}
		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);

			$data = array([
				'question' => 'Will be available Shortly',
				'answer' => 'Will be available Shortly',
				'created_at' => Carbon::now()->toDateTimeString()
			]);
			return json_encode($data);
		}

	}


}
