<?php

namespace App\Http\Controllers;

use App\customer;
use App\customerBalanceUpdate;
use App\deliveries;
use App\delivery_item;
use App\dropper;

use App\fuel;
use App\group;
use App\group_member;
use App\item_category;
use App\Mail\incomingDeliveryNotification;
use App\Mail\newDeliveryErrorNotification;
use App\Mail\paymentReceipt;
use App\maintenance;
use App\priceupdate;
use App\remittance;
use App\setting;
use App\update;
use App\User;
use App\vehicle;
use App\vehicle_assignment_histories;
use App\vehiclecateogry;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class StaffController extends Controller {


	public function __construct() {
		$this->middleware( 'auth' );
	}

	public function index() {

	}

	public function getLookupCustomers() {

		if(Input::has('time')){
			$time = Input::get('time');
			$customers = customer::where( 'role', 'Customer' )->where('created_at','>=', Carbon::now()->subDays($time))->get()->sortByDesc('created_at');
		} else {
			$customers = customer::where( 'role', 'Customer' )->get()->sortByDesc('created_at');
		}

		return view( 'staff.lookupCustomer', [
			'customers' => $customers
		] );

	}

	public function getSupportPortal() {
		return view( 'staff.support' );
	}


	public function getSelectRegisterDropper( Request $request ) {
		$customers = customer::where( 'role', 'Customer' )->get();

		return view( 'dropper.selectRegisterDropper', [
			'customers' => $customers
		] );
	}

	public function getRegisterDropper( Request $request, $cid ) {

		$customer = customer::find( $cid );
		$vehicles = vehicle::all();

		return view( 'dropper.makeDropper', [

			'customer' => $customer,
			'vehicles' => $vehicles

		] );

	}


	public function getSelectAddGroupVehicle() {
		$groups = group::all();

		return view( 'vehiclepartner.selectAddGroupVehicle', [
			'groups' => $groups
		] );

	}

	public function getAddGroupVehicle( $gid ) {
		$group = group::find( $gid );
		$vcs   = vehiclecateogry::all();

		return view( 'vehiclepartner.addGroupVehicle', [
			'group' => $group,
			'vcs'   => $vcs
		] );
	}

	public function getLookupDroppers() {
		$droppers = customer::where( 'role', 'Dropper' )->get();

		return view( 'dropper.lookupDroppers', [
			'droppers' => $droppers
		] );
	}

	public function getSelectAddVehicle() {
		$customers = customer::where( 'role', 'Customer' )->get();

		return view( 'vehiclepartner.selectAddVehicle', [
			'customers' => $customers
		] );
	}

	public function getCustomerDetails( Request $request, $cid ) {

		$customer = customer::find( $cid );

		return view( 'customer.customerDetails', [
			'customer' => $customer
		] );
	}

	public function getCustomerReceived( Request $request, $cid ) {
		$customer = customer::find( $cid );
		$received = deliveries::where( 'phone', $customer->phone )->get();

		return view( 'customer.customerReceived', [
			'received' => $received
		] );

	}


	public function getCustomerSent( $cid ) {

		$sent = deliveries::where( 'cid', $cid )->get();

		return $sent;
	}


	public function getDeliveryDetail( $did ) {

		$delivery = deliveries::find( $did );
		$droppers = dropper::where('vid', "<>", null)->get();
		$updates = update::where('did', $did)->orderBy('created_at','desc')->get();


		return view( 'customer.deliveryDetails', [
			'delivery' => $delivery,
			'droppers' => $droppers,
			'updates' => $updates
		] );
	}

	public function priceUpdate( Request $request ) {
		$did = $request->input('did');
		$amount = $request->input('amount');
		$description = $request->input('description');

		$priceUpdate = new priceupdate();
		$priceUpdate->uid = Auth::user()->uid;
		$priceUpdate->did = $did;
		$priceUpdate->amount = $amount;
		$priceUpdate->description = $description;
		$priceUpdate->save();

		$delivery = deliveries::find($did);
		$delivery->amount = $delivery->amount + $amount;
		$delivery->save();

		$request->session()->flash('success','Price Updated.');

		return redirect('view-delivery-detail/' . $did);
	}

	public function customerRequest() {
		$customers =  customer::where('role','customer')->get();

		$vehicles = vehicle::all();

		$vcat    = vehiclecateogry::all();
		$itemCat = item_category::all();

		return view('customer.requestForCustomer',[
			'customers' => $customers,
			'vehicles' => $vehicles,
			'vcs'    => $vcat,
			'itemCats' => $itemCat


		]);
	}


	public function assignDelivery( Request $request ) {
		$did = $request->input('did');
		$drid = $request->input('drid');

		try{
			$dropper = dropper::find($drid);

			$delivery = deliveries::find($did);
			$delivery->status = "Pending";
			$delivery->vid = $dropper->Vehicle->vid;
			$delivery->drid = $drid;
			$delivery->save();

			// once accepted, if paid, give earning to vehicle partner

			if($delivery->paymentStatus == 'paid'){
				$partnerPercent = setting::where('name', 'partnerPercent')->get()->last();

				if($delivery->vcid == 2){ // if its a car no maintenance fee
					$adminPercent = setting::where( 'name', 'adminPercent' )->get()->last()->value;
					$partnerPercent = 100 - $adminPercent;
				}


				$vehicle = vehicle::find($delivery->Dropper->Vehicle->vid);
				$vehicle->earnings += $delivery->amount * ($partnerPercent->value / 100);
				$vehicle->save();
			}
			// end

			$request->session()->flash('success','Delivery Assigned');

			return redirect('view-delivery-detail/' . $did);

		}catch (\Exception $exception){
			Bugsnag::notifyException($exception);
			$request->session()->flash('error','Sorry an error occurred. Please try again.');
			return redirect('view-delivery-detail/' . $did);

		}

	}

	public function getDropperDetails( $cid ) {

		$dropper = customer::find( $cid );

		if ( isset( $dropper->Dropper->Deliveries ) ) {
			$deliveries = $dropper->Dropper->Deliveries;
			$drid       = $dropper->Dropper->drid;
			$history    = vehicle_assignment_histories::where( 'drid', $drid )->get();

		} else {
			$deliveries = [];
			$history    = [];
		}


		$remittances = remittance::where('drid',$dropper->Dropper->drid)->get()->sortByDesc('created_at');
		$assignedVehicles = array();

		$droppers = dropper::all();

		foreach ( $droppers as $item ) {
			if ( isset( $item->vid ) ) {
				array_push( $assignedVehicles, $item->vid );
			}
		}

		$vehicles = vehicle::whereNotIn( 'vid', $assignedVehicles )->get();


		return view( 'dropper.dropperDetails', [
			'customer'   => $dropper,
			'vehicles'   => $vehicles,
			'history'    => $history,
			'deliveries' => $deliveries,
			'remittances' => $remittances
		] );

	}

	public function getViewVehicles( Request $request ) {
		$vehicles = vehicle::all();

		return view( 'vehicle.viewVehicles', [
			'vehicles' => $vehicles
		] );

	}


	public function getVehicleDetails( Request $request, $vid ) {

		$vehicle = vehicle::find( $vid );

		return view( 'vehicle.vehicleDetails', [
			'vehicle' => $vehicle,

		] );
	}


	public function getAddVehicle( $cid ) {
		$customer = customer::find( $cid );
		$vcs      = vehiclecateogry::all();

		return view( 'vehiclepartner.addVehicle',
			[ 'customer' => $customer, 'vcs' => $vcs ] );

	}


	public function getLookupVehiclePartners() {
		$vehiclePartners = customer::where( 'role', 'Vehicle Partner' )->get();


		return view( 'vehiclepartner.lookupVehiclePartners', [
			'vehiclePartners' => $vehiclePartners
		] );
	}

	public function vehiclePartnerDetails( $cid ) {
		$customer = customer::find( $cid );
		$earnings = 0;

		foreach ( $customer->Vehicles as $vehicle ) {
			$earnings += $vehicle->earnings;
		}

		return view( 'vehiclepartner.vehiclePartnerDetails', [
			'customer' => $customer,
			'earnings' => $earnings
		] );
	}

	public function getAddVehicleCat() {
		return view( 'staff.addVehicleCat' );
	}


	public function getManageGroup() {
		$groups = group::all();


		return view( 'vehiclepartner.manageGroup', [
			'groups' => $groups
		] );
	}

	public function getManageSingleGroup( Request $request, $gid ) {
		$group = group::find( $gid );

		return view( 'vehiclepartner.manageSingleGroup', [
			'group' => $group
		] );
	}

	public function getCreateGroup() {
		$categories = vehiclecateogry::all();

		return view( 'vehiclepartner.createGroup', [
			'categories' => $categories
		] );
	}

	public function postAddGroupVehicle( Request $request ) {

		$gid            = $request->input( 'gid' );
		$vehicle        = new vehicle();
		$vehicle->vcid  = $request->input( 'vcid' );
		$vehicle->gid   = $gid;
		$vehicle->cid   = null;
		$vehicle->type  = 'Group';
		$vehicle->model = $request->input( 'model' );
		$vehicle->brand = $request->input( 'brand' );
		$vehicle->color = $request->input( 'color' );
		$vehicle->regno = $request->input( 'regno' );
		$status         = $vehicle->save();

		$group = group::find( $gid );

		foreach ( $group->Members as $item ) {
			$customer       = $item->Customer;
			$customer->role = "Vehicle Partner";
			$customer->save();

		}


		if ( $status ) {
			$request->session()->flash( 'success', 'Vehicle Added.' );
		} else {
			$request->session()->flash( 'error', 'Sorry an error occurred' );
		}

		return redirect( 'add-group-vehicle/' . $gid );

	}


	public function postAddGroupMember( Request $request ) {
		$email  = $request->input( 'email' );
		$gid    = $request->input( 'gid' );
		$amount = $request->input( 'amount' );

		$group    = group::find( $gid );
		$customer = customer::where( 'email', $email )->first();

		$percent = ( $amount / $group->total ) * 100;

		$groupMember          = new group_member();
		$groupMember->gid     = $gid;
		$groupMember->cid     = $customer->cid;
		$groupMember->percent = $percent;
		$status               = $groupMember->save();

		if ( $status ) {
			$request->session()->flash( 'success', 'Member Added' );
		} else {
			$request->session()->flash( 'success', 'Member Added' );
		}

		return redirect( 'manage-group/' . $gid );
	}

	public function postDropper( Request $request ) {
		$cid     = $request->input( 'cid' );
		$dropper = dropper::where( 'cid', $cid )->first();


		if ( isset( $dropper->vid ) ) {   // save current assignment to history
			$history       = new vehicle_assignment_histories();
			$history->vid  = $dropper->vid;
			$history->drid = $dropper->drid;
			$history->save();

		}

		$dropper->vid = $request->input( 'vid' );

		$status = $dropper->save();

		if ( $status ) {
			$request->session()->flash( 'success', 'Vehicle Assigned.' );
		} else {
			$request->session()->flash( 'error', 'Sorry an error occurred' );
		}

		return redirect( 'dropper/' . $cid );
	}

	public function postMakeDropper( Request $request, $cid ) {

		$dropper      = new dropper();
		$dropper->cid = $cid;
		$status       = $dropper->save();

		$customer       = customer::find( $cid );
		$customer->role = 'Dropper';
		$customer->save();

		if ( $status ) {
			$request->session()->flash( 'success', 'Dropper Registered.' );
		} else {
			$request->session()->flash( 'error', 'Sorry an error occurred' );
		}

		return redirect( 'register-dropper' );


	}


	public function postCreateGroup( Request $request ) {

		$group        = new group();
		$group->name  = $request->input( 'name' );
		$group->vcid  = $request->input( 'vcid' );
		$group->total = $request->input( 'total' );
		$group->save();

		$request->session()->flash( 'success', 'Group Created' );

		return redirect( 'create-group' );
	}


	public function postVehicle( Request $request ) {

		$cid = $request->input( 'cid' );

		$vehicle        = new vehicle();
		$vehicle->vcid  = $request->input( 'vcid' );
		$vehicle->cid   = $cid;
		$vehicle->gid   = null;
		$vehicle->type  = 'Single';
		$vehicle->model = $request->input( 'model' );
		$vehicle->brand = $request->input( 'brand' );
		$vehicle->color = $request->input( 'color' );
		$vehicle->regno = $request->input( 'regno' );
		$status         = $vehicle->save();

		$customer       = customer::find( $cid );
		$customer->role = "Vehicle Partner";
		$customer->save();

		if ( $status ) {
			$request->session()->flash( 'success', 'Vehicle Added.' );
		} else {
			$request->session()->flash( 'error', 'Sorry an error occurred' );
		}

		return redirect( 'add-vehicle/' . $cid );
	}

	public function postAddMaintenance( Request $request ) {
		$maintenanceSchedule   = setting::where( 'name', 'maintenanceSchedule' )->get()->last();

		$vid = $request->input('vid');
		$maintenance = new maintenance();
		$maintenance->vid = $vid;
		$maintenance->uid = Auth::user()->uid;
		$maintenance->log = $request->input('log');
		$maintenance->details = $request->input('details');
		$maintenance->nextMaintenance = Carbon::now()->addWeeks($maintenanceSchedule->value);
		$maintenance->save();

		$request->session()->flash('success','Maintenance Record Added');

		return redirect('view-vehicle/' . $vid);

	}

	public function postAddFuel( Request $request ) {
		$vid = $request->input('vid');

		$fuel = new fuel();
		$fuel->vid = $vid;
		$fuel->station = $request->input('station');
		$fuel->amount = $request->input('amount');
		$fuel->uid = Auth::user()->uid;
		$fuel->save();

		$request->session()->flash('success','Fuel Log Added');

		return redirect('view-vehicle/' . $vid);
	}


	public function postVehicleCat( Request $request ) {
		$cat              = new vehiclecateogry();
		$cat->name        = $request->input( 'name' );
		$cat->description = $request->input( 'description' );
		$cat->maxHeight   = $request->input( 'maxHeight' );
		$cat->maxWidth    = $request->input( 'maxWidth' );
		$cat->maxDepth    = $request->input( 'maxDepth' );
		$status           = $cat->save();

		if ( $status ) {
			$request->session()->flash( 'success', 'Vehicle Category Added.' );
		} else {
			$request->session()->flash( 'error', 'Sorry an error occurred' );
		}


		return redirect( '/add-vehicle-cat' );
	}


	public function getRequests() {


		if ( Input::has( 'by' ) ) {
			$by       = Input::get( 'by' );
			$requests = deliveries::where( 'status', $by )->get();
		} else {
			$requests = deliveries::all();
		}

		$requests = $requests->reverse();

		return view( 'delivery.viewRequests', [
			'requests' => $requests
		] );
	}

	public function requestDetail( Request $request, $did ) {

		$delivery = deliveries::find( $did );

		return view( 'delivery.requestDetail', [
			'delivery' => $delivery
		] );


	}


	public function settings() {

		if ( count( setting::all() ) <= 0 ) {

			$setting        = new setting();
			$setting->name  = 'baseFare';
			$setting->value = 300;
			$setting->save();

			$setting        = new setting();
			$setting->name  = 'costPerKm';
			$setting->value = 50;
			$setting->save();

			$setting        = new setting();
			$setting->name  = 'referralBonus';
			$setting->value = 1000;
			$setting->save();

			$setting        = new setting();
			$setting->name  = 'adminPercent';
			$setting->value = 20;
			$setting->save();

			$setting        = new setting();
			$setting->name  = 'maintenancePercent';
			$setting->value = 20;
			$setting->save();

			$setting        = new setting();
			$setting->name  = 'partnerPercent';
			$setting->value = 50;
			$setting->save();

			$setting        = new setting();
			$setting->name  = 'carCostPerKm';
			$setting->value = 50;
			$setting->save();

			$setting        = new setting();
			$setting->name  = 'cancelMinutes';
			$setting->value = 5;
			$setting->save();

			$setting        = new setting();
			$setting->name  = 'cancellationFee';
			$setting->value = 100;
			$setting->save();

			$setting        = new setting();
			$setting->name  = 'maintenanceSchedule';
			$setting->value = 3;
			$setting->save();

		}

		$settings           = setting::all();
		$basefare           = setting::where( 'name', 'baseFare' )->get()->last();
		$costperkm          = setting::where( 'name', 'costPerKm' )->get()->last();
		$refferalBonus      = setting::where( 'name', 'referralBonus' )->get()->last();
		$adminPercent       = setting::where( 'name', 'adminPercent' )->get()->last();
		$maintenancePercent = setting::where( 'name', 'maintenancePercent' )->get()->last();
		$partnerPercent     = setting::where( 'name', 'partnerPercent' )->get()->last();
		$carCostPerKm        = setting::where( 'name', 'carCostPerKm' )->get()->last();
		$cancelMinutes        = setting::where( 'name', 'cancelMinutes' )->get()->last();
		$cancellationFee       = setting::where( 'name', 'cancellationFee' )->get()->last();
		$maintenanceSchedule   = setting::where( 'name', 'maintenanceSchedule' )->get()->last();


		return view( 'settings.getSettings', [
			'settings'           => $settings,
			'basefare'           => $basefare,
			'costperkm'          => $costperkm,
			'referralBonus'      => $refferalBonus,
			'adminPercent'       => $adminPercent,
			'maintenancePercent' => $maintenancePercent,
			'partnerPercent'     => $partnerPercent,
			'carCostPerKm'        => $carCostPerKm,
			'cancelMinutes'        => $cancelMinutes,
			'cancellationFee'        => $cancellationFee,
			'maintenanceSchedule'        => $maintenanceSchedule
		] );

	}

	public function postAddRemittance(Request $request) {
				$cid      = $request->input( 'cid' );

		try{
			DB::transaction(function () {

				global $request;
				$cid      = $request->input( 'cid' );
				$customer = customer::find( $cid );
				$amount   = $request->input( 'amount' );
				$type     = $request->input( 'type' );


				$dropper = dropper::find( $customer->Dropper->drid );

				if ( $dropper->unremittedCash + $dropper->unremittedPOS > 0 ) {


					if ( $type == 'cash' ) {

						if ($dropper->unremittedCash <= 0 ){
							$request->session()->flash( 'error', "Dropper has already remitted all cash" );
							return redirect('dropper/' . $cid );
						};

						if( ($dropper->unremittedCash -= $amount) <= 0){
							$request->session()->flash( 'error', "You entered a value greater than the total the dropper is meant to remit" );
							return redirect('dropper/' . $cid );
						}

						$dropper->unremittedCash -= $amount;
						$dropper->save();
					}

					if ( $type == 'pos' ) {
						if ($dropper->unremittedPOS <= 0){
							$request->session()->flash( 'error', "Dropper has already remitted all pos funds" );
							return redirect('dropper/' . $cid );
						};

						if( ($dropper->unremittedPOS -= $amount) <= 0){
							$request->session()->flash( 'error', "You entered a value greater than the total the dropper is meant to remit" );
							return redirect('dropper/' . $cid );
						}

						$dropper->unremittedPOS -= $amount;
						$dropper->save();
					}

					$remittance          = new remittance();
					$remittance->drid    = $dropper->drid;
					$remittance->amount  = $amount;
					$remittance->uid     = Auth::user()->uid;
					$remittance->details = $type;
					$remittance->save();

					$request->session()->flash( 'success', 'Remittance Added.' );


				} else {
					$request->session()->flash( 'error', "Dropper has already remitted all necessary amounts" );
				}

			},3); // number of times to retry if error exists

		} catch (\Exception $exception){
			$request->session()->flash( 'error', 'Sorry an error occurred. Please try again' );
		}

		return redirect('dropper/' . $cid );

	}

	public function postCustomerRequest(Request $request) {

//		try {


			$cid           = $request->input( 'cid' );
			$paymentMethod = $request->input( 'paymentMethod' );
			$amount        = $request->input( 'amount' );
			$customer          = customer::find( $cid );

			if ( $paymentMethod == 'balance' ) {

				if($customer->balance < $amount){
					$request->session()->flash('error',"Insufficient balance. Please ask the customer to add funds to their balance or select cash as the payment method.");
					return redirect('customer-request');
				}

			}

			$customer          = customer::find( $cid );

			if ($customer->balance < 0) {

				$amount += ( $customer->balance * - 1 );

				$request->session()->flash('error',"Customer owes $amount naira. Please request that the client adds funds to their balance before making a request on their behalf");
				return redirect('customer-request');
			}

			DB::transaction( function () {
				global $request;

				$deliveryType = $request->input('deliveryType');


				if($deliveryType == 'receiver'){
					$phone         = $request->input( 'senderPhone' );
					$fname         = $request->input( 'senderFname' );
					$sname         = $request->input( 'senderSname' );
					$email         = $request->input( 'senderEmail' );
				}

				else {
					$phone         = $request->input( 'receiverPhone' );
					$fname         = $request->input( 'receiverFname' );
					$sname         = $request->input( 'receiverSname' );
					$email         = $request->input( 'receiverEmail' );
				}


				$cid           = $request->input( 'cid' );
				$ditid         = $request->input( 'ditid' );
				$vcid          = $request->input( 'vcid' );
				$fromLat       = $request->input( 'fromLat' );
				$fromLng       = $request->input( 'fromLng' );
				$toLat         = $request->input( 'toLat' );
				$toLng         = $request->input( 'toLng' );
				$amount        = $request->input( 'amount' );
				$paymentMethod = $request->input( 'paymentMethod' );

				$delivery                = new deliveries();
				$delivery->from          = $request->input( 'from' );
				$delivery->fromAddress   = $request->input( 'fromAddress' );
				$delivery->to            = $request->input( 'to' );
				$delivery->toAddress     = $request->input( 'toAddress' );

				//senders details
				$delivery->phone         = $phone;
				$delivery->fname         = $fname;
				$delivery->sname         = $sname;
				$delivery->email         = $email;

				$delivery->companyName   = $request->input( 'companyName' );
				$delivery->description   = $request->input('description');
				$delivery->paymentMethod = $paymentMethod;
				$delivery->paymentStatus = 'unpaid';
				$delivery->deliveryType  = $request->input('deliveryType');


				$delivery->cid                = $cid;
				$delivery->ditid              = $ditid;
				$delivery->vcid               = $vcid;
				$delivery->status             = 'Available';
				$delivery->fromLng            = $fromLng;
				$delivery->fromLat            = $fromLat;
				$delivery->toLng              = $toLng;
				$delivery->toLat              = $toLat;
				$delivery->amount             = $amount;
				$delivery->isDropperConfirmed = 0;
				$delivery->isClientConfirmed  = 0;


				$status = $delivery->save();

				if ( $status && $paymentMethod == 'balance' ) {
					// deduct balance and mark delivery as paid

					$customer          = customer::find( $cid );
					$customer->balance = $customer->balance - $delivery->amount;
					$customer->save();
					$delivery->paymentStatus = 'paid';
					$delivery->save();

//					Mail::to( $customer->email )->send( new paymentReceipt( $delivery ) );

					$senderName   = $delivery->Customer->fname . " " . $delivery->Customer->sname;
					$receiverName = $delivery->fname;




					$customer = customer::where( 'phone', $delivery->phone )->first();

					// send notification
					if ( empty( $customer ) ) {
						$this->sendSms( $delivery->phone, "Hi $receiverName, you have an incoming delivery from $senderName. It was sent using dropster. Download our app to track your delivery.\n Visit www.dropsterng.com" );
					} else {

//						Mail::to( $customer->email )->send( new incomingDeliveryNotification( $delivery ) );
					}

				}

				$request->session()->flash('success','Request Successful.');

			}, 5 );
		return redirect("customer-request");

//		}  catch (\Exception $exception){
//			Bugsnag::notifyException($exception);
//			$cid           = $request->input( 'cid' );
//			$customer = customer::find($cid);
//			Mail::to("theworldofmamchika@gmail.com")->send(new newDeliveryErrorNotification($customer, $exception));
//			return response("An error occurred while making your request. Please try again, if this persists, please contact support.",500);
//		}

	}


	public function postUpdateCustomerBalance( Request $request ) {

		$cid = $request->input('cid');
		$customer = customer::find($cid);
		$customer->balance += $request->input('amount');
		$customer->save();

		$balanceUpdate = new customerBalanceUpdate();
		$balanceUpdate->cid = $cid;
		$balanceUpdate->uid = Auth::user()->uid;
		$balanceUpdate->amount = $request->input('amount');
		$balanceUpdate->reason = $request->input('reason');
		$balanceUpdate->save();

		$request->session()->flash('success','Balance Updated');

		return redirect('view-customer/' . $cid);
	}


	public function postSettings( Request $request ) {

		$settings        = new setting();
		$settings->name  = $request->input( 'name' );
		$settings->value = $request->input( 'value' );

		$status = $settings->save();

		if ( $status ) {
			$request->session()->flash( 'success', 'Settings Added.' );
		} else {
			$request->session()->flash( 'error', 'Sorry an error occurred' );
		}

		return redirect( '/settings' );
	}

	public function viewSettings() {


		$basefares  = setting::where( 'name', 'baseFare' )->get();
		$costperkms = setting::where( 'name', 'costPerKm' )->get();

		return view( 'settings.settingsHistory', [
			'basefares'  => $basefares,
			'costperkms' => $costperkms
		] );
	}

	public function viewVehicleCat() {
		$vehicleCats = vehiclecateogry::all();
		return view('vehicle.viewVehicleCat',[
			'vehicleCats' => $vehicleCats
		]);
	}

	public function editItemCat( $icid ) {
		$category = item_category::find( $icid );

		return view( 'delivery.addItemCat', [
			'category' => $category
		] );
	}

	public function getItemCat() {

		return view( 'delivery.addItemCat' );


	}

	public function postEditItemCat( Request $request ) {
		$icid     = $request->input( 'icid' );
		$category = item_category::find( $icid );

		$category->name        = $request->input( 'name' );
		$category->description = $request->input( 'description' );
		$status                = $category->save();

		if ( $status ) {
			$request->session()->flash( 'success', 'Category Edited' );
		} else {
			$request->session()->flash( 'error', 'Sorry an error occurred' );
		}

		return redirect( 'edit-item-cat/' . $icid );

	}

	public function postAddItemCat( Request $request ) {

		$items              = new item_category();
		$items->name        = $request->input( 'name' );
		$items->description = $request->input( 'description' );

		$status = $items->save();

		if ( $status ) {
			$request->session()->flash( 'success', 'Item Category Added.' );
		} else {
			$request->session()->flash( 'error', 'Sorry an error occurred' );
		}

		return redirect( 'add-item-cat' );
	}

	public function viewItemCat() {

		$items = item_category::all();

		return view( 'delivery.viewItemCat', [
			'items' => $items
		] );
	}


	public function getDeliveryItem() {

		$itemCat = item_category::all();

		return view( 'delivery.addDeliveryItem', [
			'itemCat' => $itemCat
		] );


	}

	public function postDeliveryItem( Request $request ) {

		$items       = new delivery_item();
		$items->icid = $request->input( 'icid' );
		$items->name = $request->input( 'name' );

		$status = $items->save();

		if ( $status ) {
			$request->session()->flash( 'success', 'Item Added.' );
		} else {
			$request->session()->flash( 'error', 'Sorry an error occurred' );
		}

		return redirect( 'add-delivery-items' );
	}

	public function viewDeliveryItems() {

		$items = delivery_item::all();

		return view( 'delivery.viewDeliveryItem', [
			'items' => $items
		] );
	}


	public function sendUpdates(Request $request  ) {

		$did = $request->input('did');

		$update = new update();
		$update->did = $did;
		$update->description = $request->input('description');
		$update->uid = Auth::user()->uid;

		$status = $update->save();


		if ( $status ) {
			$request->session()->flash( 'success', 'Update sent Successfully' );
		} else {
			$request->session()->flash( 'error', 'Sorry an error occurred' );
		}

		return redirect()->back();


	}


}