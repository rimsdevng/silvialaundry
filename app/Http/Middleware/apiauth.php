<?php

namespace App\Http\Middleware;

use App\setting;
use Closure;

class apiauth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    $apikey = setting::where('name', 'apikey')->get()->last();

	    if($request->headers->get('apikey') == $apikey->value){
		    return $next($request);
	    } else
	    	return response("Un-Authorized", 503);

    }
}
