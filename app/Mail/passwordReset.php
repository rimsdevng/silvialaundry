<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class passwordReset extends Mailable
{
    use Queueable, SerializesModels;

    public $code;
    public $customer;

    public function __construct($code,$customer)
    {
        $this->code = $code;
        $this->customer = $customer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from("notifications@dropster.com.ng","Dropster")
                            ->view('emails.mobilePasswordReset');
    }
}
