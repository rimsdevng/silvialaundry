<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class couponUse extends Model
{
    protected $primaryKey = 'cuid';
    public $table = 'couponuse';
}
