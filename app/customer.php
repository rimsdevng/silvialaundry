<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customer extends Model
{

    protected $primaryKey = 'cid';

	protected $hidden = [
		'password','updated_at','referredBy','deleted_at','isEmailConfirmed'
	];

	protected $guarded = [];

	public function Dropper() {
		return $this->hasOne(dropper::class, 'cid','cid');
    }

	public function Deliveries() {
		return $this->hasMany(deliveries::class,'cid','cid');
    }

	public function Vehicles() {
		return $this->hasMany(vehicle::class,'cid','cid');
    }

	public function Payments() {
		return $this->hasMany(payment::class,'cid','cid');
    }

	public function Withdrawals() {
		return $this->hasMany(withdrawal::class,'cid', 'cid');
    }

	public function BalanceUpdates() {
		return $this->hasMany(customerBalanceUpdate::class,'cid','cid');
    }
}
