<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class customerBalanceUpdate extends Model
{
    protected $primaryKey = 'cbid';
    protected $table = 'customerbalanceupdate';

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
    }
}
