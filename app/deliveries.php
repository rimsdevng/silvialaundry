<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class deliveries extends Model
{
    protected $primaryKey = 'did';
    public $table = 'deliveries';

	public function Customer() {
		return $this->belongsTo(customer::class, 'cid' , 'cid');
    }

	public function VehicleCategory() {
		return $this->belongsTo(vehiclecateogry::class,'vcid', 'vcid');
    }

	public function Dropper() {
		return $this->belongsTo(dropper::class,'drid','drid');
    }

	public function Items() {
		return $this->belongsTo(delivery_item::class,'ditid','ditid');
    }

	public function Images() {
		return $this->hasMany(delivery_image::class,'did', 'did');
    }

	public function Updates() {
		return $this->hasMany(update::class,'did','did');
    }

	public function PriceUpdates() {
		return $this->hasMany(priceupdate::class,'did','did');
    }


}
