<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class delivery_item extends Model
{
    protected $primaryKey = 'ditid';
    public $table = 'delivery_items';


	public function Deliveries() {
		return $this->belongsTo(deliveries::class, 'didtid', 'ditid');
    }

	public function Category(  ) {
		return $this->belongsTo(item_category::class, 'icid', 'icid');
    }
}
