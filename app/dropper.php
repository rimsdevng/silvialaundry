<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dropper extends Model
{
    //
    protected $primaryKey = 'drid';

    protected $table = 'droppers';


    public function Vehicle()
    {
        return $this->hasOne(vehicle::class, 'vid','vid');
    }

	public function Customer() {
		return $this->belongsTo(customer::class,'cid');
    }

	public function Deliveries() {
		return $this->hasMany(deliveries::class,'drid','drid');
    }

}
