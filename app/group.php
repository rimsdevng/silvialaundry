<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class group extends Model
{
    protected $primaryKey = 'gid';

	public function Members(  ) {
		return $this->hasMany(group_member::class,'gid');
    }

	public function Vehicles() {
		return $this->hasMany(vehicle::class,'gid','gid');
    }
}
