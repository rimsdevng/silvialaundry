<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class group_member extends Model
{
    protected $primaryKey = 'gmid';

	public function Customer() {
		return $this->hasOne(customer::class,'cid','cid');
    }
}
