<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class maintenance extends Model
{
	protected $primaryKey = 'mid';
    protected $table = 'maintenance';

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
    }

	public function Vehicle() {
		return $this->belongsTo(vehicle::class,'vid','vid');
    }
}
