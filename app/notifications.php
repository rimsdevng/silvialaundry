<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notifications extends Model
{
    protected $primaryKey = 'notid';
    protected $table = 'notifications';

	public function User() {
		return $this->belongsTo(User::class,'uid','uid');
    }
}
