<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class payment extends Model
{
    protected $primaryKey = 'payid';

	public function Customer() {
		return $this->belongsTo(customer::class,'cid','cid');
    }
}
