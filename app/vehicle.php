<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vehicle extends Model
{
    protected $primaryKey = 'vid';


    public function Customer()
    {
        return $this->belongsTo(customer::class, 'cid');
    }

    public function VehicleCategory()
    {
        return $this->belongsTo( vehiclecateogry::class, 'vcid');
    }

	public function Dropper(  ) {

    	return $this->belongsTo(dropper::class, 'vid','vid');
    }

	public function Maintenance() {
		return $this->hasMany(maintenance::class,'vid','vid');
    }

	public function fuel() {
		return $this->hasMany(fuel::class,'vid','vid');
    }

	public function Deliveries() {
		return $this->hasMany(deliveries::class,'vid','vid');
    }

}
