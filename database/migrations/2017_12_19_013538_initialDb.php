<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('delivery_issues', function (Blueprint $table){

		    $table->increments('disid');
		    $table->integer('drid');
		    $table->integer('cid');
		    $table->integer('uid');
		    $table->integer('did');
		    $table->string('message');
		    $table->enum('status',['open','closed']);
		    $table->timestamps();

	    });

	    Schema::create('remittances', function (Blueprint $table){
		    $table->increments('rmid');
		    $table->integer('drid');
		    $table->integer('uid');
		    $table->decimal('amount');
		    $table->string('details')->nullable();
		    $table->timestamps();
	    });


	    Schema::create('groups', function(Blueprint $table){
		    $table->increments('gid');
		    $table->integer('vcid');
		    $table->string('name');
		    $table->integer('total');
		    $table->timestamps();
	    });

	    Schema::create('group_members', function(Blueprint $table){
		    $table->increments('gmid');
		    $table->integer('gid');
		    $table->integer('cid');
		    $table->integer('percent');
		    $table->timestamps();
	    });

	    Schema::Create( 'support', function ( Blueprint $table ) {
		    $table->increments( 'sid' );
		    $table->integer( 'cid' )->nullable();
		    $table->integer( 'did' )->nullable();
		    $table->enum( 'type', [ 'General', 'Delivery', 'Sales', 'Complaints', 'Suggestions' ] );
		    $table->string( 'title', 2000 );
		    $table->enum( 'status', [ 'Awaiting User', 'Awaiting Staff', 'Complete' ] );
		    $table->softDeletes();
		    $table->timestamps();
	    } );

	    Schema::create( 'vehicles', function ( Blueprint $table ) {
		    $table->increments( 'vid' );
		    $table->integer( 'vcid' );
		    $table->integer('earnings')->default(0);
		    $table->enum('type',['Single','Group']);
		    $table->integer( 'cid' )->nullable();
		    $table->integer( 'gid' )->nullable();
		    $table->string( 'model' );
		    $table->string( 'brand' );
		    $table->string( 'color' );
		    $table->string( 'regno',191)->unique();
		    $table->softDeletes();
		    $table->timestamps();
	    } );




	    Schema::create( 'vehicle_assignment_histories', function ( Blueprint $table ) {
		    $table->increments( 'vahid' );
		    $table->integer( 'drid' );
		    $table->integer( 'vid' );
		    $table->softDeletes();
		    $table->timestamps();
	    } );

	    Schema::create( 'vehicle_assignment_dump', function ( Blueprint $table ) {
		    $table->increments( 'vadid' );
		    $table->string( 'model' );
		    $table->string( 'brand' );
		    $table->string( 'color' );
		    $table->integer( 'cid' );
		    $table->string( 'regno' );
		    $table->string( 'vc_name' );
		    $table->string( 'vc_drscription' );
		    $table->integer( 'vc_maxHeight' );
		    $table->integer( 'vc_maxWidth' );
		    $table->integer( 'vc_maxDepth' );
		    $table->integer( 'dr_vid' );
		    $table->string( 'dr_fname' );
		    $table->string( 'dr_sname' );
		    $table->string( 'dr_address' );
		    $table->string( 'dr_phone' );
		    $table->string( 'dr_gender' );

		    $table->string( 'image', 2000 );
		    $table->string( 'dr_rating' );
		    $table->softDeletes();
		    $table->timestamps();
	    } );

	    Schema::create( 'deliveries', function ( Blueprint $table ) {
		    $table->increments( 'did' );
		    $table->integer( 'ditid' );
		    $table->string( 'cid' );
		    $table->string( 'vcid' );
		    $table->string( 'vid' );
		    $table->integer( 'drid' )->nullable();
		    $table->string( 'from' );
		    $table->string('description',2000)->nullable();
		    $table->string( 'fromAddress' );
		    $table->decimal( 'fromLat',10,8);
		    $table->decimal( 'fromLng' ,10,8);
		    $table->string( 'to' );
		    $table->string( 'toAddress' );
		    $table->decimal( 'toLat' ,10,8);
		    $table->decimal( 'toLng' ,10,8);
		    $table->decimal( 'amount' );
		    $table->string( 'phone' );
		    $table->string( 'fname' );
		    $table->string( 'sname' )->nullable();
		    $table->string( 'email' )->nullable();
		    $table->string( 'companyName' )->nullable();
		    $table->enum('deliveryType',['sender','receiver','third','return']);
		    $table->enum('paymentMethod',['balance','pos','cash']);
		    $table->enum('paymentStatus',['paid','unpaid']);
		    $table->enum( 'status', [ 'Available', 'Pending', 'Complete', 'Issue', 'Cancelled' ] );
		    $table->boolean( 'isDropperConfirmed' )->default(0);
		    $table->boolean( 'isClientConfirmed' )->default(0);
		    $table->string('pickedUpAt')->nullable();
		    $table->string('droppedOffAt')->nullable();
		    $table->string('returnedAt')->nullable();
		    $table->softDeletes();
		    $table->timestamps();
	    } );


	    Schema::create( 'delivery_items', function ( Blueprint $table ) {
		    $table->increments( 'ditid' );
		    $table->integer( 'icid' );
		    $table->string( 'name' );
		    $table->softDeletes();
		    $table->timestamps();
	    } );

	    Schema::create( 'droppers', function ( Blueprint $table ) {
		    $table->increments( 'drid' );
		    $table->integer( 'cid' );
		    $table->integer( 'vid' )->nullable();
		    $table->boolean( 'isAvailable' )->default(0);
		    $table->decimal( 'latitude',8,6 )->nullable();
		    $table->decimal( 'longitude', 8,6)->nullable();
		    $table->decimal( 'rating')->default(0);
		    $table->integer( 'unremittedCash')->default(0);
		    $table->integer( 'unremittedPOS')->default(0);
		    $table->softDeletes();
		    $table->timestamps();
	    } );

	    Schema::create( 'dropper_refs', function ( Blueprint $table ) {
		    $table->increments( 'drrid' );
		    $table->integer( 'drid' );
		    $table->string( 'fname' );
		    $table->string( 'sname' );
		    $table->string( 'phone' );
		    $table->string( 'address' );
		    $table->enum( 'gender', [ 'Male', 'Female' ] );
		    $table->string( 'companyName' )->nullable();
		    $table->string( 'occupation' );
		    $table->softDeletes();
		    $table->timestamps();
	    } );

	    Schema::create('dropper_docs', function(Blueprint $table){
		    $table->increments('ddid');
		    $table->string('url',2000);
		    $table->string('title');
		    $table->string('description')->nullable();
		    $table->softDeletes();
		    $table->timestamps();

	    });

	    Schema::create( 'delivery_histories', function ( Blueprint $table ) {
		    $table->increments( 'dhid' );
		    $table->string( 'did' );
		    $table->string( 'cid' );
		    $table->string( 'icid' );
		    $table->integer( 'vcid' );
		    $table->integer( 'drid' )->nullable();
		    $table->integer( 'transactionid' );
		    $table->string( 'from' );
		    $table->string( 'fromDec' )->nullable();
		    $table->decimal( 'fromLat' );
		    $table->decimal( 'fromLng' );
		    $table->string( 'to' );
		    $table->string( 'toDec' )->nullable();
		    $table->decimal( 'toLat' );
		    $table->decimal( 'toLng' );
		    $table->decimal( 'amount' );
		    $table->string( 'phone' );
		    $table->string( 'fname' );
		    $table->string( 'sname' );
		    $table->string( 'email' )->nullable();
		    $table->string( 'companyName' )->nullable();
		    $table->enum( 'status', [ 'Available', 'Pending', 'Completed', 'Issue' ] )->default('Available');
		    $table->timestamp('pickedUpAt')->default(null)->nullable();
		    $table->timestamp('droppedOffAt')->default(null)->nullable();
		    $table->boolean( 'isDropperConfirmed' );
		    $table->boolean( 'isClientConfirmed' );
		    $table->softDeletes();
		    $table->timestamps();
	    } );

	    Schema::create( 'delivery_images', function ( Blueprint $table ) {
		    $table->increments( 'diid' );
		    $table->string( 'url' );
		    $table->integer( 'did' );
		    $table->softDeletes();
		    $table->timestamps();
	    } );

	    Schema::create( 'customers', function ( Blueprint $table ) {
		    $table->increments( 'cid' );
		    $table->enum('type',['Business','Personal'])->default('Personal');
		    $table->enum( 'role', [ 'Dropper', 'Vehicle Partner', 'Customer' ] )->default('Customer');
		    $table->string( 'referralCode', 191 )->unique();
		    $table->string( 'referredBy' )->nullable();
		    $table->string('deviceID');
		    $table->string( 'fname' );
		    $table->string( 'sname' );
		    $table->string( 'username' );
		    $table->string( 'password', 60 );
		    $table->string( 'email',191 )->unique();
		    $table->string( 'image', 2000 );
		    $table->enum( 'gender', [ 'Male', 'Female' ] );
		    $table->string( 'phone',191 )->unique();
		    $table->string('accountName')->nullable();
		    $table->string('accountNumber')->nullable();
		    $table->string('bankCode')->nullable();
		    $table->string('accountBank')->nullable();
		    $table->decimal( 'balance' )->default(0.00);
		    $table->string( 'address' )->nullable();
		    $table->string( 'website' )->nullable();
		    $table->boolean('isEmailConfirmed')->default(0);
		    $table->boolean('dailyPayout')->default(0);
		    $table->softDeletes();
		    $table->timestamps();
	    } );

	    Schema::create('confirmations', function(Blueprint $table){
		    $table->increments('confid');
		    $table->string('phone')->nullable();
		    $table->string('email')->nullable();
		    $table->string('code');
		    $table->timestamps();
	    });


	    Schema::create('affiliates', function ( Blueprint $table ) {
		    $table->increments( 'afid' );
		    $table->integer( 'cid' );
		    $table->decimal( 'amount' );
		    $table->softDeletes();
		    $table->timestamps();
	    } );


	    Schema::create( 'item_categories', function ( Blueprint $table ) {
		    $table->increments( 'icid' );
		    $table->string( 'name' ,191)->unique();
		    $table->string( 'description' );
		    $table->softDeletes();
		    $table->timestamps();
	    } );

	    Schema::create('settings', function(Blueprint $table){
		    $table->increments('setid');
		    $table->string('name');
		    $table->string('value');
		    $table->timestamps();
		    $table->softDeletes();
	    });


	    Schema::create( 'vehicle_categories', function ( Blueprint $table ) {
		    $table->increments( 'vcid' );
		    $table->string( 'name' );
		    $table->string( 'description' );
		    $table->integer( 'maxHeight' );
		    $table->integer( 'maxWidth' );
		    $table->integer( 'maxDepth' );
		    $table->softDeletes();
		    $table->timestamps();
	    } );

	    Schema::create('coupons', function(Blueprint $table){
		    $table->increments('coupid');
		    $table->integer('uid');
		    $table->string('code',191)->unique();
		    $table->integer('value');
		    $table->enum('status',['Available','Expired']);
		    $table->integer('maxUsages');
		    $table->timestamp('expiryDate');
		    $table->softDeletes();
		    $table->timestamps();

	    });

	    Schema::create('customerFavorites', function(Blueprint $table){
		    $table->increments('cfid');
		    $table->integer('cid');
		    $table->string('fname');
		    $table->string('sname')->nullable();
		    $table->string('phone');
		    $table->string('email')->nullable();
		    $table->softDeletes();
		    $table->timestamps();
	    });

	    Schema::create('customerLocations', function(Blueprint $table){
		    $table->increments('clid');
		    $table->integer('cid');
		    $table->string('name');
		    $table->decimal('lat',15,14);
		    $table->decimal('lng',15,14);
		    $table->softDeletes();
		    $table->timestamps();

	    });

	    Schema::create('locations', function(Blueprint $table){
		    $table->increments('lid');
		    $table->integer('drid')->index();
		    $table->decimal('lat',15,14);
		    $table->decimal('lng',15,14);
		    $table->timestamps();
		    $table->softDeletes();
	    });

	    Schema::create('couponuse', function(Blueprint $table){
		    $table->increments('cuid');
		    $table->integer('coupid');
		    $table->integer('cid');
		    $table->softDeletes();
		    $table->timestamps();
	    });

	    Schema::create('withdrawals', function(Blueprint $table){
		    $table->increments('withid');
		    $table->integer('cid');
		    $table->integer('amount');
		    $table->string('accountBank');
		    $table->string('accountName');
		    $table->string('accountNumber');
		    $table->string('bankCode');
		    $table->timestamps();
		    $table->softDeletes();

	    });

	    Schema::create('payments', function(Blueprint $table){
		    $table->increments('payid');
		    $table->string('amount');
		    $table->string('method');
		    $table->integer('cid');
		    $table->string('others');
		    $table->timestamps();
		    $table->softDeletes();
	    });

	    Schema::create('terms', function(Blueprint $table){
		    $table->increments('tid');
		    $table->string('content',10000);
		    $table->timestamps();
		    $table->softDeletes();
	    });


	    Schema::create('privacy', function(Blueprint $table){
		    $table->increments('pid');
		    $table->string('content',10000);
		    $table->timestamps();
		    $table->softDeletes();
	    });

	    Schema::create('faqs', function(Blueprint $table){
		    $table->increments('fid');
		    $table->string('question');
		    $table->string('answer',5000);
		    $table->timestamps();
		    $table->softDeletes();
	    });

	    Schema::create('vpas', function(Blueprint $table){
		    $table->increments('vaid');
		    $table->string('content',10000);
		    $table->timestamps();
		    $table->softDeletes();
	    });

	    // Schema::create('priceupdates', function(Blueprint $table){
		//     $table->increments('puid');
		//     $table->string('description');
		//     $table->integer('amount');
		//     $table->timestamps();
	    // });


	    Schema::create( 'password_resets', function ( Blueprint $table ) {
		    $table->increments('prid');
		    $table->string( 'email' ,191)->index();
		    $table->string( 'token' );
		    $table->timestamp( 'created_at' )->nullable();
	    } );


	    Schema::create( 'users', function ( Blueprint $table ) {
		    $table->increments( 'uid' );
		    $table->enum( 'role', [ 'Admin', 'Staff' ] );
		    $table->string( 'fname' );
		    $table->string( 'sname' );
		    $table->string( 'email',191 )->unique();
		    $table->string( 'password', 60 );
		    $table->string( 'phone',191 )->unique();
		    $table->string('address',1000);
		    $table->integer( 'sid' );
		    $table->enum( 'maritalStatus', [ 'Single', 'Married', 'Divorced', 'Others' ] );
		    $table->enum( 'gender', [ 'Male', 'Female' ] );
		    $table->string( 'dob' );
		    $table->rememberToken();
		    $table->string( 'image', 2000 );
		    $table->softDeletes();
		    $table->timestamps();
	    } );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('support');
	    Schema::dropIfExists('vehicles');
	    Schema::dropIfExists('vehicle_assignment_history');
	    Schema::dropIfExists('vehicle_assignment_dump');
	    Schema::dropIfExists('deliveries');
	    Schema::dropIfExists('delivery_items');
	    Schema::dropIfExists('droppers');
	    Schema::dropIfExists('dropper_refs');
	    Schema::dropIfExists('delivery_histories');
	    Schema::dropIfExists('delivery_images');
	    Schema::dropIfExists('delivery_issues');
	    Schema::dropIfExists('remittances');
	    Schema::dropIfExists('customers');
	    Schema::dropIfExists('customer_roles');
	    Schema::dropIfExists('affiliate');
	    Schema::dropIfExists('item_categories');
	    Schema::dropIfExists('vehicle_categories');
	    Schema::dropIfExists('payments');
	    Schema::dropIfExists('users');
	    Schema::dropIfExists('groups');
	    Schema::dropIfExists('group_members');
	    Schema::dropIfExists('password_resets');
	    Schema::dropIfExists('terms');
	    Schema::dropIfExists('vpas');
	    Schema::dropIfExists('faqs');
	    Schema::dropIfExists('privacy');

    }
}
