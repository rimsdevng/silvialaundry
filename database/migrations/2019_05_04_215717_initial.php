<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Initial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create( 'items', function ( Blueprint $table ) {
		    $table->increments( 'iid' );
		    $table->integer( 'icid' );
		    $table->integer('price')->default(0);
		    $table->enum('type',['Blanket','Carpet','Window-Cottins','Jeans','T-Shirts','Suits','Table-Cloth','Shirts','Skates','']);
		    $table->integer( 'cid' )->nullable();
		    $table->string( 'brand' );
		    $table->string( 'color' );
		    $table->softDeletes();
		    $table->timestamps();
	    } );


	    Schema::create( 'item_images', function ( Blueprint $table ) {
		    $table->increments( 'itmid' );
		    $table->string( 'url' );
		    $table->integer( 'bsid' );
		    $table->softDeletes();
		    $table->timestamps();
	    } );

        Schema::create( 'item_categories', function ( Blueprint $table ) {
		    $table->increments( 'icid' );
		    $table->string( 'name' ,191)->unique();
		    $table->string( 'description' );
		    $table->softDeletes();
		    $table->timestamps();
	    } );

	    Schema::create( 'booking_requests', function ( Blueprint $table ) {
		    $table->increments( 'bsid' );
		    $table->string( 'icid' )->nullable();
            $table->string( 'iid' )->nullable();
            $table->string( 'itmid' )->nullable();
            $table->string('description',2000)->nullable();
            
		    $table->string( 'from' )->nullable();
            $table->string( 'fromAddress' )->nullable();
            
            $table->string( 'to' )->nullable();
		    $table->string( 'toAddress' )->nullable();
			
			$table->integer( 'quantity' )->nullable();

			$table->decimal( 'cost' )->nullable();
			
		    $table->string( 'phone' )->nullable();
		    $table->string( 'fname' )->nullable();
		    $table->string( 'sname' )->nullable();
		    $table->string( 'email' )->nullable();
		    $table->enum('serviceType',['washing','ironing','both']);
		    $table->enum('paymentMethod',['balance','pos','cash']);
		    $table->enum('paymentStatus',['paid','unpaid']);
		    $table->enum( 'status', [ 'Processing', 'Pending', 'Completed', 'Issue', 'Cancelled' ] );
		  
		    $table->softDeletes();
		    $table->timestamps();
	    } );


	 
	    Schema::create( 'booking_request_histories', function ( Blueprint $table ) {
		    $table->increments( 'bshid' );
		    $table->integer( 'bsid' );
		    $table->string( 'icid' )->nullable();
            $table->string( 'iid' )->nullable();
            $table->string( 'itmid' )->nullable();
            $table->string('description',2000)->nullable();
            
		    $table->string( 'from' )->nullable();
            $table->string( 'fromAddress' )->nullable();
            
            $table->string( 'to' )->nullable();
		    $table->string( 'toAddress' )->nullable();
			
			$table->integer( 'quantity' )->nullable();

			$table->decimal( 'cost' )->nullable();
			
		    $table->string( 'phone' )->nullable();
		    $table->string( 'fname' )->nullable();
		    $table->string( 'sname' )->nullable();
		    $table->string( 'email' )->nullable();
		    $table->enum('serviceType',['washing','ironing','both']);
		    $table->enum('paymentMethod',['balance','pos','cash']);
		    $table->enum('paymentStatus',['paid','unpaid']);
		    $table->enum( 'status', [ 'Processing', 'Pending', 'Completed', 'Issue', 'Cancelled' ] );
		    $table->softDeletes();
		    $table->timestamps();
	    } );



	}
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         
        Schema::dropIfExists('items');
        Schema::dropIfExists('item_images');
        Schema::dropIfExists('item_categories');
	    Schema::dropIfExists('booking_requests');
	    Schema::dropIfExists('booking_request_histories');
      
    }
}
