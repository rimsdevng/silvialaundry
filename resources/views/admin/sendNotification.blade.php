@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        @include('notification')
                        <div class="white-box">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Send Push Notification</div>

                                        <div class="panel-body">

                                            <form class="form-horizontal" role="form" method="POST" action="{{ url('send-notification') }}">
                                                {{ csrf_field() }}


                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"> Message </label>

                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control" name="message" required autofocus>
                                                    </div>
                                                </div>

                                                <div class="form-group" >
                                                    <div class="col-md-10 col-md-offset-4">

                                                        <button type="submit" class="btn btn-primary pull-right">
                                                            Send Notification
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

@endsection


