<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        @include('notification')
                        <div class="white-box">

                            <div class="row">
                                <div class="col-md-12 ">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Update Vehicle Partner Agreement</div>

                                        <div class="panel-body">

                                            @if(isset($vpas))
                                            <span>Last updated: {{Carbon::createFromFormat("Y-m-d H:i:s",$vpas->created_at)->toDayDateTimeString()}}</span>
                                            @endif
                                            <form class="form-horizontal" role="form" method="POST" action="{{ url('vpas') }}">
                                                {{ csrf_field() }}

                                                <div class="form-group">
                                                    <label for="description" class="control-label">&nbsp;Content</label>

                                                    <div class="col-md-12">
                                                            <textarea id="description"  class="form-control" name="content" required>@if(isset($vpas)){{$vpas->content}}@endif</textarea>
                                                    </div>
                                                </div>



                                                <div class="form-group" >
                                                    <div class="col-md-12 ">

                                                        <button type="submit" class="btn btn-primary pull-right">
                                                            Update
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection


