
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        @include('notification')
                        <div class="white-box">
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">Register</div>
                                            <div class="panel-body">
                                                <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ route('register') }}">
                                                    {{ csrf_field() }}

                                                    <div class="form-group{{ $errors->has('fname') ? ' has-error' : '' }}">
                                                        <label for="name" class="col-md-4 control-label">First Name</label>

                                                        <div class="col-md-6">
                                                            <input id="name" type="text" class="form-control" name="fname" value="{{ old('fname') }}" required autofocus>

                                                            @if ($errors->has('fname'))
                                                                <span class="help-block">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                                                            @endif
                                                        </div>


                                                    </div>

                                                    <div class="form-group{{ $errors->has('sname') ? ' has-error' : '' }}">
                                                        <label for="name" class="col-md-4 control-label">Last Name</label>

                                                        <div class="col-md-6">
                                                            <input id="name" type="text" class="form-control" name="sname" value="{{ old('sname') }}" required autofocus>

                                                            @if ($errors->has('sname'))
                                                                <span class="help-block">
                                        <strong>{{ $errors->first('sname') }}</strong>
                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('dob') ? ' has-error' : '' }}">
                                                        <label for="dob" class="col-md-4 control-label">Date of Birth</label>

                                                        <div class="col-md-6">
                                                            <input id="dob" type="text" class="form-control" onchange="console.log(this.value)" name="dob" placeholder="DD/MM/YYYY" value="{{ old('dob') }}" required autofocus>

                                                            @if ($errors->has('dob'))
                                                                <span class="help-block">
                                        <strong>{{ $errors->first('sname') }}</strong>
                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <script>
                                                        $(document).ready( function() {

                                                            var dateFormat = "dd/mm/yy",
                                                                from = $( "#dob" )
                                                                    .datepicker({
                                                                        changeMonth: true,
                                                                        changeYear: true,
                                                                        numberOfMonths: 1,
                                                                        yearRange: "1950:2010"
                                                                    }).on( "change", function() {
                                                                        from.datepicker( "option", "dateFormat", dateFormat );
                                                                    });

                                                        } );
                                                    </script>


                                                    <div class="form-group{{ $errors->has('sid') ? ' has-error' : '' }}">
                                                        <label for="sid" class="col-md-4 control-label">Staff ID</label>

                                                        <div class="col-md-6">
                                                            <input id="sid" type="text" class="form-control" name="sid" value="{{ old('sid') }}" required autofocus>

                                                            @if ($errors->has('sid'))
                                                                <span class="help-block">
                                        <strong>{{ $errors->first('sid') }}</strong>
                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>


                                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                                        <div class="col-md-6">
                                                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                                            @if ($errors->has('email'))
                                                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                                        <label for="password" class="col-md-4 control-label">Password</label>

                                                        <div class="col-md-6">
                                                            <input id="password" type="password" class="form-control" name="password" required>

                                                            @if ($errors->has('password'))
                                                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                                        <div class="col-md-6">
                                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                                        </div>
                                                    </div>



                                                    <div class="form-group">
                                                        <label for="role" class="col-md-4 control-label">Role</label>

                                                        <div class="col-md-6">
                                                            <select class="form-control" name="role">
                                                                <option>Admin</option>
                                                                <option>Staff</option>


                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                                        <label for="phone" class="col-md-4 control-label">Phone Number</label>

                                                        <div class="col-md-6">
                                                            <input id="phone" type="number" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>

                                                            @if ($errors->has('phone'))
                                                                <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>




                                                    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                                        <label for="address" class="col-md-4 control-label">Address</label>

                                                        <div class="col-md-6">
                                                            <textarea id="address" class="form-control" name="address"  required>{{ old('address') }}</textarea>

                                                            @if ($errors->has('address'))
                                                                <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="maritalStatus" class="col-md-4 control-label">Marital Status</label>

                                                        <div class="col-md-6">
                                                            <select class="form-control" name="maritalStatus">
                                                                <option>Single</option>
                                                                <option>Married</option>

                                                            </select>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="gender" class="col-md-4 control-label">Gender</label>

                                                        <div class="col-md-6">
                                                            <select class="form-control" name="gender">
                                                                <option>Male</option>
                                                                <option>Female</option>
                                                                <option>Others</option>

                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                                        <label for="image" class="col-md-4 control-label">Upload Profile Image</label>

                                                        <div class="col-md-6">
                                                            <input id="image" type="file" class="fileinput" name="image" required>

                                                            @if ($errors->has('image'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('image') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>





                                                    <div class="form-group">
                                                        <div class="col-md-6 col-md-offset-4">
                                                            <button type="submit" class="btn btn-primary">
                                                                Create User
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
            </div>
        </div>
    </div>

@endsection


