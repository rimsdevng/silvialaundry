

<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">

                        @include('notification')

                        <!-- .row -->
                            <div class="row bg-title" >
                                <div class="col-lg-12">
                                    CUSTOMER DETAILS
                                </div>
                            </div>

                            <!-- .row -->
                            <div class="row">

                                <!-- from here -->
                                <div class="col-md-12 col-xs-12">
                                    <div class="white-box">
                                        <ul class="nav customtab nav-tabs" role="tablist">

                                            <li role="presentation" class="nav-item"><a href="#basic" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Basic</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#details" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Details</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#transactions" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Transactions</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#balance" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Balance</span></a></li>
                                        </ul>
                                        <div class="tab-content">

                                            <!-- start tab 1 -->
                                            <div class="tab-pane active" id="basic">
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="white-box">
                                                        <div class="user-bg"> <img width="100%" alt="user" src="{{url('img/landscape17.jpg')}}">
                                                            <div class="overlay-box">
                                                                <div class="user-content">
                                                                    <a href="javascript:void(0)"><img src="{{$customer->image}}" class="thumb-lg img-circle" alt="img"></a>
                                                                    <h4 class="text-white">{{$customer->fname}} {{$customer->sname}}</h4>
                                                                    <h5 class="text-white">Created {{Carbon::createFromFormat("Y-m-d H:i:s",$customer->created_at)->toFormattedDateString()}} </h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="user-btm-box">
                                                            <div class="col-md-4 col-sm-4 text-center">

                                                            </div>
                                                            <div class="col-md-4 col-sm-4 text-center">

                                                            </div>
                                                            <div class="col-md-4 col-sm-4 text-center">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-8">
                                                    <div class="row">
                                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Gender</strong>
                                                            <br>
                                                            <p class="text-muted">{{ $customer->gender }}</p>
                                                        </div>
                                                        <div class="col-md-3 col-xs-6 b-r">
                                                            <strong>Joined</strong>
                                                            <br>
                                                            <p class="text-muted">{{Carbon::createFromFormat("Y-m-d H:i:s",$customer->created_at)->diffForHumans()}}</p>
                                                        </div>

                                                        <div class="col-md-3 col-xs-6 b-r">
                                                            <strong>Total transactions</strong>
                                                            <br>
                                                            <p class="text-muted">{{count($customer->Deliveries)}}</p>
                                                        </div>

                                                        <div class="col-md-3 col-xs-6 b-r">
                                                            <strong>Balance</strong>
                                                            <br>
                                                            <p class="text-muted"> &#x20A6; {{$customer->balance}}</p>
                                                        </div>

                                                    </div>


                                                    <hr>

                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <th>First Name</th>
                                                            <th>Surname</th>
                                                            <th>Phone</th>
                                                            <th>Email</th>
                                                        </tr>


                                                        <tr>
                                                            <td>{{$customer->fname}}</td>
                                                            <td>{{$customer->sname}}</td>
                                                            <td>{{$customer->phone}}</td>
                                                            <td>{{$customer->email}}</td>
                                                        </tr>

                                                    </table>

                                                    <a href="{{url('lookup-customers')}}" class="btn btn-warning">Back</a>

                                                </div>



                                            </div>
                                            <!-- end tab 1 -->

                                            <!-- start tab 2  -->
                                            <div class="tab-pane" id="details">

                                                <div class="form-group">
                                                    <label class="col-md-12">Firstname</label>
                                                    <div class="col-md-12">
                                                        <p>  {{$customer->fname}}</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Surname</label>
                                                    <div class="col-md-12">
                                                        <p>  {{$customer->sname}}</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Email</label>
                                                    <div class="col-md-12">
                                                        <p>  {{$customer->email}}</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Phone</label>
                                                    <div class="col-md-12">
                                                        <p>  {{$customer->phone}}</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Gender</label>
                                                    <div class="col-md-12">
                                                        <p>  {{$customer->gender}}</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-12">Address</label>
                                                    <div class="col-md-12">
                                                        <p>  {{$customer->address}}</p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <a href="{{url('/customer/'. $customer->cid . '/edit')}}" class="btn btn-success">Edit</a>

                                                    </div>
                                                </div>

                                            </div>
                                            <!-- end tab 2 -->

                                            <!-- start tab 3 -->
                                            <div class="tab-pane" id="transactions">

                                                <table id="demo-foo-addrow" class="table table-bordered table-hover toggle-circle" data-page-size="10">
                                                    <thead>
                                                    <tr>
                                                        <th data-sort-initial="false" data-toggle="true">From</th>
                                                        <th>To</th>
                                                        <th data-hide="phone, tablet">Receiver Name</th>
                                                        <th data-hide="phone, tablet">Email</th>
                                                        <th data-hide="phone, tablet">Images</th>
                                                        <th >Status</th>
                                                        <th >Price(₦)</th>
                                                        <th >Date</th>
                                                        <th data-sort-ignore="true" class="min-width"> </th>
                                                    </tr>
                                                    </thead>
                                                    <div class="form-inline padding-bottom-15">
                                                        <div class="row">

                                                        </div>
                                                        <div class="col-sm-6 text-right m-b-20">
                                                            <div class="form-group">
                                                                <input id="demo-input-search2" type="text" placeholder="Search" class="form-control" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <tbody>

                                                    @foreach($customer->Deliveries as $item)
                                                        <tr>
                                                            <td>{{$item->from}}</td>
                                                            <td>{{$item->to}}</td>
                                                            <td>{{$item->fname}} {{$item->sname}}</td>
                                                            <td>{{$item->email}}</td>
                                                            <td>{{count($item->Images)}}</td>
                                                            <td>
                                                                @if($item->status == 'Issue')
                                                                    <span class="label label-danger">
                                                                            {{$item->status}}
                                                                        </span>
                                                                @elseif($item->status == 'Pending')
                                                                    <span class="label label-warning">
                                                                            {{$item->status}}
                                                                        </span>
                                                                @elseif($item->status == 'Completed')
                                                                    <span class="label label-success">
                                                                            {{$item->status}}
                                                                        </span>
                                                                @else
                                                                    <span class="label label-primary">
                                                                        {{$item->status}}
                                                                    </span>
                                                                @endif
                                                            </td>
                                                            <td>{{$item->amount}}</td>
                                                            <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at)->toDateTimeString()}}</td>
                                                            <td>
                                                                <a href="{{url('view-delivery-detail/' . $item->did)}}">
                                                                    <span class="label label-success">View</span>
                                                                </a>
                                                            </td>
                                                        </tr>

                                                    @endforeach

                                                    </tbody>

                                                    <tfoot>
                                                    <tr>
                                                        <td colspan="6">
                                                            <div class="text-right">
                                                                <ul class="pagination">
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tfoot>
                                                </table>


                                            </div>
                                            <!-- end tab 3 -->

                                            <!-- start tab 4 -->
                                            <div class="tab-pane" id="balance">

                                                <table class="table table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th>Amount</th>
                                                        <th>Reason</th>
                                                        <th>Updated By</th>
                                                        <th>Created</th>
                                                        <th> </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    @foreach(collect($customer->BalanceUpdates)->reverse() as $item)
                                                        <tr>
                                                            <td>{{$item->amount}}</td>
                                                            <td>{{$item->reason}}</td>
                                                            <td>{{$item->User->fname}} {{$item->User->sname}}</td>
                                                            <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at)->toDateTimeString()}}</td>
                                                            <td>
                                                                <a href="{{url('edit-balance-update/' . $item->cbid)}}">
                                                                    <span class="label label-success">Edit</span>
                                                                </a>
                                                            </td>
                                                        </tr>

                                                    @endforeach

                                                    </tbody>
                                                </table>


                                                <div class="panel row">
                                                    <div class="panel-heading">Update Balance</div>

                                                    <div class="panel-body col-md-8 col-md-offset-2">
                                                        <form method="post" action="{{url('update-customer-balance')}}">
                                                            {{csrf_field()}}
                                                            <input type="hidden" name="cid" value="{{$customer->cid}}">
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label"> Amount </label>
                                                                <div class="col-md-6">
                                                                    <input type="number" class="form-control" name="amount" required autofocus>
                                                                </div>
                                                            </div> <br><br>

                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label">Reason</label>

                                                                <div class="col-md-6">
                                                                    <textarea placeholder="Enter reason for the update" class="form-control" name="reason" required></textarea>
                                                                </div>
                                                            </div> <br><br>

                                                            <div class="form-group" >
                                                                <div class="col-md-10 col-md-offset-4">
                                                                    <button class="btn btn-success">Add</button>
                                                                </div>
                                                            </div>

                                                        </form>

                                                    </div>
                                                </div>

                                            </div>
                                            <!-- end tab 3 -->


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection