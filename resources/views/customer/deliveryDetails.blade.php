
<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">

                        @include('notification')

                        <!-- .row -->
                            <div class="row bg-title" >
                                <div class="col-lg-12">
                                    <h3>Delivery #{{$delivery->did}}</h3>
                                </div>
                            </div>

                            <div class="row">
                                <div class=" col-md-12 col-xs-12 ">
                                    <div class="white-box">
                                        <ul class="nav customtab nav-tabs" role="tablist">

                                            <li role="presentation" class="nav-item"><a href="#basic" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Basic</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#images" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-image"></i></span> <span class="hidden-xs">Images <span class="badge badge-info">{{count($delivery->Images)}}</span></span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#assign" class="nav-link" aria-controls="assign" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-image"></i></span> <span class="hidden-xs">Assign</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#update" class="nav-link" aria-controls="update" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-image"></i></span> <span class="hidden-xs">Updates <span class="badge badge-info">{{count($delivery->Updates)}}</span></span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#actions" class="nav-link" aria-controls="actions" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Actions</span></a></li>
                                            @if(isset($delivery->Dropper))
                                                <li role="presentation" class="nav-item"><a href="#tracking" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-map"></i></span> <span class="hidden-xs">Tracking</span></a></li>
                                            @endif

                                        </ul>
                                        <div class="tab-content">

                                            <!-- start tab 1 -->
                                            <div class="tab-pane active" id="basic">
                                                <div class="row">
                                                    <div class="col-md-3 col-xs-6 b-r"> <strong>From</strong>
                                                        <br>
                                                        <p class="text-muted">
                                                            <a target="_blank" href="https://www.google.com/maps/dir/?api=1&origin={{$delivery->fromLat}},{{$delivery->fromLng}}&destination={{$delivery->toLat}},{{$delivery->toLng}}">

                                                                @if($delivery->from == "Your current location")
                                                                    Customer's Location
                                                                @else
                                                                {{$delivery->from}}
                                                                @endif
                                                            </a>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6 b-r">
                                                        <strong>To</strong>
                                                        <br>
                                                        <p class="text-muted">
                                                            <a target="_blank" href="https://www.google.com/maps/dir/?api=1&origin={{$delivery->fromLat}},{{$delivery->fromLng}}&destination={{$delivery->toLat}},{{$delivery->toLng}}">
                                                                {{$delivery->to}}
                                                            </a>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6 b-r">
                                                        <strong>Requested on</strong>
                                                        <br>
                                                        <p class="text-muted">{{Carbon::createFromFormat("Y-m-d H:i:s", $delivery->created_at)->toDayDateTimeString()}}</p>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6 b-r">
                                                        <strong>Vehicle</strong>
                                                        <br>
                                                        <p class="text-muted">
                                                            @if(isset($delivery->VehicleCategory))
                                                                {{$delivery->VehicleCategory->name}}
                                                            @endif
                                                        </p>
                                                    </div>

                                                    <div class="col-md-3 col-xs-6 b-r">
                                                        <strong>Bill</strong>
                                                        <br>
                                                        <p class="text-muted">NGN {{$delivery->amount}}</p>
                                                    </div>
                                                    @if(isset($delivery->Dropper))
                                                        <div class="col-md-3 col-xs-6 b-r">
                                                            <strong>Dropper</strong>
                                                            <br>
                                                            <p class="text-muted">
                                                                <a href="{{url('dropper/'. $delivery->Dropper->cid)}}">
                                                                {{$delivery->Dropper->Customer->fname}} {{$delivery->Dropper->Customer->sname}}
                                                                </a>
                                                            </p>
                                                        </div>
                                                    @endif

                                                    @if(isset($delivery->ditid))
                                                        <div class="col-md-3 col-xs-6 b-r">
                                                            <strong>Item Category</strong>
                                                            <br>
                                                            <p class="text-muted">{{$delivery->Items->Category->name}}</p>
                                                        </div>
                                                    @endif

                                                    @if(isset($delivery->ditid))
                                                        <div class="col-md-3 col-xs-6 b-r">
                                                            <strong>Item Type</strong>
                                                            <br>
                                                            <p class="text-muted">{{$delivery->Items->name}}</p>
                                                        </div>
                                                    @endif

                                                    @if(isset($delivery->pickedUpAt))
                                                        <div class="col-md-3 col-xs-6 b-r">
                                                            <strong>Picked Up</strong>
                                                            <br>
                                                            <p class="text-muted">{{Carbon::createFromFormat("Y-m-d H:i:s",$delivery->pickedUpAt)->toDayDateTimeString()}}</p>
                                                        </div>

                                                    @endif
                                                    @if(isset($delivery->droppedOffAt))
                                                        <div class="col-md-3 col-xs-6 b-r">
                                                            <strong>Dropped Off</strong>
                                                            <br>
                                                            <p class="text-muted">{{Carbon::createFromFormat("Y-m-d H:i:s",$delivery->droppedOffAt)->toDayDateTimeString()}}</p>
                                                        </div>
                                                    @endif

                                                    @if(isset($delivery->returnedAt))
                                                        <div class="col-md-3 col-xs-6 b-r">
                                                            <strong>Dropper Returned At</strong>
                                                            <br>
                                                            <p class="text-muted">{{Carbon::createFromFormat("Y-m-d H:i:s",$delivery->returnedAt)->toDayDateTimeString()}}</p>
                                                        </div>
                                                    @endif


                                                    <div class="col-md-12 col-xs-12 b-r">
                                                        <strong>Description</strong>
                                                        <br>
                                                        <p class="text-muted">
                                                            {{$delivery->description}}
                                                        </p>
                                                    </div>


                                                </div>
                                                <hr>
                                                @if($delivery->deliveryType == "return")
                                                    <label class="label label-success">Return Request</label>
                                                @endif

                                                @if($delivery->deliveryType == "sender")
                                                    <label class="label label-success">Requested By Sender</label>
                                                @endif
                                                @if($delivery->deliveryType == "receiver")
                                                    <label class="label label-success">Requested By Receiver</label>
                                                @endif

                                                @if($delivery->deliveryType == "sender")
                                                    <h3>SENDER's DETAILS</h3>
                                                @endif
                                                @if($delivery->deliveryType == "receiver")
                                                    <h3>RECEIVER's DETAILS</h3>
                                                @endif

                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Phone</th>
                                                        <th>Email</th>
                                                        <th>Payment</th>
                                                        <th>Status</th>
                                                        <th>Transaction ID</th>

                                                    </tr>


                                                    <tr>
                                                        <td>
                                                            <a href="{{url('view-customer/'. $delivery->cid)}}">
                                                                {{$delivery->Customer->fname}} {{$delivery->Customer->sname}}
                                                            </a>
                                                        </td>
                                                        <td>
                                                           @if(isset($delivery->Dropper))
                                                            <a href="https://api.whatsapp.com/send?phone={{$delivery->Dropper->Customer->phone}}&text={{$delivery->Customer->fname . "%20-%20" . $delivery->Customer->phone}}">
                                                                {{$delivery->Customer->phone}}
                                                            </a>
                                                           @else
                                                               {{$delivery->Customer->phone}}
                                                           @endif
                                                        </td>
                                                        <td>{{$delivery->Customer->email}}</td>
                                                        <td>
                                                            @if($delivery->paymentStatus == "paid")
                                                                <label class="label label-success">Paid</label>
                                                            @endif

                                                            @if($delivery->paymentStatus == "unpaid")
                                                                <label class="label label-danger">Un-Paid</label>
                                                            @endif

                                                        </td>
                                                        <td>

                                                            @if($delivery->status == "Pending")
                                                                <label class="label label-warning">Pending</label>
                                                            @endif

                                                            @if($delivery->status == "Complete")
                                                                <label class="label label-success">Complete</label>
                                                            @endif

                                                            @if($delivery->status == "Available")
                                                                <label class="label label-primary">Available</label>
                                                            @endif

                                                            @if($delivery->status == "Issue")
                                                                <label class="label label-danger">Issue</label>
                                                            @endif

                                                        </td>
                                                        <td>
                                                            #{{str_pad( $delivery->did, 6, "0", STR_PAD_LEFT)}}
                                                        </td>
                                                    </tr>

                                                </table>


                                                <hr>

                                                @if($delivery->deliveryType == "sender")
                                                    <h3>RECEIVER's DETAILS</h3>
                                                @endif
                                                @if($delivery->deliveryType == "receiver")
                                                    <h3>SENDERS's DETAILS</h3>
                                                @endif

                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Phone</th>
                                                        <th>Email</th>
                                                    </tr>


                                                    <tr>
                                                        <td>
                                                            {{$delivery->fname}} {{$delivery->sname}}
                                                        </td>
                                                        <td>
                                                            @if(isset($delivery->Dropper))
                                                                <a href="https://api.whatsapp.com/send?phone={{$delivery->Dropper->Customer->phone}}&text={{$delivery->fname . "%20-%20" . $delivery->phone}}">
                                                                    {{$delivery->phone}}
                                                                </a>
                                                            @else
                                                                {{$delivery->phone}}
                                                            @endif

                                                        </td>
                                                        <td>{{$delivery->email}}</td>
                                                    </tr>

                                                </table>


                                            </div>

                                            <!-- start tab 2 -->
                                            <div class="tab-pane" id="images">
                                                @foreach($delivery->Images as $image)
                                                <div class="col-md-3">
                                                    <img src="{{$image->url}}" class="img img-thumbnail">
                                                </div>
                                                @endforeach
                                            </div>

                                            <!-- start tab 3 -->

                                            <div class="tab-pane" id="tracking">


                                                <div id="map" style="height:300px !important;width:100% !important;"></div>


                                                @if(isset($delivery->Dropper))
                                                    <script>

                                                        document.getElementById("trackingTrigger").addEventListener("click",initMap);

                                                        function initMap(){
                                                            console.log('loading Map');
                                                            console.log("{{url('api/dropper-location')}}/{{$delivery->Dropper->drid}}");
                                                            var map;
                                                            var marker;

                                                            var latLng = new google.maps.LatLng(9.070400, 7.487462);

                                                            map = new google.maps.Map(document.getElementById('map'), {
                                                                center: latLng,
                                                                zoom: 15,
                                                                tilt: 30,
                                                                mapTypeControl: false,
                                                                clickableIcons: false
                                                            });

                                                            marker = new google.maps.Marker({
                                                                position: latLng,
                                                                title: 'Your dropper is here!'
                                                            });

                                                            marker.setMap(map);
                                                            map.setCenter(latLng);


                                                            setInterval(function(){
                                                                console.log('updating dropper location');

                                                                $.ajax({
                                                                    url:"{{url('api/dropper-location')}}/{{$delivery->Dropper->drid}}",
                                                                    method: "get",
                                                                    success: function (success) {

                                                                        var latLng = new google.maps.LatLng( success.lat , success.lng);

                                                                        marker.setPosition(latLng);
                                                                        map.setCenter(latLng);

                                                                    },
                                                                    error: function (error) {
                                                                        console.log(error);
                                                                    }

                                                                });
                                                            },15000);


                                                        }


                                                    </script>

                                                @endif
                                            </div>

                                            <!-- end tab 3 -->


                                            <!-- start tab 4 -->
                                            <div class="tab-pane" id="assign">





                                                <form method="post" action="{{url('assign-delivery')}}">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="did" value="{{$delivery->did}}">


                                                    <select class="form-control" name="drid">
                                                        @foreach($droppers as $dropper)
                                                            <option value="{{$dropper->drid}}">{{$dropper->Customer->fname}} {{$dropper->Customer->sname}} - {{$dropper->Vehicle->VehicleCategory->name}} - {{$dropper->Vehicle->brand}} {{$dropper->Vehicle->model}} ({{$dropper->Vehicle->regno}})</option>
                                                        @endforeach
                                                    </select>
                                                    <br><br>



                                                    <button class="btn btn-success">Assign</button>
                                                </form>
                                            </div>

                                            <!-- end tab 4  -->


                                            <!-- start tab 5 -->
                                            <div class="tab-pane" id="update">


                                                <table class="table table-bordered table-hover">
                                                    <tr>
                                                        <th>Update</th>
                                                        <th>Updated By</th>
                                                        <th>Created</th>
                                                    </tr>
                                                    @foreach($updates as $update)
                                                    <tr>

                                                        <td>
                                                            {{$update->description}}
                                                        </td>

                                                        <td>
                                                            {{$update->Staff->fname}} {{$update->Staff->sname}}
                                                        </td>

                                                        <td>
                                                           {{$update->created_at->toDayDateTimeString()}}
                                                        </td>

                                                    </tr>
                                                        @endforeach

                                                </table>
                                                <hr>
                                                <h2>New Update</h2>


                                                <form method="post" action="{{url('send-updates')}}">
                                                    {{csrf_field()}}
                                                    <input type="hidden" name="did" value="{{$delivery->did}}">

                                                    <textarea class="form-control" name="description" placeholder="Please enter an update for the delivery"></textarea>

                                                    <br><br>

                                                    <button class="btn btn-success">Update</button>
                                                </form>

                                            </div>
                                            <!-- end tab 5  -->

                                            <!-- start tab 6 -->

                                            <div class="tab-pane" id="actions">

                                                <div id="alert" class="hidden alert alert-danger">Sorry and error occurred. Try again</div>
                                                <div id="success" class="hidden alert alert-success">Successful</div>

                                                <div id="loading" class="hidden"><i class="fa fa-spin fa-spinner"></i> </div>

                                                @if($delivery->paymentStatus == 'unpaid')

                                                <form id="paymentForm" class="form-inline">
                                                    <select id="paymentMethod" class="form-control">
                                                        <option value="cash">Cash</option>
                                                        <option value="pos">POS</option>
                                                    </select> &nbsp;
                                                    <a id="paymentCollected" class="btn btn-success">Payment Collected</a>
                                                </form>
                                                @endif

                                                <br>
                                                @if(!isset($delivery->pickedUpAt))
                                                <a id="itemPickedUp" style="color:white" class="label label-success">Item Picked Up</a>
                                                @endif

                                                @if(isset($delivery->pickedUpAt) && !isset($delivery->droppedOffAt))
                                                <a id="itemDroppedOff" style="color:white" class="label label-success">Item Dropped Off</a>
                                                @endif


                                                <br><br>


                                                <fieldset>
                                                    <legend>Price Update</legend>

                                                    <form class="form-inline" method="post" action="{{url('price-update')}}">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="did" value="{{$delivery->did}}">
                                                        <label>Modify By (NGN):</label> &nbsp;
                                                        <input  type="number" name="amount" class="form-control" style="width: 100px;">
                                                        &nbsp;&nbsp;
                                                        <label>Reason:</label> &nbsp;
                                                        <input  type="text" name="description" class="form-control" style="width: 200px;">&nbsp;&nbsp;
                                                        <button  class="btn btn-success">Update Price</button>
                                                    </form>

                                                    <br>
                                                    <table class="table table-bordered table-hover">
                                                        <tr>
                                                            <th>Amount</th>
                                                            <th>Reason</th>
                                                            <th>Updated By</th>
                                                            <th>Time Updated</th>
                                                        </tr>
                                                        @foreach($delivery->PriceUpdates as $update)
                                                            <tr>
                                                                <td>
                                                                    {{$update->amount}}
                                                                </td>

                                                                <td>
                                                                    {{$update->description}}
                                                                </td>

                                                                <td>
                                                                    {{$update->Staff->fname}} {{$update->Staff->sname}}
                                                                </td>

                                                                <td>
                                                                    {{$update->created_at->toDayDateTimeString()}}
                                                                </td>

                                                            </tr>
                                                        @endforeach

                                                    </table>


                                                </fieldset>

                                            </div>

                                            <script>
                                                $(document).ready(function () {
                                                    var alert = $('#alert');
                                                    var success = $('#success');
                                                    var loading = $('#loading');
                                                    var paymentCollected = $('#paymentCollected');
                                                    var itemPickedUp = $('#itemPickedUp');
                                                    var itemDroppedOff = $('#itemDroppedOff');

                                                    function reset() {
                                                        alert.addClass('hidden');
                                                        success.addClass('hidden');
                                                        loading.removeClass('hidden');
                                                    }

                                                    itemPickedUp.on('click',function () {
                                                        reset();

                                                        $.ajax({
                                                            url:"{{url('api/pickedup-delivery/' . $delivery->did )}}",
                                                            method: "get",
                                                            data:{},
                                                            success: function (response) {
                                                                console.log(response);
                                                                loading.addClass('hidden');
                                                                success.removeClass('hidden');
                                                                itemPickedUp.addClass('hidden');
                                                            },
                                                            error: function (error) {

                                                            }
                                                        });
                                                    });

                                                    itemDroppedOff.on('click',function () {
                                                        reset();

                                                        $.ajax({
                                                            url:"{{url('api/droppedoff-delivery/' . $delivery->did )}}",
                                                            method: "get",
                                                            data:{},
                                                            success: function (response) {
                                                                console.log(response);
                                                                loading.addClass('hidden');
                                                                success.removeClass('hidden');
                                                                itemDroppedOff.addClass('hidden');
                                                            },
                                                            error: function (error) {

                                                            }
                                                        });
                                                    });

                                                    paymentCollected.on('click',function () {
                                                        reset();
                                                        console.log($('#paymentMethod').val());
                                                        $.ajax({
                                                            url:"{{url('api/payment-collected')}}",
                                                            method: "post",
                                                            data:{
                                                                drid : "{{$delivery->drid}}",
                                                                did  : "{{$delivery->did}}",
                                                                paymentMethod : $('#paymentMethod').val()
                                                            },
                                                            success: function (response) {
                                                                loading.addClass('hidden');
                                                                console.log(response);
                                                                if(response == 1) {
                                                                    success.removeClass('hidden');
                                                                    $('#paymentForm').addClass('hidden');
                                                                } else {
                                                                    alert.removeClass('hidden');

                                                                }
                                                            },
                                                            error: function (error) {
                                                                loading.addClass('hidden');
                                                                console.log(error);
                                                                alert.removeClass('hidden');

                                                            }
                                                        });

                                                    });
                                                })
                                            </script>


                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection