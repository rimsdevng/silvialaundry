@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        @include('notification')
                        <div class="white-box" style="height: 100%;">

                            <div class="row col-md-12 col-sm-12 col-xs-12">
                                <div id="map" style="height:300px; !important;width:100% !important;"></div>
                                <div id="infowindow-content">
                                    <img src="" width="16" height="16" id="place-icon">
                                    <span id="place-name" class="title"></span><br>
                                    <span id="place-address" ></span> <br>
                                </div>
                            </div>


                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="panel panel-default">

                                        <div class="panel-body">

                                            <div id="loading" align="center" class="hidden"><i class="fa fa-spin fa-spinner"></i> </div>

                                            <p align="center" id="price" style="color:darkgreen;font-size: 24px"></p>

                                            <form style="width:100%;" role="form" method="POST" enctype="multipart/form-data" action="{{ url('customer-request') }}">
                                                    {{ csrf_field() }}

                                                <input type="hidden" name="fromLat" id="fromLat">
                                                <input type="hidden" name="fromLng" id="fromLng">
                                                <input type="hidden" name="toLat" id="toLat">
                                                <input type="hidden" name="toLng" id="toLng">
                                                <input type="hidden" name="amount" id="amount">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="role" class="col-md-4 control-label"> Customer Name </label>

                                                        <div class="col-md-6">

                                                            <select class="selectpicker" data-live-search="true" name="cid">
                                                                @foreach($customers as $customer)
                                                                    <option value="{{$customer->cid}}"> {{$customer->fname}} {{$customer->sname}}({{$customer->phone}})</option>
                                                                @endforeach
                                                            </select>

                                                        </div>
                                                    </div>
                                                    <br>

                                                    <div class="form-group">
                                                        <label for="description" class="col-md-4 control-label">Pick up Location</label>

                                                        <div class="col-md-6">

                                                            <input data-type="from" onkeyup="autoComplete(this)" id="from" required class="form-control" placeholder="pick up location" name="from">
                                                        </div>
                                                    </div>
                                                    <br>

                                                    <div class="form-group">
                                                        <label for="description" class="col-md-4 control-label">Drop off Location</label>

                                                        <div class="col-md-6">
                                                            <input data-type="to" onkeyup="autoComplete(this)" id="to" required class="form-control" placeholder="Drop off location" name="to">
                                                        </div>

                                                    </div>
                                                    <br>

                                                    <div class="form-group">
                                                        <label for="role" class="col-md-4 control-label"> Delivery Type </label>
                                                        <div class="col-md-6">

                                                            <select id="deliveryType" class="form-control" name="deliveryType">
                                                                <option value="sender">Requester is sending the item</option>
                                                                <option value="receiver">Requester is to receive the item</option>
                                                                <option value="return">Return request (two-way)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br>


                                                </div>

                                                <div class="col-md-6">

                                                    <div class="form-group">
                                                        <label for="role" class="col-md-4 control-label"> Vehicle Type </label>
                                                        <div class="col-md-6">

                                                            <select class="form-control" id="vcid" name="vcid">
                                                                @foreach($vcs as $vc)
                                                                    <option value="{{$vc->vcid}}"> {{$vc->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br>

                                                    <div class="form-group">
                                                        <label for="role" class="col-md-4 control-label"> Item Type</label>


                                                        <div class="col-md-6">


                                                            <select class="selectpicker" data-live-search="true" name="ditid">
                                                                @foreach($itemCats as $item)
                                                                    <optgroup label="{{$item->name}}">
                                                                        @foreach($item->Items as $singleItem)
                                                                            <option value="{{$singleItem->ditid}}">{{$singleItem->name}}</option>
                                                                        @endforeach
                                                                    </optgroup>
                                                                @endforeach
                                                            </select>

                                                        </div>
                                                    </div>
                                                    <br>

                                                    <div class="form-group">
                                                        <label for="description" class="col-md-4 control-label">Instructions</label>

                                                        <div class="col-md-6">
                                                            <textarea style="margin: 10px 0;" placeholder="Enter instructions from the customer" name="description"></textarea>
                                                        </div>

                                                    </div>
                                                    <br>

                                                    <div class="form-group">
                                                        <label for="role" class="col-md-4 control-label"> Payment Method </label>
                                                        <div class="col-md-6">

                                                            <select class="form-control" id="paymentMethod" name="paymentMethod">
                                                                <option value="cash">Cash</option>
                                                                <option value="balance">Balance</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <br>

                                                </div>

                                                <!-- details for sender and receiver -->
                                                <div>
                                                    <div id="sender" class="col-md-6">
                                                        <div class="col-md-12">
                                                            <h4>Sender's Details</h4>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label">Sender's Firstname</label>

                                                            <div class="col-md-6">
                                                                <input id="senderFname" class="form-control" placeholder="Sender's firstame" name="senderFname">
                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label">Sender's Surname</label>

                                                            <div class="col-md-6">
                                                                <input class="form-control" placeholder="Sender's surname (Optional)" name="senderSname">
                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <label for="description" class="col-md-6 control-label">Sender's Phone</label>

                                                            <div class="col-md-6">
                                                                <input id="senderPhone" class="form-control" placeholder="Sender's phone number" name="senderPhone">
                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label">Sender's Email</label>

                                                            <div class="col-md-6">
                                                                <input class="form-control" placeholder="Sender's email (Optional)" name="senderEmail">
                                                            </div>

                                                        </div>

                                                    </div>
                                                    <div id="receiver" class="col-md-6">
                                                        <div class="col-md-12">
                                                            <br>
                                                            <h4>Receivers's Details</h4>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label">Receivers's Firstname</label>

                                                            <div class="col-md-6">
                                                                <input id="receiverFname" class="form-control" placeholder="Receivers's firstame" name="receiverFname" >
                                                            </div>
                                                        </div>
                                                        <br>

                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label">Receivers's Surname</label>

                                                            <div class="col-md-6">
                                                                <input class="form-control" placeholder="Receivers's surname (Optional)" name="receiverSname">
                                                            </div>

                                                        </div>
                                                        <br>

                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label">Receivers's Phone</label>

                                                            <div class="col-md-6">
                                                                <input id="receiverPhone" class="form-control" placeholder="Receivers's phone number" name="receiverPhone">
                                                            </div>

                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-6 control-label">Receivers's Email</label>

                                                            <div class="col-md-6">
                                                                <input class="form-control" placeholder="Receivers's email (Optional)" name="receiverEmail">
                                                            </div>

                                                        </div>


                                                    </div>
                                                </div>



                                                <div class="form-group" >
                                                    <div class="col-md-10 col-md-offset-4">

                                                        <a href="{{url('/')}}" style="margin-left:10px;" class="btn btn-warning pull-right">Back</a>
                                                        <button type="submit" class="btn btn-primary pull-right">
                                                            Request
                                                        </button>
                                                    </div>
                                                </div>

                                            </form>



                                        </div>
                                    </div>
                                </div>

                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>




    <script>
        var map,lastfromLat,lastfromLng;

        $(document).ready(function () {

            //clear location history
                localStorage.removeItem('from-place-lat');
                localStorage.removeItem('from-place-lng');
                localStorage.removeItem('to-place-lng');
                localStorage.removeItem('to-place-lng');

            var mapView = document.getElementById('map');
            var latLng = new google.maps.LatLng(9.070400, 7.487462);

            var mapOptions = {
                center: latLng,
                zoom: 15,
                tilt:30,
                disableDefaultUI:false,
                mapTypeControl:false,
                zoomControl:true,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                clickableIcons:false
            };

            map = new google.maps.Map(mapView, mapOptions);

            //update price when type of vehicle changes
            var vcid = $('#vcid');
            vcid.on('change',function () {
                checkPrice();
            });

            // hide sender or receiver form appropriately
            var deliveryType = $('#deliveryType');
            handleDeliveryType();
            deliveryType.on('change',handleDeliveryType);
        });

        function handleDeliveryType() {
            var deliveryType = $('#deliveryType');
            var sender = $('#sender');
            var receiver = $('#receiver');
            var senderFname = $('#senderFname');
            var senderPhone = $('#senderPhone');
            var receiverFname = $('#receiverFname');
            var receiverPhone = $('#receiverPhone');

            if(deliveryType.val() === 'sender' || deliveryType.val() === 'return' ){
                receiver.removeClass("hidden");
                sender.addClass("hidden");
                senderFname.prop("required",false);
                senderPhone.prop("required",false);
                receiverFname.prop("required",true);
                receiverPhone.prop("required",true);
            }

            if(deliveryType.val() === 'receiver'){
                sender.removeClass("hidden");
                receiver.addClass("hidden");

                receiverFname.prop("required",false);
                receiverPhone.prop("required",false);

                senderFname.prop("required",true);
                senderPhone.prop("required",true);

            }


        }

        function isNumber(item) {
            return !isNaN(item) || item !== null;
        }

        function autoComplete(input) {

            var str = input.value;
            var prefix = 'Abuja, ';
            if(str.indexOf(prefix) == 0) {

            } else {
                if (prefix.indexOf(str) >= 0) {
                    input.value = prefix;
                } else {
                    input.value = prefix+str;
                }
            }

            var options = {
                language: 'en-US',
                // types: ['(cities)'],
                componentRestrictions: { country: "ng" }
            };

            var autocomplete = new google.maps.places.Autocomplete(input,options);
            autocomplete.bindTo('bounds', map);


            var infowindow = new google.maps.InfoWindow();
            var infowindowContent = document.getElementById('infowindow-content');
            infowindow.setContent(infowindowContent);
            var marker = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
            });



            autocomplete.addListener('place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    // User entered the name of a Place that was not suggested and
                    // pressed the Enter key, or the Place Details request failed.

                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                }
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || ''),
                        (place.address_components[3] && place.address_components[3].short_name || '')
                    ].join(' ');
                }


                infowindowContent.children['place-icon'].src = place.icon;
                infowindowContent.children['place-name'].textContent = place.name;
                infowindowContent.children['place-address'].textContent = address;

                if(input.dataset.type === "from"){
                    localStorage.setItem("from-place-lat",place.geometry.viewport.f.f);
                    localStorage.setItem("from-place-lng",place.geometry.viewport.b.b);

                }

                if(input.dataset.type === "to"){
                    localStorage.setItem("to-place-lat",place.geometry.viewport.f.f);
                    localStorage.setItem("to-place-lng",place.geometry.viewport.b.b);

                }


                input.setAttribute("data-place-lat", place.geometry.viewport.f.f);
                input.setAttribute("data-place-lng", place.geometry.viewport.b.b);
                infowindow.open(map, marker);

                setTimeout(checkPrice(),500);

            });

        }

        function checkPrice(){

            if(localStorage.getItem('from-place-lat') > 0 &&
               localStorage.getItem('from-place-lng') > 0 &&
               localStorage.getItem('to-place-lng') > 0 &&
               localStorage.getItem('to-place-lng') > 0 ){

                var fromLat = $('#fromLat');
                var fromLng = $('#fromLng');
                var toLat   = $('#toLat');
                var toLng  = $('#toLng');

                // make it show loading and clear price with new search
                var loading = $('#loading');
                loading.removeClass("hidden");
                $('#price').text("");

                $.ajax({
                    url:"{{url('api/price')}}",
                    method: "post",
                    data:{
                        _token : "{{csrf_token()}}",
                        fromLat : localStorage.getItem('from-place-lat'),
                        fromLng : localStorage.getItem('from-place-lng'),
                        toLat : localStorage.getItem('to-place-lat'),
                        toLng : localStorage.getItem('to-place-lng'),
                        vcid : document.getElementById('vcid').value
                    },
                    success: function (response) {

                        loading.addClass("hidden");

                        fromLat.val(localStorage.getItem('from-place-lat'));
                        fromLng.val(localStorage.getItem('from-place-lng'));
                        toLat.val(localStorage.getItem('to-place-lat'));
                        toLng.val(localStorage.getItem('to-place-lng'));

                        $('#amount').val(response);
                        $('#price').text("NGN" + response);

                    },
                    error: function (error) {
                        loading.addClass("hidden");
                        $('#price').text("");
                        console.log(error);
                    }
                });

            }


        }


    </script>

@endsection


