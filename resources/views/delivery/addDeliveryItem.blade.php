@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">

                        @include('notification')
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Add Delivery Item</div>


                                        <div class="panel-body">
                                            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ url('/delivery-items') }}">
                                                {{ csrf_field() }}

                                                    <div class="form-group">
                                                        <label for="role" class="col-md-4 control-label">Category</label>

                                                        <div class="col-md-6">
                                                            <select class="form-control" name="icid">

                                                               @foreach($itemCat as $item)
                                                                    <option value="{{$item->icid}}">{{$item->name}}</option>
                                                               @endforeach



                                                            </select>
                                                        </div>
                                                    </div>


                                                <div class="form-group">
                                                    <label for="role" class="col-md-4 control-label"> Name </label>


                                                    <div class="col-md-6">
                                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                                        @if ($errors->has('name'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('name') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-10 col-md-offset-4">
                                                        <button type="submit" class="btn btn-primary pull-right">
                                                            Add Delivery Item
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

@endsection


