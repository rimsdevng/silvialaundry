@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        @include('notification')
                        <div class="white-box">

                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">
                                    <div class="panel panel-default">
                                        @if(!isset($category))
                                        <div class="panel-heading">Add Item Category</div>
                                        @endif

                                        @if(isset($category))
                                            <div class="panel-heading">Edit Item Category</div>
                                        @endif


                                        <div class="panel-body">

                                            @if(!isset($category))
                                            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ url('/item-cat') }}">
                                                {{ csrf_field() }}


                                                <div class="form-group">
                                                    <label for="role" class="col-md-4 control-label"> Name </label>


                                                    <div class="col-md-6">
                                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                                    </div>
                                                </div>



                                                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                                    <label for="description" class="col-md-4 control-label">Description</label>

                                                    <div class="col-md-6">
                                                        <textarea id="description"  class="form-control" name="description" required>{{old('description')}}</textarea>
                                                    </div>


                                                </div>



                                                <div class="form-group" >
                                                    <div class="col-md-10 col-md-offset-4">

                                                        <a href="{{url('/view-item-cat')}}" style="margin-left:10px;" class="btn btn-warning pull-right">Back</a>
                                                        <button type="submit" class="btn btn-primary pull-right">
                                                            Add Category
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                            @endif

                                            @if(isset($category))
                                                <form class="form-horizontal" role="form" method="POST" action="{{ url('/edit-item-cat') }}">
                                                    {{ csrf_field() }}

                                                    <input type="hidden" value="{{$category->icid}}" name="icid">

                                                    <div class="form-group">
                                                        <label for="role" class="col-md-4 control-label"> Name </label>


                                                        <div class="col-md-6">
                                                            <input id="name" type="text" class="form-control" name="name" value="{{ $category->name  }}" required autofocus>
                                                        </div>
                                                    </div>



                                                    <div class="form-group">
                                                        <label for="description" class="col-md-4 control-label">Description</label>

                                                        <div class="col-md-6">
                                                            <textarea id="description"  class="form-control" name="description" required>{{ $category->description }}</textarea>
                                                        </div>
                                                    </div>

                                                    <div class="form-group" >
                                                        <div class="col-md-10 col-md-offset-4">
                                                            <a href="{{url('/view-item-cat')}}" style="margin-left:10px;" class="btn btn-warning pull-right">Back</a>
                                                            <button type="submit" class="btn btn-primary pull-right">
                                                                Save
                                                            </button>
                                                        </div>

                                                    </div>
                                                </form>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>

@endsection


