@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Delivery Items</h3>
                            <table id="demo-foo-addrow" class="table table-bordered table-hover toggle-circle" data-page-size="7">
                                <thead>
                                <tr>
                                    <th data-sort-initial="true" data-toggle="true">Item Name</th>
                                    <th data-toggle="true">Category</th>

                                </tr>
                                </thead>
                                <div class="form-inline padding-bottom-15">
                                    <div class="row">
                                    </div>
                                </div>

                                <tbody>

                                @foreach($items as $item)
                                    <tr>
                                        <td>{{$item->name}}</td>

                                        <td>

                                            {{$item->Category->name}}
                                            {{--<a href="{{url('dropper/' . $item->cid)}}">--}}
                                                {{--<span class="label label-success">View Profile</span>--}}
                                            {{--</a>--}}


                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>

                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <div class="text-right">
                                            <ul class="pagination">
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection