<?php use Illuminate\Support\Facades\Input; use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">DELIVERY REQUESTS
                                @if( Input::get('by') == "Issue")
                                    <span class="label label-success">Filtered by Issues</span>
                                @endif
                                @if( Input::get('by') == "Complete")
                                    <span class="label label-success">Filtered by Completed</span>
                                @endif
                                @if( Input::get('by') == "Available")
                                    <span class="label label-success">Filtered by Availability</span>
                                @endif
                                @if( Input::get('by') == "Pending")
                                    <span class="label label-success">Filtered by Pending</span>
                                @endif
                                @if( Input::get('by') == "Cancelled")
                                    <span class="label label-success">Filtered by Cancelled</span>
                                @endif


                            </h3>

                             <div class="col-md-5">
                        <form class="form-inline" style="padding-top: 14px">
                            <label>Filter Requests By: </label>

                                <div class="row">
                                <div class="col"></div>
                                </div>

                            <select name="by" class="form-control">
                                <option value=""> All </option>
                                <option value="Available"
                                @if( Input::get('by') == "Available")
                                    selected
                                @endif
                                >Available</option>

                                <option value="Complete"
                                @if( Input::get('by') == "Complete")
                                selected
                                @endif
                                >Complete</option>
                                <option value="Pending"
                                @if( Input::get('by') == "Pending")
                                selected
                                @endif
                                >Pending</option>
                                <option value="Issue"
                                @if( Input::get('by') == "Issue")
                                selected
                                @endif
                                >Issue</option>
                                <option value="Cancelled"
                                        @if( Input::get('by') == "Cancelled")
                                        selected
                                        @endif
                                >Cancelled</option>

                            </select>

                            <button class="btn btn-primary">GO</button>
                            <br>
                        </form>
                    </div>


                            <table id="demo-foo-addrow" class="table table-hover " data-page-size="7">
                                <thead>
                                <tr>
                                    <th>DID</th>
                                    <th data-toggle="true">Requested By</th>
                                    <th>To Be Received By</th>
                                    <th data-hide="phone, tablet">Dropper</th>
                                    <th data-hide="phone, tablet">Sender's Phone</th>
                                    <th data-hide="phone, tablet">Amount</th>
                                    <th data-hide="phone, tablet">Payment</th>
                                    <th>Status</th>
                                    <th>Created</th>
                                    <th data-sort-ignore="true" class="min-width"></th>
                                </tr>
                                </thead>
                                <div class="form-inline padding-bottom-15">
                                    <div class="row">

                                    </div>
                                    <div class="col-sm-12 text-right m-b-20 pull-right">
                                        <div class="form-group">
                                            <input id="demo-input-search2" type="text" placeholder="Search" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <tbody>

                                @foreach($requests as $item)
                                    <tr>
                                        <td>{{$item->did}}</td>
                                        <td>
                                            <a href="{{url('view-customer/'. $item->cid)}}">
                                                {{$item->Customer->fname}} {{$item->Customer->sname}}
                                            </a>
                                        </td>
                                        <td>{{$item->fname}} {{$item->sname}}</td>
                                        <td>
                                            @if(isset($item->Dropper))
                                                <a href="{{url('dropper/'. $item->Dropper->cid)}}">
                                                    {{$item->Dropper->Customer->fname}} {{$item->Dropper->Customer->sname}}
                                                </a>
                                                @else
                                                Not Accepted Yet
                                            @endif

                                        </td>
                                        <td>{{$item->Customer->phone}}</td>
                                         <td>{{$item->amount}}</td>
                                        <td>
                                            @if($item->paymentStatus == "paid")
                                                <label class="label label-success">Paid</label>
                                            @endif

                                            @if($item->paymentStatus == "unpaid")
                                                <label class="label label-danger">Un-Paid</label>
                                            @endif

                                        </td>
                                        <td>
                                            @if($item->status == "Pending")
                                                <label class="label label-warning">Pending</label>
                                            @endif

                                            @if($item->status == "Complete")
                                                <label class="label label-success">Complete</label>
                                            @endif

                                            @if($item->status == "Available")
                                                <label class="label label-primary">Available</label>
                                            @endif

                                            @if($item->status == "Issue")
                                                <label class="label label-danger">Issue</label>
                                            @endif

                                            @if($item->status == "Cancelled")
                                                <label class="label label-default" style="background-color: saddlebrown">Cancelled</label>
                                            @endif

                                        </td>
                                        <td>
                                            {{Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at)->diffForHumans()}}
                                        </td>
                                        <td>
                                            <a href="{{url('view-delivery-detail/' . $item->did)}}">
                                                <span class="label label-success">View </span>
                                            </a>

                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>

                                <tfoot>
                                <tr>
                                    <td colspan="10">
                                        <div class="text-right">
                                            <ul class="pagination">
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection



