
<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">

                        @include('notification')

                        <!-- .row -->
                            <div class="row bg-title" >
                                <div class="col-lg-12">
                                </div>
                            </div>

                            <!-- .row -->
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="white-box">
                                        <div class="user-bg"> <img width="100%" alt="user" src="{{url('img/landscape17.jpg')}}">
                                            <div class="overlay-box">
                                                <div class="user-content">
                                                    <a href="javascript:void(0)"><img src="{{$customer->image}}" class="thumb-lg img-circle" alt="img"></a>
                                                    <h4 class="text-white">{{$customer->fname}} {{$customer->sname}}</h4>
                                                    <h5 class="text-white">Created {{Carbon::createFromFormat("Y-m-d H:i:s",$customer->Dropper->created_at)->toFormattedDateString()}} </h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="user-btm-box">
                                            <div class="col-md-4 col-sm-4 text-center">

                                            </div>
                                            <div class="col-md-4 col-sm-4 text-center">

                                            </div>
                                            <div class="col-md-4 col-sm-4 text-center">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <div class="white-box">
                                        <ul class="nav customtab nav-tabs" role="tablist">

                                            <li role="presentation" class="nav-item"><a href="#details" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Basic</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#history" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">History</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#transactions" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Transactions</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#remittances" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-money"></i></span> <span class="hidden-xs">Remittances</span></a></li>
                                            <li role="presentation" class="nav-item"><a id="trackingTrigger" href="#tracking" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-map"></i></span> <span class="hidden-xs">Tracking</span></a></li>
                                        </ul>
                                        <div class="tab-content">

                                            <!-- start tab 1 -->
                                            <div class="tab-pane active" id="details">

                                                <div class="row">
                                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Gender</strong>
                                                        <br>
                                                        <p class="text-muted">{{$customer->gender}}</p>
                                                    </div>

                                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Assigned</strong>
                                                        <br>
                                                        @if(isset($customer->Dropper->vid ))
                                                        <p class="text-muted">{{$customer->Dropper->Vehicle->brand}} - {{$customer->Dropper->Vehicle->regno}}</p>
                                                        @endif
                                                    </div>

                                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Un-Remitted Cash</strong>
                                                        <br>
                                                            <p class="text-muted">{{$customer->Dropper->unremittedCash}}</p>
                                                    </div>

                                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Un-Remitted POS</strong>
                                                        <br>
                                                        <p class="text-muted">
                                                            {{$customer->Dropper->unremittedPOS}}
                                                        </p>
                                                    </div>


                                                </div>
                                                <hr>



                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>First Name</th>
                                                    <th>Surname</th>
                                                    <th>Phone</th>
                                                    <th>Email</th>
                                                </tr>


                                                <tr>
                                                    <td>{{$customer->fname}}</td>
                                                    <td>{{$customer->sname}}</td>
                                                    <td>{{$customer->phone}}</td>
                                                    <td>{{$customer->email}}</td>
                                                </tr>

                                            </table>

                                        </div>

                                            <!-- start tab 2 -->
                                            <div class="tab-pane" id="transactions">
                                            <table class="table table-hover">

                                                <tr>
                                                    <th>Sender</th>
                                                    <th>Sender Phone</th>
                                                    <th>Receiver</th>
                                                    <th data-hide="phone, tablet">Receiver Phone</th>
                                                    <th data-hide="phone, tablet">Status</th>
                                                    <th>Item</th>
                                                    <th></th>
                                                </tr>


                                                @foreach($deliveries as $item)
                                                    <tr>
                                                        <td>{{$item->Customer->fname}} {{$item->Customer->sname}}</td>
                                                        <td>{{$item->Customer->phone}}</td>
                                                        <td>{{$item->fname}} {{$item->sname}}</td>
                                                        <td>{{$item->phone}}</td>
                                                        <td>
                                                            @if($item->status == "Pending")
                                                                <label class="label label-warning">Pending</label>
                                                            @endif

                                                            @if($item->status == "Complete")
                                                                <label class="label label-success">Complete</label>
                                                            @endif

                                                            @if($item->status == "Available")
                                                                <label class="label label-primary">Available</label>
                                                            @endif

                                                            @if($item->status == "Issue")
                                                                <label class="label label-danger">Issue</label>
                                                            @endif

                                                        </td>
                                                        <td>{{$item->Items->name}}</td>
                                                        <td>
                                                            <a href="{{url('/view-delivery-detail/' . $item->did)}}" class="label label-success">View</a>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            </table>
                                        </div>

                                            <!-- start tab 3  -->
                                            <div class="tab-pane" id="history">

                                            <table class="table table-hover">
                                                <tr>
                                                    <th>Vehicle</th>
                                                    <th>Time Un-assigned</th>
                                                </tr>


                                                @foreach($history as $item)
                                                    <tr>
                                                        <td>
                                                            {{$item->Vehicle->brand}} - {{$item->Vehicle->regno}}
                                                        </td>

                                                        <td>
                                                            {{$item->created_at}}
                                                        </td>

                                                    </tr>
                                                @endforeach

                                            </table>

                                        </div><br>

                                            <!-- start tab 4 -->

                                            <div class="tab-pane" id="tracking">


                                                <div id="map" style="height:300px !important;width:100% !important;"></div>


                                                <script>

                                                    document.getElementById("trackingTrigger").addEventListener("click",initMap);

                                                    function initMap(){
                                                        console.log('loading Map');
                                                        console.log("{{url('api/dropper-location')}}/{{$customer->Dropper->drid}}");
                                                        var map;
                                                        var marker;

                                                        var latLng = new google.maps.LatLng(9.070400, 7.487462);

                                                            map = new google.maps.Map(document.getElementById('map'), {
                                                            center: latLng,
                                                            zoom: 15,
                                                            tilt: 30,
                                                            mapTypeControl: false,
                                                            clickableIcons: false
                                                        });

                                                        marker = new google.maps.Marker({
                                                            position: latLng,
                                                            title: 'Your dropper is here!'
                                                        });

                                                        marker.setMap(map);
                                                        map.setCenter(latLng);


                                                        setInterval(function(){
                                                            console.log('updating dropper location');

                                                            $.ajax({
                                                                url:"{{url('api/dropper-location')}}/{{$customer->Dropper->drid}}",
                                                                method: "get",
                                                                success: function (success) {

                                                                    var latLng = new google.maps.LatLng( success.lat , success.lng);

                                                                    marker.setPosition(latLng);
                                                                    map.setCenter(latLng);

                                                                },
                                                                error: function (error) {
                                                                    console.log(error);
                                                                }

                                                            });
                                                        },15000);


                                                    }


                                                </script>

                                            </div>

                                            <!-- start tab 5 -->

                                            <div class="tab-pane" id="remittances">
                                                <table class="table table-hover">

                                                    <tr>
                                                        <th>Amount</th>
                                                        <th>Type</th>
                                                        <th>Processed By</th>
                                                        <th>Date</th>
                                                    </tr>

                                                    @foreach($remittances as $item)
                                                        <tr>
                                                            <td>{{$item->amount}}</td>
                                                            <td>{{$item->details}}</td>
                                                            <td>{{$item->User->fname}} {{$item->User->sname}}</td>
                                                            <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at)->toDayDateTimeString()}}</td>
                                                        </tr>
                                                    @endforeach
                                                </table>

                                            </div>

                                            <hr><br>

                                            <div class="row">
                                                <div class="col-md-6"> <!-- assign vehicle -->
                                                    <label> <b>Assign Vehicle</b> </label>

                                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('post-dropper') }}">
                                                        {{ csrf_field() }}

                                                        <div class="form-group">
                                                            <label for="maritalStatus" class="col-md-4 control-label">Vehicle</label>

                                                            <input type="hidden" name="cid" value="{{$customer->cid}}" >
                                                            <div class="col-md-6">
                                                                <select class="form-control" name="vid">
                                                                    @foreach($vehicles as $vehicle)
                                                                        <option value="{{ $vehicle->vid }}">{{$vehicle->brand}} - {{$vehicle->regno}}</option>
                                                                    @endforeach
                                                                    <option value="">None</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-md-10 col-md-offset-4">
                                                                <button type="submit" class="btn btn-primary pull-right">
                                                                    Assign Vehicle
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div> <!-- end assign vehicle -->
                                                <div class="col-md-6">
                                                    <label> <b>Add Remittance</b> </label>

                                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('add-remittance') }}">
                                                        {{ csrf_field() }}

                                                        <div class="form-group">
                                                            <input type="hidden" name="cid" value="{{$customer->cid}}" >

                                                            <label class="col-md-4 control-label">Amount</label>

                                                            <div class="col-md-6">
                                                            <input type="number" name="amount" class="form-control">
                                                            </div> <br><br>

                                                            <label for="type" class="col-md-4 control-label">Type</label>

                                                            <div class="col-md-6">
                                                                <select id="type" class="form-control" name="type">
                                                                    <option value="cash">Cash</option>
                                                                    <option value="pos">POS</option>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-md-10 col-md-offset-4">
                                                                <button type="submit" class="btn btn-primary pull-right">
                                                                    Add
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>



                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
        </div>
    </div>
    </div>


@endsection