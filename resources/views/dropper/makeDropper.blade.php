{{--@extends('layouts.app')--}}
{{--@section('content')--}}
{{--{{$staff->fname}}--}}
{{--<form method="POST" action="{{url('/staff/' .$staff->uid )}}" >--}}
{{--{{csrf_field()}}--}}

{{--<input type="hidden" name='uid' value="{{$staff->uid}}">--}}
{{--<button type="submit" class="btn btn-primary">--}}
{{--Edit User--}}
{{--</button>--}}
{{--</form>--}}
{{--<form method="POST" action="{{url('/staff/delete')}}" >--}}
{{--{{csrf_field()}}--}}
{{--<input type="hidden" name='uid' value="{{$staff->uid}}">--}}
{{--<button type="submit" class="btn btn-primary">--}}
{{--Delete User--}}
{{--</button>--}}
{{--</form>--}}
{{--<a href="{{url('client-details/' . $client->clid)}}" >--}}
{{--@endsection--}}


<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">

                        @include('notification')

                        <!-- .row -->
                            <div class="row bg-title" >
                                <div class="col-lg-12">
                                </div>
                                <div class="col-sm-6 col-md-6 col-xs-12">
                                    <ol class="breadcrumb pull-left">
                                        <li class="active">Profile</li>
                                    </ol>
                                </div>
                                <div class="col-sm-6 col-md-6 col-xs-12">
                                    <form role="search" class="app-search hidden-xs pull-right">
                                        <input type="text" placeholder="Search..." class="form-control">
                                        <a href="javascript:void(0)"><i class="fa fa-search"></i></a>
                                    </form>
                                </div>
                            </div>

                            <!-- .row -->
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="white-box">
                                        <div class="user-bg"> <img width="100%" alt="user" src="{{url('img/landscape17.jpg')}}">
                                            <div class="overlay-box">
                                                <div class="user-content">
                                                    <a href="javascript:void(0)"><img src="img/sunny.jpg" class="thumb-lg img-circle" alt="img"></a>
                                                    <h4 class="text-white">{{$customer->name}}</h4>
                                                    {{--                                                    <h5 class="text-white">Created {{Carbon::createFromFormat("Y-m-d H:i:s",$staff->created_at)->toFormattedDateString()}} </h5>--}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="user-btm-box">
                                            <div class="col-md-4 col-sm-4 text-center">

                                            </div>
                                            <div class="col-md-4 col-sm-4 text-center">

                                            </div>
                                            <div class="col-md-4 col-sm-4 text-center">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-xs-12">
                                    <div class="white-box">
                                        <ul class="nav customtab nav-tabs" role="tablist">

                                            <li role="presentation" class="nav-item"><a href="#details" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Basic</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#addMember" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Details</span></a></li>
                                        </ul>
                                        <div class="tab-content">

                                            <!-- start tab 1 -->
                                            <div class="tab-pane active" id="details">
                                                <div class="row">
                                                    {{--<div class="col-md-3 col-xs-6 b-r"> <strong>Staff Basic Info</strong>--}}
                                                    <br>
                                                    <p class="text-muted">  <strong>Gender: </strong> {{$customer->gender}}</p>
                                                </div>
                                            </div>
                                            <hr>

                                            <table class="table table-bordered">
                                                <tr>
                                                    <th>First Name</th>
                                                    <th>Surname</th>
                                                    <th>Phone</th>
                                                    <th>Email</th>
                                                </tr>


                                                <tr>
                                                    <td>{{$customer->fname}}</td>
                                                    <td>{{$customer->sname}}</td>
                                                    <td>{{$customer->phone}}</td>
                                                    <td>{{$customer->email}}</td>
                                                </tr>

                                            </table>

                                        </div>


                                        <!-- start tab 2  -->
                                        <div class="tab-pane" id="addMember">
                                            {{--<form class="form-horizontal form-material" method="get" action="{{url('add-group-member')}}">--}}
                                            {{csrf_field()}}

                                            {{--<input type="hidden" name="gid" value="{{$group->gid}}">--}}

                                            <div class="form-group">
                                                <label class="col-md-12">Address</label>
                                                <div class="col-md-12">

                                                    {{--<input type="text" placeholder="Type users email here" name="email" class="form-control form-control-line">--}}
                                                    <p>  {{$customer-> address}}</p>




                                                </div>
                                            </div>

                                        </div><br>

                                            <hr><br>


                                            <label> <b>Assign Vehicle</b> </label>

                                            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{ url('post-dropper') }}">
                                                {{ csrf_field() }}



                                                <div class="form-group">
                                                    <label for="maritalStatus" class="col-md-4 control-label">Vehicle</label>

                                                   <input type="hidden" name="cid" value="{{$customer->cid}}" >
                                                    <div class="col-md-6">
                                                        <select class="form-control" name="vid">
                                                            @foreach($vehicles as $vehicle)
                                                            <option value="{{ $vehicle->vid }}">{{$vehicle->brand}} - {{$vehicle->regno}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>



                                                <div class="form-group">
                                                    <div class="col-md-6 col-md-offset-4">
                                                        <button type="submit" class="btn btn-primary">
                                                           Assign Vehicle
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>







                                        </div>
                                        {{--</form>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->

                </div>
            </div>
        </div>
    </div>
    </div>
    </div>


@endsection