@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Registered Users</h3>
                            <table id="demo-foo-addrow" class="table table-bordered table-hover toggle-circle" data-page-size="7">
                                <thead>
                                <tr>
                                    <th data-sort-initial="true" data-toggle="true">First Name</th>
                                    <th>Last Name</th>
                                    <th>Phone</th>
                                    <th data-hide="phone, tablet">Email</th>
                                    <th data-hide="phone, tablet">Balance</th>
                                    <th data-sort-ignore="true" class="min-width"> </th>
                                </tr>
                                </thead>
                                <div class="form-inline padding-bottom-15">
                                    <div class="row">

                                    </div>
                                    <div class="col-sm-6 text-right m-b-20">
                                        <div class="form-group">
                                            <input id="demo-input-search2" type="text" placeholder="Search" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <tbody>

                                @foreach($customers as $item)
                                    <tr>
                                        <td>{{$item->fname}}</td>
                                        <td>{{$item->sname}}</td>
                                        <td>{{$item->phone}}</td>
                                        <td>{{$item->email}}</td>
                                        <td>{{$item->balance}}</td>
                                        <td>



                                            <a href="{{url('makeDropper/'. $item->cid )}}">
                                                <span class="label label-success">Make Dropper</span>
                                            </a>

                                            <a href="{{url('edit-customer/' . $item->cid)}}">
                                                <span class="label label-warning">Edit</span>
                                            </a>

                                            <a href="{{url('suspend-customer/' . $item->cid)}}">
                                                <span class="label label-danger">Suspend</span>
                                            </a>

                                            {{--<button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete"><i class="ti-close" aria-hidden="true"></i></button>--}}
                                        </td>
                                    </tr>

                                @endforeach

                                </tbody>

                                <tfoot>
                                <tr>
                                    <td colspan="6">
                                        <div class="text-right">
                                            <ul class="pagination">
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection