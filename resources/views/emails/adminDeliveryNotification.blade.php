<?php use Carbon\Carbon; ?>
<html>
<head></head>
<body>
<h3>There is a new delivery request</h3> <br>

<p>
    Requested by - {{$delivery->Customer->fname}} {{$delivery->Customer->sname}} <br>
    Requested at - {{Carbon::createFromFormat("Y-m-d H:i:s",$delivery->created_at)->toDayDateTimeString()}} ({{Carbon::createFromFormat("Y-m-d H:i:s",$delivery->created_at)->diffForHumans()}}) <br>
    Pick Up      - {{$delivery->to}} <br>
    Drop Off     - {{$delivery->from}} <br>

    View Details <a target="_blank" href="{{url('view-delivery-details/' . $delivery->did)}}">here</a>
</p>

</body>
</html>
