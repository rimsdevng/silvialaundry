<html>
<body>
<div class="m_2597963863469383166m_-687298137282382961clean-body" style="margin:0;padding:0;background-color:#41046a">
    <table class="m_2597963863469383166m_-687298137282382961nl-container"
           style="border-collapse:collapse;table-layout:fixed;border-spacing:0;vertical-align:top;min-width:320px;Margin:0 auto;background-color:#41046a;width:100%">
        <tbody>
        <tr style="vertical-align:top">
            <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top">


                <div style="background-color:#ffffff">
                    <div style="Margin:0 auto;min-width:320px;max-width:690px;word-wrap:break-word;word-break:break-word;background-color:transparent"
                         class="m_2597963863469383166m_-687298137282382961block-grid m_2597963863469383166m_-687298137282382961two-up">
                        <div style="border-collapse:collapse;display:table;width:100%;background-color:transparent">


                            <div class="m_2597963863469383166m_-687298137282382961col m_2597963863469383166m_-687298137282382961num6"
                                 style="min-width:320px;max-width:345px;display:table-cell;vertical-align:top">
                                <div style="background-color:transparent;width:100%!important">
                                    <div style="border-top:0px solid transparent;border-left:0px solid transparent;border-bottom:0px solid transparent;border-right:0px solid transparent;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px">


                                        <div class="m_2597963863469383166m_-687298137282382961img-container m_2597963863469383166m_-687298137282382961left"
                                             style="padding-right:0px;padding-left:0px" align="left">

                                            <img class="m_2597963863469383166m_-687298137282382961left CToWUd"
                                                 src="https://ci4.googleusercontent.com/proxy/rtcT-l0H_GuVD2BxlukhQ0GiQUVBeBC1ibQRSAUhrQDMOB8FTVKNyly8e8Km0GJopHu6HNfFL4QFmW-pUUoDo0INa9suLpulXoGC5buacLHfbX0yMLOORkK4kRZroDpDJSay3A9z67_mlkGfSpROC1U0dC9IfEa8FMHJrhO3hyUvA1qy=s0-d-e1-ft#https://pro-bee-user-content-eu-west-1.s3.amazonaws.com/public/users/BeeFree/beefree-h5bq9rglfp7/logosmall.png"
                                                 alt="Image" title="Image"
                                                 style="outline:none;text-decoration:none;clear:both;display:block!important;border:0;height:auto;float:none;width:100%;max-width:140px"
                                                 width="140" border="0" align="left"></div>


                                    </div>
                                </div>
                            </div>

                            <div class="m_2597963863469383166m_-687298137282382961col m_2597963863469383166m_-687298137282382961num6"
                                 style="min-width:320px;max-width:345px;display:table-cell;vertical-align:top">
                                <div style="background-color:transparent;width:100%!important">
                                    <div style="border-top:0px solid transparent;border-left:0px solid transparent;border-bottom:0px solid transparent;border-right:0px solid transparent;padding-top:20px;padding-bottom:20px;padding-right:0px;padding-left:0px">


                                        <div style="padding-right:10px;padding-left:10px;padding-bottom:10px"
                                             align="center">
                                            <div style="line-height:10px;font-size:1px">&nbsp;</div>
                                            <div style="display:table;max-width:151px">

                                                <table style="border-collapse:collapse;table-layout:fixed;border-spacing:0;vertical-align:top;Margin-right:5px"
                                                       width="32" height="32" cellspacing="0" cellpadding="0" border="0"
                                                       align="left">
                                                    <tbody>
                                                    <tr style="vertical-align:top">
                                                        <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top"
                                                            valign="middle" align="left">
                                                            <a href="https://www.facebook.com/dropsterng" title="Facebook"
                                                               target="_blank"
                                                            >
                                                                <img src="{{url('images/facebook.png')}}"
                                                                     alt="Facebook" title="Facebook"
                                                                     style="outline:none;text-decoration:none;clear:both;display:block!important;border:none;height:auto;float:none;max-width:32px!important"
                                                                     class="CToWUd" width="32"></a>
                                                            <div style="line-height:5px;font-size:1px">&nbsp;</div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <table style="border-collapse:collapse;table-layout:fixed;border-spacing:0;vertical-align:top;Margin-right:5px"
                                                       width="32" height="32" cellspacing="0" cellpadding="0" border="0"
                                                       align="left">
                                                    <tbody>
                                                    <tr style="vertical-align:top">
                                                        <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top"
                                                            valign="middle" align="left">
                                                            <a href="http://twitter.com/dropsterng" title="Twitter"
                                                               target="_blank">

                                                                <img src="{{url('images/twitter.png')}}"
                                                                     alt="Twitter" title="Twitter"
                                                                     style="outline:none;text-decoration:none;clear:both;display:block!important;border:none;height:auto;float:none;max-width:32px!important"
                                                                     class="CToWUd" width="32"></a>
                                                            <div style="line-height:5px;font-size:1px">&nbsp;</div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <table style="border-collapse:collapse;table-layout:fixed;border-spacing:0;vertical-align:top;Margin-right:0"
                                                       width="32" height="32" cellspacing="0" cellpadding="0" border="0"
                                                       align="left">
                                                    <tbody>
                                                    <tr style="vertical-align:top">
                                                        <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top"
                                                            valign="middle" align="left">
                                                            <a href="https://instagram.com/dropsterng" title="Instagram"
                                                               target="_blank">
                                                                <img src="{{url('images/instagram.png')}}"
                                                                     alt="Instagram" title="Instagram"
                                                                     style="outline:none;text-decoration:none;clear:both;display:block!important;border:none;height:auto;float:none;max-width:32px!important"
                                                                     class="CToWUd" width="32"></a>
                                                            <div style="line-height:5px;font-size:1px">&nbsp;</div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div style="background-image:url('{{url('images/dropster.jpg')}}');background-position:top center;background-repeat:no-repeat;background-color:transparent">
                    <div style="Margin:0 auto;min-width:320px;max-width:690px;word-wrap:break-word;word-break:break-word;background-color:transparent"
                         class="m_2597963863469383166m_-687298137282382961block-grid">
                        <div style="border-collapse:collapse;display:table;width:100%;background-color:transparent">


                            <div class="m_2597963863469383166m_-687298137282382961col m_2597963863469383166m_-687298137282382961num12"
                                 style="min-width:320px;max-width:690px;display:table-cell;vertical-align:top">
                                <div style="background-color:transparent;width:100%!important">
                                    <div style="border-top:0px solid transparent;border-left:0px solid transparent;border-bottom:0px solid transparent;border-right:0px solid transparent;padding-top:30px;padding-bottom:30px;padding-right:25px;padding-left:0px">


                                        <div style="font-family:'Lato',Tahoma,Verdana,Segoe,sans-serif;line-height:120%;color:#333333;padding-right:0px;padding-left:0px;padding-top:30px;padding-bottom:25px">
                                            <div style="font-size:12px;line-height:14px;font-family:'Lato',Tahoma,Verdana,Segoe,sans-serif;color:#333333;text-align:left">
                                                <p style="margin:0;font-size:14px;line-height:17px;text-align:right">
                                                    &nbsp;<br></p>
                                                <p style="margin:0;font-size:14px;line-height:17px;text-align:right">
                                                    <span style="font-size:28px;line-height:33px">Hi {{$data->customer->first_name}}
                                                        ,<br>Hope you're good?</span></p></div>
                                        </div>


                                        <div style="padding-right:15px;padding-left:15px;padding-top:15px;padding-bottom:15px">

                                            <div align="center">
                                                <div style="border-top:0px solid #bbbbbb;width:90%;line-height:0px;height:0px;font-size:0px">
                                                    &nbsp;
                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div style="background-color:#ffffff">
                    <div style="Margin:0 auto;min-width:320px;max-width:690px;word-wrap:break-word;word-break:break-word;background-color:transparent"
                         class="m_2597963863469383166m_-687298137282382961block-grid">
                        <div style="border-collapse:collapse;display:table;width:100%;background-color:transparent">


                            <div class="m_2597963863469383166m_-687298137282382961col m_2597963863469383166m_-687298137282382961num12"
                                 style="min-width:320px;max-width:690px;display:table-cell;vertical-align:top">
                                <div style="background-color:transparent;width:100%!important">
                                    <div style="border-top:0px solid transparent;border-left:0px solid transparent;border-bottom:0px solid transparent;border-right:0px solid transparent;padding-top:30px;padding-bottom:30px;padding-right:0px;padding-left:0px">


                                        <div style="font-family:'Lato',Tahoma,Verdana,Segoe,sans-serif;line-height:120%;color:#41046a;padding-right:10px;padding-left:10px;padding-top:25px;padding-bottom:10px">
                                            <div style="font-size:12px;line-height:14px;color:#41046a;font-family:'Lato',Tahoma,Verdana,Segoe,sans-serif;text-align:left">
                                                <p style="margin:0;font-size:18px;line-height:22px;text-align:center">
                                                    <span style="font-size:24px;line-height:28px"><strong>Card Payment Successful<br></strong></span>
                                                </p></div>
                                        </div>


                                        <div style="color:#39b339;line-height:120%;font-family:'Lato',Tahoma,Verdana,Segoe,sans-serif;padding-right:10px;padding-left:10px;padding-top:10px;padding-bottom:10px">
                                            <div style="font-size:12px;line-height:14px;color:#39b339;font-family:'Lato',Tahoma,Verdana,Segoe,sans-serif;text-align:left">
                                                <p style="margin:0;font-size:12px;line-height:14px;text-align:center">
                                                    <span style="font-size:36px;line-height:43px"><strong> 	&#x20A6; {{$data->amount / 100}}</strong></span>
                                                </p></div>
                                        </div>


                                        <div style="font-family:'Lato',Tahoma,Verdana,Segoe,sans-serif;line-height:150%;color:#030306;padding-right:10px;padding-left:10px;padding-top:0px;padding-bottom:10px">
                                            <div style="font-size:12px;line-height:18px;color:#030306;font-family:'Lato',Tahoma,Verdana,Segoe,sans-serif;text-align:left">
                                                <p style="margin:0;font-size:12px;line-height:18px;text-align:center">
                                                    <span style="font-size:16px;line-height:24px">&nbsp;&nbsp;</span>
                                                </p>
                                                <p style="margin:0;font-size:12px;line-height:18px;text-align:center">
                                                    <span style="font-size:16px;line-height:24px">Your card payment&nbsp;was successful!<span
                                                                style="line-height:24px;font-size:16px">﻿</span></span>
                                                </p>
                                                <p style="margin:0;font-size:12px;line-height:18px;text-align:center">
                                                    &nbsp;<br></p>
                                                <p style="margin:0;font-size:12px;line-height:18px;text-align:center">
                                                    <span style="font-size:12px;line-height:18px"><span
                                                                style="line-height:18px;font-size:12px">{{\Carbon\Carbon::now()->toDayDateTimeString()}} </span></span>
                                                </p>

                                                <p style="margin:0;font-size:12px;line-height:18px;text-align:center">
                                                    <span style="font-size:16px;line-height:24px">&nbsp;</span><br></p>
                                                <p style="margin:0;font-size:12px;line-height:18px;text-align:center">
                                                    <span style="font-size:16px;line-height:24px">Thanks for trusting Dropster,&nbsp;{{$data->customer->first_name}}</span>
                                                </p>
                                                <p style="margin:0;font-size:12px;line-height:18px;text-align:center">
                                                    &nbsp;<br></p>
                                                <p style="margin:0;font-size:12px;line-height:18px">&nbsp;<br></p></div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div style="background-color:#41046a">
                    <div style="Margin:0 auto;min-width:320px;max-width:690px;word-wrap:break-word;word-break:break-word;background-color:transparent"
                         class="m_2597963863469383166m_-687298137282382961block-grid">
                        <div style="border-collapse:collapse;display:table;width:100%;background-color:transparent">


                            <div class="m_2597963863469383166m_-687298137282382961col m_2597963863469383166m_-687298137282382961num12"
                                 style="min-width:320px;max-width:690px;display:table-cell;vertical-align:top">
                                <div style="background-color:transparent;width:100%!important">
                                    <div style="border-top:0px solid transparent;border-left:0px solid transparent;border-bottom:0px solid transparent;border-right:0px solid transparent;padding-top:30px;padding-bottom:30px;padding-right:0px;padding-left:0px">


                                        <div style="font-family:'Lato',Tahoma,Verdana,Segoe,sans-serif;line-height:150%;color:#f2edf6;padding-right:10px;padding-left:10px;padding-top:15px;padding-bottom:10px">
                                            <div style="font-size:12px;line-height:18px;color:#f2edf6;font-family:'Lato',Tahoma,Verdana,Segoe,sans-serif;text-align:left">
                                                <p style="margin:0;font-size:12px;line-height:18px;text-align:center">
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Want a free dropster delivery?
                                                    &nbsp; &nbsp; &nbsp; &nbsp;</p>
                                                <p style="margin:0;font-size:12px;line-height:18px;text-align:center">
                                                    Share your&nbsp;code :&nbsp;<span
                                                            style="font-size:18px;line-height:27px"><strong><span
                                                                    style="line-height:27px;font-size:18px">{{$customer->referralCode}}</span></strong></span><br>
                                                </p></div>
                                        </div>


                                        <div style="padding-right:10px;padding-left:10px;padding-bottom:10px"
                                             align="center">
                                            <div style="line-height:10px;font-size:1px">&nbsp;</div>
                                            <div style="display:table;max-width:151px">

                                                <table style="border-collapse:collapse;table-layout:fixed;border-spacing:0;vertical-align:top;Margin-right:5px"
                                                       width="32" height="32" cellspacing="0" cellpadding="0" border="0"
                                                       align="left">
                                                    <tbody>
                                                    <tr style="vertical-align:top">
                                                        <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top"
                                                            valign="middle" align="left">
                                                            <a href="https://www.facebook.com/" title="Facebook"
                                                               target="_blank"
                                                               data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://www.facebook.com/&amp;source=gmail&amp;ust=1505565659709000&amp;usg=AFQjCNENcZnCOHq_Tt-KnpjEXGpyU_AwYQ">
                                                                <img src="https://ci5.googleusercontent.com/proxy/ueHVKwun5CYHY9jmIwRlQDw8IyxVIAtwpY_7aywnaefXpv7iYBPDwiqbBMxE8ECggrz3br_TFjzwM72hx4qYgEpIxOc8Qkphj4zVJwqxvIJHFIDHT7y8FhSKfauPMn1w3HkpUs-SUVEq6-l40rP0cdZE-kqjB9hCh-FZpj5HjASkjAs=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-color/facebook@2x.png"
                                                                     alt="Facebook" title="Facebook"
                                                                     style="outline:none;text-decoration:none;clear:both;display:block!important;border:none;height:auto;float:none;max-width:32px!important"
                                                                     class="CToWUd" width="32"></a>
                                                            <div style="line-height:5px;font-size:1px">&nbsp;</div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <table style="border-collapse:collapse;table-layout:fixed;border-spacing:0;vertical-align:top;Margin-right:5px"
                                                       width="32" height="32" cellspacing="0" cellpadding="0" border="0"
                                                       align="left">
                                                    <tbody>
                                                    <tr style="vertical-align:top">
                                                        <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top"
                                                            valign="middle" align="left">
                                                            <a href="https://twitter.com/" title="Twitter"
                                                               target="_blank"
                                                               data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://twitter.com/&amp;source=gmail&amp;ust=1505565659709000&amp;usg=AFQjCNG30RetBCCAiJ1QANQkHrQvqF97Gw">
                                                                <img src="https://ci6.googleusercontent.com/proxy/inlRvuejLBdUuGo4Obt16vKeC7WBi-3G8FPNEuHKLKeXbisg3R1IkF1vZwhwJqmSlTQx07x03g8TsU0W4bDEGp5zcZf4I6kt3W8NTUE6HHPDtFFCHkB67q1pRa4oUmiMaavKtNljOll4fT3ALIhcX9e8oEiVaFmIlgdy3hqo0KO1bA=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-color/twitter@2x.png"
                                                                     alt="Twitter" title="Twitter"
                                                                     style="outline:none;text-decoration:none;clear:both;display:block!important;border:none;height:auto;float:none;max-width:32px!important"
                                                                     class="CToWUd" width="32"></a>
                                                            <div style="line-height:5px;font-size:1px">&nbsp;</div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <table style="border-collapse:collapse;table-layout:fixed;border-spacing:0;vertical-align:top;Margin-right:0"
                                                       width="32" height="32" cellspacing="0" cellpadding="0" border="0"
                                                       align="left">
                                                    <tbody>
                                                    <tr style="vertical-align:top">
                                                        <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top"
                                                            valign="middle" align="left">
                                                            <a href="https://plus.google.com/" title="Google+"
                                                               target="_blank"
                                                               data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://plus.google.com/&amp;source=gmail&amp;ust=1505565659709000&amp;usg=AFQjCNGeUxUJ5aWYqa3kJcLNk5KJ5mrtPw">
                                                                <img src="https://ci3.googleusercontent.com/proxy/_fZ9Zt6XuobHG2nudm4ku4WIjlxRZotwHLXSxKGhqbedKsF-H_6viKAvhSniZcTcgZa7AuVE-5H3GWR-hUNEWdfLm-R5m_cW3Un96dZ-3IGQV1Q6mQSiCUkyNrsHBbMrBq9XE05dSSZT5PuPWcqaGqce1xjxkwAz4NKh6UxQrORxvAokoQ=s0-d-e1-ft#https://d2fi4ri5dhpqd1.cloudfront.net/public/resources/social-networks-icon-sets/circle-color/googleplus@2x.png"
                                                                     alt="Google+" title="Google+"
                                                                     style="outline:none;text-decoration:none;clear:both;display:block!important;border:none;height:auto;float:none;max-width:32px!important"
                                                                     class="CToWUd" width="32"></a>
                                                            <div style="line-height:5px;font-size:1px">&nbsp;</div>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>


                                        <div style="color:#ffffff;line-height:120%;font-family:'Lato',Tahoma,Verdana,Segoe,sans-serif;padding-right:10px;padding-left:10px;padding-top:10px;padding-bottom:10px">
                                            <div style="font-size:12px;line-height:14px;color:#ffffff;font-family:'Lato',Tahoma,Verdana,Segoe,sans-serif;text-align:left">
                                                <p style="margin:0;font-size:12px;line-height:14px;text-align:center">
                                                    Need &nbsp;Help?</p></div>
                                        </div>


                                        <div style="color:#efebf2;line-height:120%;font-family:'Lato',Tahoma,Verdana,Segoe,sans-serif;padding-right:10px;padding-left:10px;padding-top:10px;padding-bottom:10px">
                                            <div style="font-size:12px;line-height:14px;color:#efebf2;font-family:'Lato',Tahoma,Verdana,Segoe,sans-serif;text-align:left">
                                                <p style="margin:0;font-size:14px;line-height:17px;text-align:center">
                                                    <span style="font-size:16px;line-height:19px">Tap the support button in your app to contact us about your delivery.</span>
                                                </p></div>
                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <table style="border-spacing:0;border-collapse:collapse;vertical-align:top" width="100%" cellspacing="0"
           cellpadding="0" border="0" align="center">
        <tbody>
        <tr style="vertical-align:top">
            <td style="word-break:break-word;vertical-align:top;background-color:rgb(147,211,237);border-collapse:collapse!important;width:100%">


                <table class="m_2597963863469383166m_-687298137282382961container"
                       style="border-spacing:0;border-collapse:collapse;vertical-align:top;max-width:700px;margin:0 auto;text-align:inherit"
                       width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                    <tr style="vertical-align:top">
                        <td style="word-break:break-word;vertical-align:top;border-collapse:collapse!important;width:100%">
                            <table class="m_2597963863469383166m_-687298137282382961block-grid"
                                   style="border-spacing:0;border-collapse:collapse;vertical-align:top;width:100%;max-width:700px;color:#000000;background-color:transparent"
                                   width="100%" cellspacing="0" cellpadding="0">
                                <tbody>
                                <tr style="vertical-align:top">
                                    <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;background-color:transparent;text-align:center;font-size:0">
                                        <div class="m_2597963863469383166m_-687298137282382961col m_2597963863469383166m_-687298137282382961num12"
                                             style="display:inline-block;vertical-align:top;width:100%">
                                            <table style="border-spacing:0;border-collapse:collapse;vertical-align:top"
                                                   width="100%" cellspacing="0" cellpadding="0" border="0"
                                                   align="center">
                                                <tbody>
                                                <tr style="vertical-align:top">
                                                    <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;background-color:transparent;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;border-top:0px solid transparent;border-right:0px solid transparent;border-bottom:4px solid #93d3ed;border-left:0px solid transparent">
                                                        <table style="border-spacing:0;border-collapse:collapse;vertical-align:top"
                                                               width="100%" cellspacing="0" cellpadding="0" border="0"
                                                               align="center">
                                                            <tbody>
                                                            <tr style="vertical-align:top">
                                                                <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px"
                                                                    align="center">
                                                                    <div style="height:0px">
                                                                        <table style="border-spacing:0;border-collapse:collapse;vertical-align:top;border-top:0px solid transparent;width:100%"
                                                                               cellspacing="0" border="0"
                                                                               align="center">
                                                                            <tbody>
                                                                            <tr style="vertical-align:top">
                                                                                <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top"
                                                                                    align="center"></td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>


            </td>
        </tr>
        </tbody>
    </table>

    <table class="m_2597963863469383166m_-687298137282382961block-grid"
           style="border-spacing:0;border-collapse:collapse;vertical-align:top;width:100%;max-width:700px;color:#000000;background-color:transparent"
           width="100%" cellspacing="0" cellpadding="0">
        <tbody>
        <tr style="vertical-align:top">
            <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;background-color:transparent;text-align:center;font-size:0">
                <div class="m_2597963863469383166m_-687298137282382961col m_2597963863469383166m_-687298137282382961num12"
                     style="display:inline-block;vertical-align:top;width:100%">
                    <table style="border-spacing:0;border-collapse:collapse;vertical-align:top" width="100%"
                           cellspacing="0" cellpadding="0" border="0" align="center">
                        <tbody>
                        <tr style="vertical-align:top">
                            <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;background-color:transparent;padding-top:5px;padding-right:0px;padding-bottom:5px;padding-left:0px;border-top:0px solid transparent;border-right:0px solid transparent;border-bottom:0px solid transparent;border-left:0px solid transparent">
                                <table style="border-spacing:0;border-collapse:collapse;vertical-align:top" width="100%"
                                       cellspacing="0" cellpadding="0">
                                    <tbody>
                                    <tr style="vertical-align:top">
                                        <td style="word-break:break-word;border-collapse:collapse!important;vertical-align:top;padding-top:20px;padding-right:15px;padding-bottom:20px;padding-left:15px">
                                            <div style="color:#93d3ed;line-height:180%;font-family:'Lucida Sans Unicode','Lucida Grande','Lucida Sans',Geneva,Verdana,sans-serif">
                                                <div style="font-size:12px;line-height:22px;color:#93d3ed;font-family:'Lucida Sans Unicode','Lucida Grande','Lucida Sans',Geneva,Verdana,sans-serif;text-align:left">
                                                    <p style="margin:0;font-size:12px;line-height:22px;text-align:center">

                                                        Copyright &copy; <?php echo date('Y'); ?>. Developed by

                                                        <a style="color:#93d3ed;text-decoration:underline"
                                                                title="Gurudeveloeper Inc." href="https://gurudeveloperinc.com"
                                                                target="_blank">Gurudeveloper Inc.
                                                        </a>
                                                    </p>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>

