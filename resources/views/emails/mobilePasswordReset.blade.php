<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml><![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
    <title>Dropster</title>
    <!--[if !mso]><!-- -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
    <!--<![endif]-->

    <style type="text/css" id="media-query">
        body {
            margin: 0;
            padding: 0;
        }

        table, tr, td {
            vertical-align: top;
            border-collapse: collapse;
        }

        .ie-browser table, .mso-container table {
            table-layout: fixed;
        }

        * {
            line-height: inherit;
        }

        a[x-apple-data-detectors=true] {
            color: inherit !important;
            text-decoration: none !important;
        }

        [owa] .img-container div, [owa] .img-container button {
            display: block !important;
        }

        [owa] .fullwidth button {
            width: 100% !important;
        }

        [owa] .block-grid .col {
            display: table-cell;
            float: none !important;
            vertical-align: top;
        }

        .ie-browser .num12, .ie-browser .block-grid, [owa] .num12, [owa] .block-grid {
            width: 600px !important;
        }

        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
            line-height: 100%;
        }

        .ie-browser .mixed-two-up .num4, [owa] .mixed-two-up .num4 {
            width: 200px !important;
        }

        .ie-browser .mixed-two-up .num8, [owa] .mixed-two-up .num8 {
            width: 400px !important;
        }

        .ie-browser .block-grid.two-up .col, [owa] .block-grid.two-up .col {
            width: 300px !important;
        }

        .ie-browser .block-grid.three-up .col, [owa] .block-grid.three-up .col {
            width: 200px !important;
        }

        .ie-browser .block-grid.four-up .col, [owa] .block-grid.four-up .col {
            width: 150px !important;
        }

        .ie-browser .block-grid.five-up .col, [owa] .block-grid.five-up .col {
            width: 120px !important;
        }

        .ie-browser .block-grid.six-up .col, [owa] .block-grid.six-up .col {
            width: 100px !important;
        }

        .ie-browser .block-grid.seven-up .col, [owa] .block-grid.seven-up .col {
            width: 85px !important;
        }

        .ie-browser .block-grid.eight-up .col, [owa] .block-grid.eight-up .col {
            width: 75px !important;
        }

        .ie-browser .block-grid.nine-up .col, [owa] .block-grid.nine-up .col {
            width: 66px !important;
        }

        .ie-browser .block-grid.ten-up .col, [owa] .block-grid.ten-up .col {
            width: 60px !important;
        }

        .ie-browser .block-grid.eleven-up .col, [owa] .block-grid.eleven-up .col {
            width: 54px !important;
        }

        .ie-browser .block-grid.twelve-up .col, [owa] .block-grid.twelve-up .col {
            width: 50px !important;
        }

        @media only screen and (min-width: 620px) {
            .block-grid {
                width: 600px !important;
            }

            .block-grid .col {
                vertical-align: top;
            }

            .block-grid .col.num12 {
                width: 600px !important;
            }

            .block-grid.mixed-two-up .col.num4 {
                width: 200px !important;
            }

            .block-grid.mixed-two-up .col.num8 {
                width: 400px !important;
            }

            .block-grid.two-up .col {
                width: 300px !important;
            }

            .block-grid.three-up .col {
                width: 200px !important;
            }

            .block-grid.four-up .col {
                width: 150px !important;
            }

            .block-grid.five-up .col {
                width: 120px !important;
            }

            .block-grid.six-up .col {
                width: 100px !important;
            }

            .block-grid.seven-up .col {
                width: 85px !important;
            }

            .block-grid.eight-up .col {
                width: 75px !important;
            }

            .block-grid.nine-up .col {
                width: 66px !important;
            }

            .block-grid.ten-up .col {
                width: 60px !important;
            }

            .block-grid.eleven-up .col {
                width: 54px !important;
            }

            .block-grid.twelve-up .col {
                width: 50px !important;
            }
        }

        @media (max-width: 620px) {
            .block-grid, .col {
                min-width: 320px !important;
                max-width: 100% !important;
                display: block !important;
            }

            .block-grid {
                width: calc(100% - 40px) !important;
            }

            .col {
                width: 100% !important;
            }

            .col > div {
                margin: 0 auto;
            }

            img.fullwidth {
                max-width: 100% !important;
            }
        }

    </style>
</head>
<body class="clean-body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #FFFFFF">
<style type="text/css" id="media-query-bodytag">
    @media (max-width: 520px) {
        .block-grid {
            min-width: 320px !important;
            max-width: 100% !important;
            width: 100% !important;
            display: block !important;
        }

        .col {
            min-width: 320px !important;
            max-width: 100% !important;
            width: 100% !important;
            display: block !important;
        }

        .col > div {
            margin: 0 auto;
        }

        img.fullwidth {
            max-width: 100% !important;
        }
    }
</style>
<!--[if IE]>
<div class="ie-browser"><![endif]-->
<!--[if mso]>
<div class="mso-container"><![endif]-->
<table class="nl-container"
       style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #FFFFFF;width: 100%">
    <tbody>
    <tr style="vertical-align: top">
        <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
            <!--[if (mso)|(IE)]>
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td align="center" style="background-color: #FFFFFF;"><![endif]-->

            <div style="background-color:transparent;">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;"
                     class="block-grid ">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                        <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="background-color:transparent;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width: 600px;">
                                        <tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

                        <!--[if (mso)|(IE)]>
                        <td align="center" width="600"
                            style=" width:600px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top"><![endif]-->
                        <div class="col num12"
                             style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <!--[if (!mso)&(!IE)]><!-->
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                    <!--<![endif]-->


                                    <div style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                        <!--[if (mso)]>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="padding-right: 10px;padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                                    <table width="100%" align="center" cellpadding="0" cellspacing="0"
                                                           border="0">
                                                        <tr>
                                                            <td><![endif]-->
                                        <div align="center">
                                            <div style="border-top: 0px solid transparent; width:100%; line-height:0px; height:0px; font-size:0px;">
                                                &#160;
                                            </div>
                                        </div>
                                        <!--[if (mso)]></td></tr></table></td></tr></table><![endif]-->
                                    </div>


                                    <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                            </div>
                        </div>
                        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                    </div>
                </div>
            </div>
            <div style="background-color:#FFFFFF;">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;"
                     class="block-grid two-up">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                        <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="background-color:#FFFFFF;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width: 600px;">
                                        <tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

                        <!--[if (mso)|(IE)]>
                        <td align="center" width="300"
                            style=" width:300px; padding-right: 0px; padding-left: 0px; padding-top:10px; padding-bottom:15px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top"><![endif]-->
                        <div class="col num6"
                             style="max-width: 320px;min-width: 300px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <!--[if (!mso)&(!IE)]><!-->
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:10px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;">
                                    <!--<![endif]-->


                                    <div align="center" class="img-container center"
                                         style="padding-right: 0px;  padding-left: 0px;">
                                        <!--[if mso]>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="padding-right: 0px; padding-left: 0px;" align="center">
                                        <![endif]-->
                                        <img class="center" align="center" border="0" src="images/logosmall.png"
                                             alt="Image" title="Image"
                                             style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: 0;height: auto;float: none;width: 100%;max-width: 140px"
                                             width="140">
                                        <div style="line-height:5px;font-size:1px">&#160;</div>
                                        <!--[if mso]></td></tr></table><![endif]-->
                                    </div>


                                    <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                            </div>
                        </div>
                        <!--[if (mso)|(IE)]></td>
                        <td align="center" width="300"
                            style=" width:300px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:15px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top"><![endif]-->
                        <div class="col num6"
                             style="max-width: 320px;min-width: 300px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <!--[if (!mso)&(!IE)]><!-->
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;">
                                    <!--<![endif]-->


                                    <div align="center"
                                         style="padding-right: 10px; padding-left: 10px; padding-bottom: 10px;">
                                        <div style="line-height:20px;font-size:1px">&#160;</div>
                                        <div style="display: table; max-width:188px;">
                                            <!--[if (mso)|(IE)]>
                                            <table width="168" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td style="border-collapse:collapse; padding-right: 10px; padding-left: 10px; padding-bottom: 10px;"
                                                        align="center">
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                               style="border-collapse:collapse; mso-table-lspace: 0pt;mso-table-rspace: 0pt; width:168px;">
                                                            <tr>
                                                                <td width="32" style="width:32px; padding-right: 5px;"
                                                                    valign="top"><![endif]-->
                                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="32"
                                                   height="32"
                                                   style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px">
                                                <tbody>
                                                <tr style="vertical-align: top">
                                                    <td align="left" valign="middle"
                                                        style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                        <a href="https://www.facebook.com/dropsterng" title="Facebook"
                                                           target="_blank">
                                                            <img src="{{url('images/facebook.png')}}" alt="Facebook"
                                                                 title="Facebook" width="32"
                                                                 style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                        </a>
                                                        <div style="line-height:5px;font-size:1px">&#160;</div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!--[if (mso)|(IE)]></td>
                                            <td width="32" style="width:32px; padding-right: 5px;" valign="top">
                                            <![endif]-->
                                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="32"
                                                   height="32"
                                                   style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px">
                                                <tbody>
                                                <tr style="vertical-align: top">
                                                    <td align="left" valign="middle"
                                                        style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                        <a href="https://twitter.com/dropsterng" title="Twitter"
                                                           target="_blank">
                                                            <img src="{{url('images/twitter.png')}}" alt="Twitter"
                                                                 title="Twitter" width="32"
                                                                 style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                        </a>
                                                        <div style="line-height:5px;font-size:1px">&#160;</div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!--[if (mso)|(IE)]></td>
                                            <td width="32" style="width:32px; padding-right: 5px;" valign="top">
                                            <![endif]-->
                                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="32"
                                                   height="32"
                                                   style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px">
                                                <tbody>
                                                <tr style="vertical-align: top">
                                                    <td align="left" valign="middle"
                                                        style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                        <a href="https://www.youtube.com/dropsterng" title="YouTube"
                                                           target="_blank">
                                                            <img src="{{url('images/youtube@2x.png')}}" alt="YouTube"
                                                                 title="YouTube" width="32"
                                                                 style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                        </a>
                                                        <div style="line-height:5px;font-size:1px">&#160;</div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!--[if (mso)|(IE)]></td>
                                            <td width="32" style="width:32px; padding-right: 0;" valign="top">
                                            <![endif]-->
                                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="32"
                                                   height="32"
                                                   style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 0">
                                                <tbody>
                                                <tr style="vertical-align: top">
                                                    <td align="left" valign="middle"
                                                        style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                        <a href="https://instagram.com/dropsterng" title="Instagram"
                                                           target="_blank">
                                                            <img src="{{url('images/instagram.png')}}" alt="Instagram"
                                                                 title="Instagram" width="32"
                                                                 style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                        </a>
                                                        <div style="line-height:5px;font-size:1px">&#160;</div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                        </div>
                                    </div>


                                    <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                            </div>
                        </div>
                        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                    </div>
                </div>
            </div>
            <div style="background-image:url('https://pro-bee-user-content-eu-west-1.s3.amazonaws.com/public/users/Integrators/BeeProAgency/82061_57356/Dropster%20%281%29.png');background-position:top left;background-repeat:no-repeat;;background-color:transparent">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;"
                     class="block-grid ">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                        <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="background-image:url('https://pro-bee-user-content-eu-west-1.s3.amazonaws.com/public/users/Integrators/BeeProAgency/82061_57356/Dropster%20%281%29.png');background-position:top left;background-repeat:no-repeat;;background-color:transparent"
                                    align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width: 600px;">
                                        <tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

                        <!--[if (mso)|(IE)]>
                        <td align="center" width="600"
                            style=" width:600px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top"><![endif]-->
                        <div class="col num12"
                             style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <!--[if (!mso)&(!IE)]><!-->
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                    <!--<![endif]-->


                                    <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 10px; padding-left: 10px; padding-top: 30px; padding-bottom: 5px;">
                                    <![endif]-->
                                    <div style="line-height:120%;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;color:#FFFFFF; padding-right: 10px; padding-left: 10px; padding-top: 30px; padding-bottom: 5px;">
                                        <div style="font-size:12px;line-height:14px;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;color:#FFFFFF;text-align:left;">
                                            <p style="margin: 0;font-size: 12px;line-height: 14px;text-align: center">
                                                <span style="font-size: 48px; line-height: 57px;"><strong><span
                                                                style="line-height: 57px; font-size: 48px;">{{$code}}</span></strong></span>
                                            </p></div>
                                    </div>
                                    <!--[if mso]></td></tr></table><![endif]-->


                                    <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 10px; padding-left: 10px; padding-top: 5px; padding-bottom: 10px;">
                                    <![endif]-->
                                    <div style="line-height:120%;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;color:#F9F9F9; padding-right: 10px; padding-left: 10px; padding-top: 5px; padding-bottom: 10px;">
                                        <div style="font-size:12px;line-height:14px;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;color:#F9F9F9;text-align:left;">
                                            <p style="margin: 0;font-size: 14px;line-height: 17px;text-align: center">
                                                <span style="font-size: 20px; line-height: 24px;"><strong>Please reset your Dropster password!</strong> </span><br>
                                            </p></div>
                                    </div>
                                    <!--[if mso]></td></tr></table><![endif]-->


                                    <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                            </div>
                        </div>
                        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                    </div>
                </div>
            </div>
            <div style="background-color:transparent;">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;"
                     class="block-grid ">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                        <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="background-color:transparent;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width: 600px;">
                                        <tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

                        <!--[if (mso)|(IE)]>
                        <td align="center" width="600"
                            style=" width:600px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:15px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top"><![endif]-->
                        <div class="col num12"
                             style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <!--[if (!mso)&(!IE)]><!-->
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:15px; padding-right: 0px; padding-left: 0px;">
                                    <!--<![endif]-->


                                    <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 10px; padding-left: 10px; padding-top: 5px; padding-bottom: 10px;">
                                    <![endif]-->
                                    <div style="line-height:120%;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;color:#555555; padding-right: 10px; padding-left: 10px; padding-top: 5px; padding-bottom: 10px;">
                                        <div style="font-size:12px;line-height:14px;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;color:#555555;text-align:left;">
                                            <p style="margin: 0;font-size: 14px;line-height: 17px;text-align: center">
                                                <span style="font-size: 20px; line-height: 24px;"><strong>Hello {{$customer->fname}}</strong></span><br>
                                            </p></div>
                                    </div>
                                    <!--[if mso]></td></tr></table><![endif]-->


                                    <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 25px; padding-left: 25px; padding-top: 5px; padding-bottom: 20px;">
                                    <![endif]-->
                                    <div style="line-height:180%;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;color:#555555; padding-right: 25px; padding-left: 25px; padding-top: 5px; padding-bottom: 20px;">
                                        <div style="font-size:12px;line-height:22px;color:#555555;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;text-align:left;">
                                            <p style="margin: 0;font-size: 14px;line-height: 25px;text-align: center">
                                                <span style="font-size: 16px; line-height: 28px;"><span
                                                            style="line-height: 28px; font-size: 16px;">We’ve sent this message because you requested your Dropster password to be reset. To get back into your Dropster account you will need to create a new password</span><span
                                                            style="line-height: 28px; font-size: 16px;">.</span></span>
                                            </p></div>
                                    </div>
                                    <!--[if mso]></td></tr></table><![endif]-->


                                    <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 25px; padding-left: 25px; padding-top: 5px; padding-bottom: 20px;">
                                    <![endif]-->
                                    <div style="line-height:180%;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;color:#555555; padding-right: 25px; padding-left: 25px; padding-top: 5px; padding-bottom: 20px;">
                                        <div style="font-size:12px;line-height:22px;color:#555555;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;text-align:left;">
                                            <p style="margin: 0;font-size: 14px;line-height: 25px;text-align: left">
                                                <span style="font-size: 16px; line-height: 28px;"><span
                                                            style="line-height: 28px; font-size: 16px;">Here's how you do that</span><span
                                                            style="line-height: 28px; font-size: 16px;">.</span></span>
                                            </p>
                                            <p style="margin: 0;font-size: 14px;line-height: 25px;text-align: left">
                                                <span style="font-size: 16px; line-height: 28px;"><span
                                                            style="line-height: 28px; font-size: 16px;">1. Click the button&#160;below to open a new secure browser window.</span></span>
                                            </p>
                                            <p style="margin: 0;font-size: 14px;line-height: 25px;text-align: left">
                                                <span style="font-size: 16px; line-height: 28px;"><span
                                                            style="line-height: 28px; font-size: 16px;">2. Follow the instructions to successfully reset your password.</span></span>
                                            </p></div>
                                    </div>
                                    <!--[if mso]></td></tr></table><![endif]-->


                                    <div align="center" class="button-container center"
                                         style="padding-right: 10px; padding-left: 10px; padding-top:15px; padding-bottom:20px;">
                                        <!--[if mso]>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                               style="border-spacing: 0; border-collapse: collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                            <tr>
                                                <td style="padding-right: 10px; padding-left: 10px; padding-top:15px; padding-bottom:20px;"
                                                    align="center">
                                                    <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml"
                                                                 xmlns:w="urn:schemas-microsoft-com:office:word" href=""
                                                                 style="height:42px; v-text-anchor:middle; width:170px;"
                                                                 arcsize="12%" strokecolor="#41046A"
                                                                 fillcolor="transparent">
                                                        <w:anchorlock/>
                                                        <center style="color:#41046A; font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif; font-size:14px;">
                                        <![endif]-->
                                        <div style="color: #41046A; background-color: transparent; border-radius: 5px; -webkit-border-radius: 5px; -moz-border-radius: 5px; max-width: 170px; width: 130px;width: auto; border-top: 2px solid #41046A; border-right: 2px solid #41046A; border-bottom: 2px solid #41046A; border-left: 2px solid #41046A; padding-top: 5px; padding-right: 20px; padding-bottom: 5px; padding-left: 20px; font-family: 'Lato', Tahoma, Verdana, Segoe, sans-serif; text-align: center; mso-border-alt: none;">
                                            <span style="font-size:12px;line-height:24px;"><span
                                                        style="font-size: 14px; line-height: 28px;"
                                                        data-mce-style="font-size: 14px;"><strong>RESET PASSWORD</strong></span></span>
                                        </div>
                                        <!--[if mso]></center></v:roundrect></td></tr></table><![endif]-->
                                    </div>


                                    <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                            </div>
                        </div>
                        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                    </div>
                </div>
            </div>
            <div style="background-color:transparent;">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;"
                     class="block-grid ">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                        <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="background-color:transparent;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width: 600px;">
                                        <tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

                        <!--[if (mso)|(IE)]>
                        <td align="center" width="600"
                            style=" width:600px; padding-right: 0px; padding-left: 0px; padding-top:0px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top"><![endif]-->
                        <div class="col num12"
                             style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <!--[if (!mso)&(!IE)]><!-->
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:0px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                    <!--<![endif]-->


                                    <div style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                        <!--[if (mso)]>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="padding-right: 10px;padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                                    <table width="100%" align="center" cellpadding="0" cellspacing="0"
                                                           border="0">
                                                        <tr>
                                                            <td><![endif]-->
                                        <div align="center">
                                            <div style="border-top: 0px solid transparent; width:100%; line-height:0px; height:0px; font-size:0px;">
                                                &#160;
                                            </div>
                                        </div>
                                        <!--[if (mso)]></td></tr></table></td></tr></table><![endif]-->
                                    </div>


                                    <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                            </div>
                        </div>
                        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                    </div>
                </div>
            </div>
            <div style="background-color:transparent;">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;"
                     class="block-grid ">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                        <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="background-color:transparent;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width: 600px;">
                                        <tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

                        <!--[if (mso)|(IE)]>
                        <td align="center" width="600"
                            style=" width:600px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top"><![endif]-->
                        <div class="col num12"
                             style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <!--[if (!mso)&(!IE)]><!-->
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                    <!--<![endif]-->


                                    <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 5px; padding-left: 5px; padding-top: 5px; padding-bottom: 5px;">
                                    <![endif]-->
                                    <div style="line-height:120%;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;color:#41046a; padding-right: 5px; padding-left: 5px; padding-top: 5px; padding-bottom: 5px;">
                                        <div style="font-size:12px;line-height:14px;font-family:'Helvetica Neue', Helvetica, Arial, sans-serif;color:#41046a;text-align:left;">
                                            <p style="margin: 0;font-size: 12px;line-height: 14px;text-align: center">
                                                <span style="font-size: 14px; line-height: 16px;">Thanks</span></p>
                                            <p style="margin: 0;font-size: 12px;line-height: 14px;text-align: center">
                                                <span style="font-size: 14px; line-height: 16px;">Dropster Team</span>
                                            </p></div>
                                    </div>
                                    <!--[if mso]></td></tr></table><![endif]-->


                                    <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                            </div>
                        </div>
                        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                    </div>
                </div>
            </div>
            <div style="background-color:transparent;">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;"
                     class="block-grid ">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                        <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="background-color:transparent;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width: 600px;">
                                        <tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

                        <!--[if (mso)|(IE)]>
                        <td align="center" width="600"
                            style=" width:600px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top"><![endif]-->
                        <div class="col num12"
                             style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <!--[if (!mso)&(!IE)]><!-->
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                    <!--<![endif]-->


                                    <div style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px; ">
                                        <!--[if (mso)]>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="padding-right: 10px;padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                                    <table width="100%" align="center" cellpadding="0" cellspacing="0"
                                                           border="0">
                                                        <tr>
                                                            <td><![endif]-->
                                        <div align="center">
                                            <div style="border-top: 0px solid transparent; width:100%; line-height:0px; height:0px; font-size:0px;">
                                                &#160;
                                            </div>
                                        </div>
                                        <!--[if (mso)]></td></tr></table></td></tr></table><![endif]-->
                                    </div>


                                    <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                            </div>
                        </div>
                        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                    </div>
                </div>
            </div>
            <div style="background-color:transparent;">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;"
                     class="block-grid ">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                        <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="background-color:transparent;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width: 600px;">
                                        <tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

                        <!--[if (mso)|(IE)]>
                        <td align="center" width="600"
                            style=" width:600px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top"><![endif]-->
                        <div class="col num12"
                             style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <!--[if (!mso)&(!IE)]><!-->
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                    <!--<![endif]-->


                                    <div style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                        <!--[if (mso)]>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="padding-right: 10px;padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                                    <table width="100%" align="center" cellpadding="0" cellspacing="0"
                                                           border="0">
                                                        <tr>
                                                            <td><![endif]-->
                                        <div align="center">
                                            <div style="border-top: 0px solid transparent; width:100%; line-height:0px; height:0px; font-size:0px;">
                                                &#160;
                                            </div>
                                        </div>
                                        <!--[if (mso)]></td></tr></table></td></tr></table><![endif]-->
                                    </div>


                                    <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                            </div>
                        </div>
                        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                    </div>
                </div>
            </div>

            <div style="background-color:#41046A;">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #41046A;"
                     class="block-grid ">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:#41046A;">
                        <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="background-color:#41046A;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width: 600px;">
                                        <tr class="layout-full-width" style="background-color:#41046A;"><![endif]-->

                        <!--[if (mso)|(IE)]>
                        <td align="center" width="600"
                            style=" width:600px; padding-right: 0px; padding-left: 0px; padding-top:5px; padding-bottom:5px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top"><![endif]-->
                        <div class="col num12"
                             style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <!--[if (!mso)&(!IE)]><!-->
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:5px; padding-bottom:5px; padding-right: 0px; padding-left: 0px;">
                                    <!--<![endif]-->


                                    <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                    <![endif]-->
                                    <div style="line-height:120%;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;color:#FFFFFF; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                        <div style="font-size:12px;line-height:14px;color:#FFFFFF;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;text-align:left;">
                                            <p style="margin: 0;font-size: 12px;line-height: 14px;text-align: center">
                                                <span style="font-size: 12px; line-height: 14px;" id="_mce_caret"
                                                      data-mce-bogus="true"><span
                                                            style="font-size: 16px; line-height: 19px;">﻿</span></span><span
                                                        style="font-size: 16px; line-height: 19px;">www.dropsterng.com</span>
                                            </p>
                                            <p style="margin: 0;font-size: 12px;line-height: 14px;text-align: center">
                                                &#160;<br></p></div>
                                    </div>
                                    <!--[if mso]></td></tr></table><![endif]-->


                                    <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                    <![endif]-->
                                    <div style="line-height:120%;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;color:#FFFFFF; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                        <div style="font-size:12px;line-height:14px;color:#FFFFFF;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;text-align:left;">
                                            <p style="margin: 0;font-size: 12px;line-height: 14px;text-align: center">
                                                <span style="font-size: 20px; line-height: 24px;">Connect with Us</span>
                                            </p></div>
                                    </div>
                                    <!--[if mso]></td></tr></table><![endif]-->


                                    <div align="center"
                                         style="padding-right: 10px; padding-left: 10px; padding-bottom: 15px;">
                                        <div style="line-height:10px;font-size:1px">&#160;</div>
                                        <div style="display: table; max-width:225px;">
                                            <!--[if (mso)|(IE)]>
                                            <table width="205" cellpadding="0" cellspacing="0" border="0">
                                                <tr>
                                                    <td style="border-collapse:collapse; padding-right: 10px; padding-left: 10px; padding-bottom: 15px;"
                                                        align="center">
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                               style="border-collapse:collapse; mso-table-lspace: 0pt;mso-table-rspace: 0pt; width:205px;">
                                                            <tr>
                                                                <td width="32" style="width:32px; padding-right: 5px;"
                                                                    valign="top"><![endif]-->
                                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="32"
                                                   height="32"
                                                   style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px">
                                                <tbody>
                                                <tr style="vertical-align: top">
                                                    <td align="left" valign="middle"
                                                        style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                        <a href="https://www.facebook.com/" title="Facebook"
                                                           target="_blank">
                                                            <img src="images/facebook@2x.png" alt="Facebook"
                                                                 title="Facebook" width="32"
                                                                 style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                        </a>
                                                        <div style="line-height:5px;font-size:1px">&#160;</div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!--[if (mso)|(IE)]></td>
                                            <td width="32" style="width:32px; padding-right: 5px;" valign="top">
                                            <![endif]-->
                                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="32"
                                                   height="32"
                                                   style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px">
                                                <tbody>
                                                <tr style="vertical-align: top">
                                                    <td align="left" valign="middle"
                                                        style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                        <a href="https://twitter.com/" title="Twitter" target="_blank">
                                                            <img src="images/twitter@2x.png" alt="Twitter"
                                                                 title="Twitter" width="32"
                                                                 style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                        </a>
                                                        <div style="line-height:5px;font-size:1px">&#160;</div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!--[if (mso)|(IE)]></td>
                                            <td width="32" style="width:32px; padding-right: 5px;" valign="top">
                                            <![endif]-->
                                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="32"
                                                   height="32"
                                                   style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px">
                                                <tbody>
                                                <tr style="vertical-align: top">
                                                    <td align="left" valign="middle"
                                                        style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                        <a href="https://www.youtube.com/" title="YouTube"
                                                           target="_blank">
                                                            <img src="images/youtube@2x.png" alt="YouTube"
                                                                 title="YouTube" width="32"
                                                                 style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                        </a>
                                                        <div style="line-height:5px;font-size:1px">&#160;</div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!--[if (mso)|(IE)]></td>
                                            <td width="32" style="width:32px; padding-right: 5px;" valign="top">
                                            <![endif]-->
                                            <table align="left" border="0" cellspacing="0" cellpadding="0" width="32"
                                                   height="32"
                                                   style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 5px">
                                                <tbody>
                                                <tr style="vertical-align: top">
                                                    <td align="left" valign="middle"
                                                        style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
                                                        <a href="https://www.instagram.com/dropsterng" title="Insagram"
                                                           target="_blank">
                                                            <img src="{{url('images/instagram.png')}}" alt="Instagram"
                                                                 title="Instagram" width="32"
                                                                 style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">
                                                        </a>
                                                        <div style="line-height:5px;font-size:1px">&#160;</div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <!--[if (mso)|(IE)]></td>
                                            <td width="32" style="width:32px; padding-right: 0;" valign="top">
                                            <![endif]-->
                                            {{--<table align="left" border="0" cellspacing="0" cellpadding="0" width="32" height="32" style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;Margin-right: 0">--}}
                                              {{--<tbody><tr style="vertical-align: top"><td align="left" valign="middle" style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">--}}
                                                      {{--<a href="https://telegram.org" title="Telegram" target="_blank">--}}
                                                          {{--<img src="images/telegram@2x.png" alt="Telegram" title="Telegram" width="32" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: block !important;border: none;height: auto;float: none;max-width: 32px !important">--}}
                                                      {{--</a>--}}
                                                      {{--<div style="line-height:5px;font-size:1px">&#160;</div>--}}
                                                  {{--</td></tr>--}}
                                              {{--</tbody></table>--}}
                                          <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                                        </div>
                                    </div>


                                    <!--[if mso]>
                                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                        <tr>
                                            <td style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                    <![endif]-->
                                    <div style="line-height:120%;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;color:#FFFFFF; padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                        <div style="font-size:12px;line-height:14px;color:#FFFFFF;font-family:'Lato', Tahoma, Verdana, Segoe, sans-serif;text-align:left;">
                                            <p style="margin: 0;font-size: 14px;line-height: 17px;text-align: center">
                                                <strong>© 2017 <a href="http://www.gurudeveloperinc.com">Gurudeveloper
                                                        Inc.</a></strong> All rights reserved</p></div>
                                    </div>
                                    <!--[if mso]></td></tr></table><![endif]-->


                                    <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                            </div>
                        </div>
                        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                    </div>
                </div>
            </div>
            <div style="background-color:#F9F9F9;">
                <div style="Margin: 0 auto;min-width: 320px;max-width: 600px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: transparent;"
                     class="block-grid ">
                    <div style="border-collapse: collapse;display: table;width: 100%;background-color:transparent;">
                        <!--[if (mso)|(IE)]>
                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td style="background-color:#F9F9F9;" align="center">
                                    <table cellpadding="0" cellspacing="0" border="0" style="width: 600px;">
                                        <tr class="layout-full-width" style="background-color:transparent;"><![endif]-->

                        <!--[if (mso)|(IE)]>
                        <td align="center" width="600"
                            style=" width:600px; padding-right: 0px; padding-left: 0px; padding-top:15px; padding-bottom:0px; border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent;"
                            valign="top"><![endif]-->
                        <div class="col num12"
                             style="min-width: 320px;max-width: 600px;display: table-cell;vertical-align: top;">
                            <div style="background-color: transparent; width: 100% !important;">
                                <!--[if (!mso)&(!IE)]><!-->
                                <div style="border-top: 0px solid transparent; border-left: 0px solid transparent; border-bottom: 0px solid transparent; border-right: 0px solid transparent; padding-top:15px; padding-bottom:0px; padding-right: 0px; padding-left: 0px;">
                                    <!--<![endif]-->


                                    <div style="padding-right: 10px; padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                        <!--[if (mso)]>
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td style="padding-right: 10px;padding-left: 10px; padding-top: 10px; padding-bottom: 10px;">
                                                    <table width="100%" align="center" cellpadding="0" cellspacing="0"
                                                           border="0">
                                                        <tr>
                                                            <td><![endif]-->
                                        <div align="center">
                                            <div style="border-top: 0px solid transparent; width:100%; line-height:0px; height:0px; font-size:0px;">
                                                &#160;
                                            </div>
                                        </div>
                                        <!--[if (mso)]></td></tr></table></td></tr></table><![endif]-->
                                    </div>


                                    <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
                            </div>
                        </div>
                        <!--[if (mso)|(IE)]></td></tr></table></td></tr></table><![endif]-->
                    </div>
                </div>
            </div>
            <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
        </td>
    </tr>
    </tbody>
</table>
<!--[if (mso)|(IE)]></div><![endif]-->

</body>
</html>