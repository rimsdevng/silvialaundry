<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Dropster is an efficient on-demand delivery service that operates within the city. With dropster you can
    track deliveries in real time">
    <meta name="keywords" content="delivery, delivery companies in Nigeria, delivery services in Nigeria, delivery service
    dropster,lagos,gurudeveloper, gurudeveloper inc, gurudeveloper inclined limited, dropster app, drop it, #dropsterng, #dropit,
    abuja,nigeria,dropsterng,dropster nigeria,">
    <meta name="author" content="Gurudeveloper Inc.">
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('plugins/images/favicon.png')}}">
    <title>Dropster Admin</title>
    <!-- Core CSS -->
    <link href="{{url('bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{url('plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css')}}" rel="stylesheet">
    <link href="{{url('plugins/bower_components/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet">
    <!-- morris CSS -->
    <link href="{{url('plugins/bower_components/morrisjs/morris.css')}}" rel="stylesheet">
    <!-- animation CSS -->
    <link href="{{url('css/animate.css')}}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{url('css/style.css')}}" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{url('css/colors/default.css')}}" id="theme" rel="stylesheet">
    <link rel="stylesheet" href="{{url('css/font-awesome.min.css')}}">
    <script src="{{url('js/jquery.min.js')}}"></script>

    <link rel="stylesheet" href="{{url('css/jquery-ui.min.css')}}">

    <script type="text/javascript" src="{{url('js/jquery-ui.min.js')}}"></script>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyD38rNyWsCdOAxpvdeuHunyR6zLTxDfLYc&libraries=places&region=NG"></script>
</head>

<body>
<div id="wrapper">


    <!-- Top Navigation -->
    <nav class="navbar navbar-default navbar-static-top m-b-0">
        <div class="navbar-header">

            <!-- .Logo -->
            <div class="top-left-part">
                <a class="logo" href="{{url('/')}}">
                    <!--This is logo icon--><img src="{{url('img/dropster.JPG')}}" alt="home" class="light-logo" /></a>
            </div>
            <!-- /.Logo -->

            <!-- top right panel -->
            <ul class="nav navbar-top-links navbar-right pull-right">

                {{--the one tha comes from the top with tasks--}}
                <li>
                    <a title="Request for a customer" class="waves-effect waves-light" href="{{url('customer-request')}}"><i class="fa fa-plus"></i></a>

                </li>

                <li class="dropdown">
                    <a title="Search for transaction" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="fa fa-search"></i>
                        <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                    </a>

                    <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                        <li>
                            <input id="by" name="by" type="text" required class="form-control" placeholder="Find a Transaction by ID">
                            <button class="btn btn-primary" id="findTransaction">GO</button>
                            <br>
                            <script>
                                $(document).ready(function(){
                                    $('#findTransaction').on('click',function(){
                                        var by = $('#by').val();
                                        window.location = "{{url('view-delivery-detail')}}/" + by;
                                    });
                                })
                            </script>
                        </li>
                        
                        
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>


                <li class="dropdown">
                    <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="{{Auth::user()->image}}" width="36" class="img-circle"><b class="hidden-xs">{{Auth::user()->fname}} {{Auth::user()->sname}}</b> </a>
                    <ul class="dropdown-menu dropdown-user animated flipInY">
                        <li><a href="{{url('staff/' . Auth::user()->uid)}}"><i class="ti-user"></i> My Profile</a></li>

                        <li role="separator" class="divider"></li>
                        <li><a href="{{url('logout')}}"><i class="fa fa-power-off"></i> Logout</a></li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>

                {{--<li class="right-side-toggle"><a class="waves-effect waves-light" href="javascript:void(0)"><i class="ti-arrow-right"></i></a></li>--}}

            </ul>
            <!-- top right panel -->
        </div>
    </nav>
    <!-- End Top Navigation -->


    @include('sidebars.left')

    <!-- Page Content -->
        @yield('content')
    <!-- End Page Content -->


    {{--@include('sidebars.right')--}}

</div>
<!-- /#wrapper -->
<!-- jQuery -->
<footer class="footer text-center"> &copy; <?php echo date('Y') ?> Dropster by <a href="https://gurudeveloperinc.com">GuruDeveloper Inc.</a> </footer>

<!-- Bootstrap Core JavaScript -->
<script src="{{url('bootstrap/dist/js/tether.min.js')}}"></script>
<script src="{{url('bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{url('plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js')}}"></script>
<script src="{{url('plugins/bower_components/bootstrap-select/bootstrap-select.min.js')}}" type="text/javascript"></script>


<script>
    $(document).ready(function(){

        var previousComplete;
        var previousInProgress;
        var previousAvailable;
        var previousIssue;

        setInterval(function () {
            $.ajax({
                url:"{{url('status')}}",
                method: "post",
                data:{
                    _token : "{{csrf_token()}}"
                },
                success: function (response) {

                    if(
                        response.available > previousAvailable
                    ){
                        var audioElement = document.createElement('audio');
                        audioElement.setAttribute('src', "{{url('notification.mp3')}}");
                        audioElement.play();

                    }

                    previousAvailable  = response.available;
                    previousComplete   = response.complete;
                    previousInProgress = response.inprogress;
                    previousIssue      = response.issues;



                },
                error: function (error) {

                }
            });

        },5000);
    });
</script>

<!--slimscroll JavaScript -->
<script src="{{url('js/jquery.slimscroll.js')}}"></script>
<!--Wave Effects -->
<script src="{{url('js/waves.js')}}"></script>
<!--weather icon -->
<script src="{{url('plugins/bower_components/skycons/skycons.js')}}"></script>
<!--Counter js -->
<script src="{{url('plugins/bower_components/waypoints/lib/jquery.waypoints.js')}}"></script>
<script src="{{url('plugins/bower_components/counterup/jquery.counterup.min.js')}}"></script>
{{--<!--Morris JavaScript -->--}}
<script src="{{url('plugins/bower_components/raphael/raphael-min.js')}}"></script>
<script src="{{url('plugins/bower_components/morrisjs/morris.js')}}"></script>
<!-- Custom Theme JavaScript -->
<script src="{{url('js/custom.min.js')}}"></script>
<script src="{{url('js/dashboard4.js')}}"></script>
<!-- Sparkline chart JavaScript -->
<script src="{{url('plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{url('plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js')}}"></script>

<!-- Footable -->
<script src="{{url('plugins/bower_components/footable/js/footable.all.min.js')}}"></script>

<!--FooTable init-->
<script src="{{url('js/footable-init.js')}}"></script>

</body>

</html>
