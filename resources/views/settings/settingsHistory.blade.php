

<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">

                        <div class="white-box">
                            <h3 class="box-title m-b-0">Cost History</h3>

                        @include('notification')


                        <!-- .row -->
                            <div class="row bg-title" >
                                <div class="col-lg-12">
                                </div>
                                <div class="col-sm-6 col-md-6 col-xs-12">
                                    <ol class="breadcrumb pull-left">
                                        <li class="active">Profile</li>
                                    </ol>
                                </div>
                            </div>

                            <!-- .row -->
                            <div class="row">
                                <div class="col-md-12 col-xs-12">
                                    <div class="white-box">
                                        <ul class="nav customtab nav-tabs" role="tablist">

                                            <li role="presentation" class="nav-item"><a href="#basic" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Base Fare</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#details" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Cost per Km</span></a></li>
                                        </ul>
                                        <div class="tab-content">

                                            <!-- start tab 1 -->
                                            <div class="tab-pane active" id="basic">



                                                <hr>

                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th>Base Fare</th>
                                                        <th>Set</th>

                                                    </tr>

                                                    @foreach($basefares as $base)
                                                        <tr>
                                                            <td>{{$base->value}}</td>
                                                            <td>{{$base->created_at}}</td>


                                                        </tr>

                                                    @endforeach


                                                </table>

                                            </div>


                                            <!-- start tab 2  -->
                                            <div class="tab-pane" id="details">


                                                <div class="tab-pane active" id="basic">



                                                    <hr>

                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <th>Cost Per Km</th>
                                                            <th>Set</th>

                                                        </tr>

                                                        @foreach($costperkms as $km)
                                                            <tr>
                                                                <td>{{$km->value}}</td>
                                                                <td>{{$km->created_at}}</td>
                                                            </tr>

                                                        @endforeach

                                                    </table>

                                                </div>



                                                </div>

                                              </div>

                                            </div>

                                            <!-- end tab 2 -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection