<!-- .Side panel -->
<div class="side-mini-panel">
    <ul class="mini-nav">
        <div class="togglediv"><a href="javascript:void(0)" id="togglebtn"><i class="fa fa-bars"></i></a></div>

        <li>
            <a href="javascript:void(0)">
                <i class="fa fa-tachometer" aria-hidden="true"></i>
            </a>
            <div class="sidebarmenu">
                <h3 class="menu-title"> <a href="{{url('/')}}">Dashboard </a></h3>
                <ul class="sidebar-menu">
                    <li><a href="{{url('/')}}">DashBoard</a></li>
                </ul>
            </div>
        </li>

        <li>
            <a href="javascript:void(0)" title="Deliveries">
                <i class="fa fa-dropbox" aria-hidden="true"  ></i>
            </a>
            <div class="sidebarmenu">
                <h3 class="menu-title">Deliveries</h3>
                <ul class="sidebar-menu">

                    <li class="menu">
                        <a href="javascript:void(0)">Delivery Requests<i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="sub-menu">
                            <li><a href="{{url('lookup-requests?by=Available')}}">Available Deliveries</a></li>
                            <li><a href="{{url('lookup-requests?by=Pending')}}">Deliveries in progress</a></li>
                            <li><a href="{{url('lookup-requests?by=Issue')}}">Deliveries with issues</a></li>
                            <li><a href="{{url('lookup-requests?by=Complete')}}">Completed Deliveries</a></li>
                            <li><a href="{{url('lookup-requests?by=Cancelled')}}">Cancelled Deliveries</a></li>
                        </ul>
                    </li>


                    <li class="menu">
                        <a href="javascript:void(0)">Categories<i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="sub-menu">
                            <li><a href="{{url('/add-item-cat')}}">Add Category</a></li>
                            <li><a href="{{url('/view-item-cat')}}">View Categories</a></li>
                        </ul>
                    </li>


                    <li class="menu">
                        <a href="javascript:void(0)">Delivery Items<i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="sub-menu">
                            <li><a href="{{url('/view-delivery-items')}}">View Delivery Items</a></li>
                            <li><a href="{{url('/add-delivery-items')}}">Add Delivery Items</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </li>

        <li>
            <a href="javascript:void(0)" title="Customers">
                <i class="fa fa-users" aria-hidden="true"></i>
            </a>
            <div class="sidebarmenu">
                <h3 class="menu-title">Customers</h3>
                <ul class="sidebar-menu">
                    <li><a href="{{url('lookup-customers')}}">Lookup Customers</a></li>
                    <li><a href="{{url('customer-request')}}">Request for Customer</a></li>
                </ul>
            </div>
        </li>

        <li>
            <a href="javascript:void(0)" title="Droppers">
                <i class="fa fa-motorcycle" aria-hidden="true"></i>
            </a>
            <div class="sidebarmenu">
                <h3 class="menu-title">Droppers</h3>
                <ul class="sidebar-menu">
                    <li><a href="{{url('lookup-droppers')}}">Lookup Droppers</a></li>
                    <li><a href="{{url('register-dropper')}}">Register Dropper</a></li>
                </ul>
            </div>
        </li>

        <li>
            <a href="javascript:void(0)" title="Vehicle Partners">
                <i class="fa fa-handshake-o" aria-hidden="true"></i>
            </a>
            <div class="sidebarmenu">
                <h3 class="menu-title">Vehicle Partners</h3>
                <ul class="sidebar-menu">
                    <li class="menu">
                        <a href="javascript:void(0)">Individuals<i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="sub-menu">
                            <li><a href="{{url('lookup-vehicle-partners')}}">Lookup Vehicle Partners</a></li>
                            <li><a href="{{url('add-vehicle')}}">Add Vehicle</a></li>

                        </ul>
                    </li>

                    <li class="menu">
                        <a href="javascript:void(0)">Groups<i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="sub-menu">
                            <li><a href="{{url('add-group-vehicle')}}">Add Vehicle To Group</a></li>
                            <li><a href="{{url('create-group')}}">Create Group</a></li>
                            <li><a href="{{url('manage-group')}}">Manage Group</a></li>
                        </ul>
                    </li>

                    <li class="menu">
                        <a href="javascript:void(0)">Vehicles<i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="sub-menu">
                            <li><a href="{{url('view-vehicles')}}">Lookup Vehicles </a></li>
                            <li><a href="{{url('add-vehicle-cat')}}">Add Vehicle Category</a></li>
                            <li><a href="{{url('view-vehicle-categories')}}">View Vehicle Categories</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </li>

        <li>
            <a href="javascript:void(0)" title="Support">
                <i class="fa fa-question-circle" aria-hidden="true"  ></i>
            </a>
            <div class="sidebarmenu">
                <h3 class="menu-title">Support</h3>
                <ul class="sidebar-menu">
                    <li><a href="{{url('support-portal')}}">Access Support Portal</a></li>
                </ul>
            </div>
        </li>


        @if(Auth::user()->role == "Admin")
        <li>
            <a href="javascript:void(0)" title="Staff">
                <i class="fa fa-id-card" aria-hidden="true"  ></i>
            </a>
            <div class="sidebarmenu">
                <h3 class="menu-title">Staff</h3>
                <ul class="sidebar-menu">
                    <li><a href="{{url('/staff')}}">Lookup Staff</a></li>
                    <li><a href="{{url('/register')}}">Register Staff</a></li>
                </ul>
            </div>
        </li>

        <li>
            <a href="javascript:void(0)" title="Settings">
                <i class="fa fa-cog" aria-hidden="true"></i>
            </a>

            <div class="sidebarmenu">
                <h3 class="menu-title">Settings</h3>

                <ul class="sidebar-menu">

                    <li class="menu">
                        <a href="javascript:void(0)">Settings<i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="sub-menu">
                            <li><a href="{{url('/settings')}}">Manage</a></li>
                            <li><a href="{{url('/view-settings')}}">History</a></li>
                        </ul>
                    </li>

                    <li class="menu">
                        <a href="javascript:void(0)">Coupons<i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="sub-menu">
                            <li><a href="{{url('add-coupon')}}">Add Coupon</a></li>
                            <li><a href="{{url('manage-coupons')}}">Manage Coupons</a></li>
                        </ul>
                    </li>
                    <li class="menu">
                        <a href="javascript:void(0)">Notifications<i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="sub-menu">
                            <li><a href="{{url('send-notification')}}">Send Push Notification</a></li>
                            <li><a href="{{url('sent-notifications')}}">View Sent Notifications</a></li>
                        </ul>
                    </li>

                    <li class="menu">
                        <a href="javascript:void(0)">Actions<i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="sub-menu">
                            <li><a href="{{url('pay-vehicle-partners')}}">Pay Vehicle Partners</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
        </li>

        <li>
            <a href="javascript:void(0)" title="Documents">
                <i class="fa fa-info" aria-hidden="true"></i>
            </a>
            <div class="sidebarmenu">
                <h3 class="menu-title">Terms & Policies</h3>
                <ul class="sidebar-menu">
                    <li class="menu"> <a href="#">FAQs<i class="glyphicons glyphicons-taxi"></i></a>
                        <ul class="sub-menu">
                            <li><a href="{{url('add-faq')}}">Add FAQ</a></li>
                            <li><a href="{{url('faqs')}}">Manage FAQs</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('privacy')}}"><span>Privacy Policy</span></a></li>
                    <li><a href="{{url('terms')}}"><span>Terms & Conditions</span></a></li>
                    <li><a href="{{url('vpas')}}"><span>Vehicle Partner Agreements</span></a></li>
                </ul>
            </div>
        </li>

        @endif


    </ul>
</div>
<!-- /.Side panel -->

<script>
    $(document).ready(function(){
        setTimeout(function(){
            $('#togglebtn').click();
        },300);

        console.log('ready');

    });

</script>