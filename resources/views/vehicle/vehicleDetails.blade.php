
<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">

                        @include('notification')

                            <div class="row">

                                <div class=" col-md-12 col-xs-12 ">
                                    <div class="white-box">
                                        <ul class="nav customtab nav-tabs" role="tablist">

                                            <li role="presentation" class="nav-item"><a href="#basic" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Basic</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#maintenance" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Maintenance Logs</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#fuel" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Fuel Logs</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#transactions" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Transactions</span></a></li>
                                        </ul>
                                        <div class="tab-content">

                                            <!-- start tab 1 -->
                                            <div class="tab-pane active" id="basic">
                                                <div class="row">
                                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Owned By:</strong>
                                                        <br>
                                                        <p class="text-muted">
                                                            <a href="{{url('view-customer/' . $vehicle->Customer->cid)}}">
                                                                {{$vehicle->Customer->fname}} {{$vehicle->Customer->sname}}
                                                            </a>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6 b-r">
                                                        @if(isset($vehicle->Dropper))
                                                        <strong>Currently Assigned To</strong>
                                                        <br>
                                                        <p class="text-muted">
                                                            <a href="{{url('dropper/' . $vehicle->Dropper->Customer->cid)}}">
                                                                {{$vehicle->Dropper->Customer->fname}} {{$vehicle->Dropper->Customer->sname}}
                                                            </a>
                                                        </p>
                                                        @else
                                                            <strong>Currently Not Assigned</strong>
                                                        @endif
                                                    </div>
                                                    <div class="col-md-3 col-xs-6 b-r">
                                                        <strong> Date Registered</strong>
                                                        <br>
                                                        <p class="text-muted">{{Carbon::createFromFormat("Y-m-d H:i:s", $vehicle->created_at)->toDayDateTimeString()}}</p>
                                                    </div>
                                                    <div class="col-md-3 col-xs-6 b-r">
                                                        <strong>Vehicle Category</strong>
                                                        <br>
                                                        <p class="text-muted">{{$vehicle->VehicleCategory->name}}</p>
                                                    </div>

                                                    <div class="col-md-3 col-xs-6 b-r">
                                                        <strong>Total Transactions</strong>
                                                        <br>
                                                        <p class="text-muted">{{count($vehicle->Deliveries)}}</p>
                                                    </div>

                                                    <div class="col-md-3 col-xs-6 b-r">
                                                        <strong>Total Earnings</strong>
                                                        <br>
                                                        <p class="text-muted"> &#x20A6; {{$vehicle->earnings}}</p>
                                                    </div>




                                                </div>


                                                <hr>

                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th>Brand</th>
                                                        <th>Model</th>
                                                        <th>RegNo</th>
                                                        <th>Vehicle ID</th>

                                                    </tr>


                                                    <tr>
                                                        <td>{{$vehicle->brand}}</td>
                                                        <td>{{$vehicle->model}}</td>
                                                        <td>{{$vehicle->regno}}</td>
                                                        <td>{{str_pad( $vehicle->vid, 6, "0", STR_PAD_LEFT)}}</td>
                                                    </tr>

                                                </table>

                                                <a href="{{url('view-vehicles')}}" class="btn btn-success">Back</a>


                                            </div>

                                            <!-- start tab 2 -->
                                            <div class="tab-pane" id="maintenance">
                                                <table class="table table-hover toggle-circle" data-page-size="50">
                                                    <thead>
                                                    <tr>
                                                        <th data-sort-initial="true" data-toggle="true">Maintenance ID</th>
                                                        <th data-hide="phone, tablet">Log</th>
                                                        <th data-hide="phone, tablet">Details</th>
                                                        <th>Next Maintenance</th>
                                                        <th >Date Maintained</th>
                                                        <th >Entered By</th>
                                                        <th data-sort-ignore="true" class="min-width"> </th>
                                                    </tr>
                                                    </thead>
                                                    <div class="form-inline padding-bottom-15">
                                                        <div class="row">

                                                        </div>
                                                        <div class="col-sm-12 text-right m-b-20 pull-right">
                                                            <div class="form-group">
                                                                <input id="demo-input-search2" type="text" placeholder="Search" class="form-control" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <tbody>

                                                    @foreach($vehicle->Maintenance as $maintenance)
                                                        <tr>
                                                            <td>{{$maintenance->mid}}</td>
                                                            <td>{{$maintenance->log}}</td>
                                                            <td>{{$maintenance->details}}</td>
                                                            <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$maintenance->nextMaintenance)->toDayDateTimeString()}}</td>
                                                            <td>{{$maintenance->created_at->toDayDateTimeString() }}</td>
                                                            <td>{{$maintenance->User->fname}} {{$maintenance->User->sname}}</td>
                                                            <td>
                                                                <a href="{{url('edit-maintenance-log/' . $maintenance->mid)}}">
                                                                    <span class="label label-success">Edit Log</span>
                                                                </a>
                                                            </td>
                                                        </tr>

                                                    @endforeach

                                                    </tbody>

                                                    <tfoot>
                                                    <tr>
                                                        <td colspan="7">
                                                            <div class="text-right">
                                                                <ul class="pagination">
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tfoot>
                                                </table>


                                                <div class="panel row">
                                                    <div class="panel-heading">Add Maintenance Log</div>

                                                    <div class="panel-body col-md-8 col-md-offset-2">
                                                        <form method="post" action="{{url('add-maintenance')}}">
                                                            {{csrf_field()}}
                                                            <input type="hidden" name="vid" value="{{$vehicle->vid}}">
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label"> Brief Log </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" name="log" placeholder="Briefly state what was maintained" required autofocus>
                                                                </div>
                                                            </div> <br><br>

                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label">Details</label>

                                                                <div class="col-md-6">
                                                                    <textarea placeholder="Enter log details if any" class="form-control" name="details"></textarea>
                                                                </div>
                                                            </div> <br><br>

                                                            <div class="form-group" >
                                                                <div class="col-md-10 col-md-offset-4">
                                                                    <button class="btn btn-success">Add</button>
                                                                </div>
                                                            </div>

                                                        </form>

                                                    </div>
                                                </div>

                                            </div>

                                            <!-- start tab 3 -->
                                            <div class="tab-pane" id="fuel">
                                                <table class="table table-hover toggle-circle" data-page-size="50">
                                                    <thead>
                                                    <tr>
                                                        <th data-hide="phone, tablet">Log</th>
                                                        <th data-hide="phone, tablet">Amount</th>
                                                        <th>Station</th>
                                                        <th >Entered By</th>
                                                        <th >Date</th>
                                                        <th data-sort-ignore="true" class="min-width"> </th>
                                                    </tr>
                                                    </thead>
                                                    <div class="form-inline padding-bottom-15">
                                                        <div class="row">

                                                        </div>
                                                        <div class="col-sm-12 text-right m-b-20 pull-right">
                                                            <div class="form-group">
                                                                <input id="demo-input-search2" type="text" placeholder="Search" class="form-control" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <tbody>

                                                    @foreach(collect($vehicle->Fuel)->reverse() as $fuel)
                                                        <tr>
                                                            <td>{{$fuel->fid}}</td>
                                                            <td>{{$fuel->amount}}</td>
                                                            <td>{{$fuel->station}}</td>
                                                            <td>{{$fuel->User->fname}} {{$fuel->User->sname}}</td>
                                                            <td>{{$fuel->created_at->toDayDateTimeString()}}</td>
                                                            <td>
                                                                <a href="{{url('edit-fuel-log/' . $fuel->fid)}}">
                                                                    <span class="label label-success">Edit Log</span>
                                                                </a>
                                                            </td>
                                                        </tr>

                                                    @endforeach

                                                    </tbody>

                                                    <tfoot>
                                                    <tr>
                                                        <td colspan="7">
                                                            <div class="text-right">
                                                                <ul class="pagination">
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tfoot>
                                                </table>


                                                <div class="panel row">
                                                    <div class="panel-heading">Add Fuel Log</div>

                                                    <div class="panel-body col-md-8 col-md-offset-2">
                                                        <form method="post" action="{{url('add-fuel')}}">
                                                            {{csrf_field()}}
                                                            <input type="hidden" name="vid" value="{{$vehicle->vid}}">
                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label"> Station </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" name="station" placeholder="What filling station" required autofocus>
                                                                </div>
                                                            </div> <br><br>

                                                            <div class="form-group">
                                                                <label class="col-md-4 control-label"> Amount </label>
                                                                <div class="col-md-6">
                                                                    <input type="number" class="form-control" name="amount" placeholder="Amount Of fuel put" required autofocus>
                                                                </div>
                                                            </div> <br><br>

                                                            <div class="form-group" >
                                                                <div class="col-md-10 col-md-offset-4">
                                                                    <button class="btn btn-success">Add</button>
                                                                </div>
                                                            </div>

                                                        </form>

                                                    </div>
                                                </div>

                                            </div>
                                            <!-- end tab 3 -->


                                            <!-- start tab 4 -->
                                            <div class="tab-pane" id="transactions">

                                                <div class="col-md-6">
                                                    <h3>Total Transactions - {{count($vehicle->Deliveries)}}</h3>
                                                </div>

                                                <table id="demo-foo-addrow" class="table table-bordered table-hover toggle-circle" data-page-size="10">
                                                    <thead>
                                                    <tr>
                                                        <th data-sort-initial="false" data-toggle="true">From</th>
                                                        <th>To</th>
                                                        <th data-hide="phone, tablet">Receiver Name</th>
                                                        <th data-hide="phone, tablet">Email</th>
                                                        <th data-hide="phone, tablet">Images</th>
                                                        <th >Status</th>
                                                        <th >Price(₦)</th>
                                                        <th >Date</th>
                                                        <th data-sort-ignore="true" class="min-width"> </th>
                                                    </tr>
                                                    </thead>
                                                    <div class="form-inline padding-bottom-15">
                                                        <div class="row">

                                                        </div>
                                                        <div class="col-sm-6 text-right m-b-20">
                                                            <div class="form-group">
                                                                <input id="demo-input-search2" type="text" placeholder="Search" class="form-control" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <tbody>

                                                    @foreach($vehicle->Deliveries as $item)
                                                        <tr>
                                                            <td>{{$item->from}}</td>
                                                            <td>{{$item->to}}</td>
                                                            <td>{{$item->fname}} {{$item->sname}}</td>
                                                            <td>{{$item->email}}</td>
                                                            <td>{{count($item->Images)}}</td>
                                                            <td>
                                                                @if($item->status == 'Issue')
                                                                    <span class="label label-danger">
                                                                            {{$item->status}}
                                                                        </span>
                                                                @elseif($item->status == 'Pending')
                                                                    <span class="label label-warning">
                                                                            {{$item->status}}
                                                                        </span>
                                                                @elseif($item->status == 'Completed')
                                                                    <span class="label label-success">
                                                                            {{$item->status}}
                                                                        </span>
                                                                @else
                                                                    <span class="label label-primary">
                                                                        {{$item->status}}
                                                                    </span>
                                                                @endif
                                                            </td>
                                                            <td>{{$item->amount}}</td>
                                                            <td>{{Carbon::createFromFormat("Y-m-d H:i:s",$item->created_at)->toDateTimeString()}}</td>
                                                            <td>
                                                                <a href="{{url('view-delivery-detail/' . $item->did)}}">
                                                                    <span class="label label-success">View</span>
                                                                </a>
                                                            </td>
                                                        </tr>

                                                    @endforeach

                                                    </tbody>

                                                    <tfoot>
                                                    <tr>
                                                        <td colspan="6">
                                                            <div class="text-right">
                                                                <ul class="pagination">
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tfoot>
                                                </table>

                                            </div>
                                            <!-- end tab 4 -->



                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection