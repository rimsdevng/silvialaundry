@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Available Vehicles</h3>
                            <p class="box-title m-b-0 small">There are {{count($vehicles)}} vehicles in total</p>

                            <table id="demo-foo-addrow" class="table table-hover toggle-circle" data-page-size="50">
                                <thead>
                                <tr>
                                    <th data-sort-initial="true" data-toggle="true">Vehicle ID</th>
                                    <th data-hide="phone, tablet">Brand</th>
                                    <th>Model</th>
                                    <th data-hide="phone, tablet">Color</th>
                                    <th >Registration Number</th>
                                    <th >Last Maintenance</th>
                                    <th data-sort-ignore="true" class="min-width"> </th>
                                </tr>
                                </thead>
                                <div class="form-inline padding-bottom-15">
                                    <div class="row">

                                    </div>
                                    <div class="col-sm-12 text-right m-b-20 pull-right">
                                        <div class="form-group">
                                            <input id="demo-input-search2" type="text" placeholder="Search" class="form-control" autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <tbody>

                                @foreach($vehicles as $vehicle)
                                    <tr>
                                        <td>{{$vehicle->vid}}</td>
                                        <td>{{$vehicle->brand}}</td>
                                        <td>{{$vehicle->model}}</td>
                                        <td>{{$vehicle->color}}</td>
                                        <td>{{$vehicle->regno}}</td>
                                        <td>

                                            @if(count($vehicle->Maintenance) > 0)
                                            {{$vehicle->Maintenance->last()->created_at->toDayDateTimeString()}}
                                            @else
                                                Not yet maintained
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{url('view-vehicle/' . $vehicle->vid)}}">
                                                <span class="label label-success">View Vehicle</span>
                                            </a>
                                       </td>
                                    </tr>

                                @endforeach

                                </tbody>

                                <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <div class="text-right">
                                            <ul class="pagination">
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection