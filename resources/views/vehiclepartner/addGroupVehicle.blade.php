@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">

                            @include('notification')
                            <h4>Group Name: {{$group->name}}</h4>

                            <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data" action="{{url('add-group-vehicle')}}">


                                {{ csrf_field() }}

                                <label> Add Vehicle Information</label>

                                <input type="hidden" name="gid" value="{{$group->gid}}">


                                <div class="form-group">
                                    <label for="vcid" class="col-md-4 control-label">Vehicle Category</label>

                                    <div class="col-md-6" class=" form">

                                        <select class="form-control" name="vcid">
                                            @foreach( $vcs as $vc) <option value="{{$vc->vcid}}" >{{$vc->name}}</option>   @endforeach
                                        </select>

                                    </div>
                                </div>




                                <div class="form-group{{ $errors->has('brand') ? ' has-error' : '' }}">
                                    <label for="fname" class="col-md-4 control-label">Vehicle Brand</label>

                                    <div class="col-md-6">
                                        <input id="brand" type="text" class="form-control" name="brand"  required autofocus>

                                        @if ($errors->has('brand'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('brand') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('model') ? ' has-error' : '' }}">
                                    <label for="dob" class="col-md-4 control-label">vehicle Model</label>

                                    <div class="col-md-6">
                                        <input id="model" type="text" class="form-control" name="model"  required autofocus>

                                        @if ($errors->has('model'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('model') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>



                                <div class="form-group{{ $errors->has('color') ? ' has-error' : '' }}">
                                    <label for="color" class="col-md-4 control-label">Color</label>

                                    <div class="col-md-6">
                                        <input id="color" type="text" class="form-control" name="color"  required autofocus>

                                        @if ($errors->has('color'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('color') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('regno') ? ' has-error' : '' }}">
                                    <label for="regno" class="col-md-4 control-label">Reg. Number</label>

                                    <div class="col-md-6">
                                        <input id="regno" type="text" class="form-control" name="regno"  required autofocus>

                                        @if ($errors->has('regno'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('regno') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-4">
                                        <button type="submit" class="btn btn-primary">
                                            Add Vehicle
                                        </button>

                                        <a class="btn btn-primary" href="{{url('add-vehicle-to-group')}}">Go Back</a>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

