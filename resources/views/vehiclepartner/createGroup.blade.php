@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">

                            @include('notification')

                            <h3>Create A Group</h3>
                            <form action="{{url('create-group')}}" method="post">
                                {{csrf_field()}}

                                <div class="form-group">
                                    <label >Group Name</label>
                                    <input type="text" name="name" value="{{old('name')}}">
                                </div>

                                <div class="form-group">
                                    <label>Vehicle Category</label>
                                    <select name="vcid">
                                        @foreach($categories as $item)
                                            <option value="{{$item->vcid}}">{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Total Amount</label>
                                    <input type="text" name="total" value="{{old('total')}}">
                                </div>

                                <button class="btn btn-success">Create</button>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection