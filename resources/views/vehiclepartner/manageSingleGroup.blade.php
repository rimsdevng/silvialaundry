<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

        <div class="container-fluid">
                <div id="page-wrapper">
                    <div class="container-fluid">
                        <!-- .row -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="white-box">

                                    @include('notification')

                                    <!-- .row -->
                                    <div class="row bg-title" >
                                        <div class="col-lg-12">
                                        </div>
                                    </div>

                                    <!-- .row -->
                                    <div class="row">
                                        <div class="col-md-4 col-xs-12">
                                            <div class="white-box">
                                                <div class="user-bg"> <img width="100%" alt="user" src="{{url('img/landscape17.jpg')}}">
                                                    <div class="overlay-box">
                                                        <div class="user-content">
                                                            <h4 class="text-white">{{$group->name}}</h4>
                                                            <h5 class="text-white">Created {{Carbon::createFromFormat("Y-m-d H:i:s",$group->created_at)->toFormattedDateString()}} </h5>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="user-btm-box">
                                                    <div class="col-md-4 col-sm-4 text-center">

                                                    </div>
                                                    <div class="col-md-4 col-sm-4 text-center">

                                                    </div>
                                                    <div class="col-md-4 col-sm-4 text-center">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-xs-12">
                                            <div class="white-box">
                                                <ul class="nav customtab nav-tabs" role="tablist">

                                                    <li role="presentation" class="nav-item"><a href="#details" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Details</span></a></li>
                                                    <li role="presentation" class="nav-item"><a href="#addMember" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Add Member</span></a></li>
                                                </ul>
                                                <div class="tab-content">

                                                    <!-- start tab 1 -->
                                                    <div class="tab-pane active" id="details">
                                                        <div class="row">
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Total Members</strong>
                                                                <br>
                                                                <p class="text-muted">{{count($group->Members)}}</p>
                                                            </div>
                                                            <div class="col-md-3 col-xs-6 b-r"> <strong>Total Amount</strong>
                                                                <br>
                                                                <p class="text-muted">{{$group->total}}</p>
                                                            </div>

                                                        </div>
                                                        <hr>

                                                        <table class="table table-bordered">
                                                            <tr>
                                                                <th>First Name</th>
                                                                <th>Surname</th>
                                                                <th>Phone</th>
                                                                <th>Email</th>
                                                                <th>Owns</th>
                                                            </tr>

                                                                @foreach($group->Members as $item)
                                                                    <tr>
                                                                        <td>{{$item->Customer->fname}}</td>
                                                                        <td>{{$item->Customer->sname}}</td>
                                                                        <td>{{$item->Customer->phone}}</td>
                                                                        <td>{{$item->Customer->email}}</td>
                                                                        <td>{{$item->percent}}%</td>
                                                                    </tr>
                                                                @endforeach

                                                        </table>

                                                    </div>


                                                    <!-- start tab 2  -->
                                                    <div class="tab-pane" id="addMember">
                                                        <form class="form-horizontal form-material" method="post" action="{{url('add-group-member')}}">
                                                            {{csrf_field()}}

                                                            <input type="hidden" name="gid" value="{{$group->gid}}">

                                                            <div class="form-group">
                                                                <label class="col-md-12">Email</label>
                                                                <div class="col-md-12">
                                                                    <input type="text" placeholder="Type users email here" name="email" class="form-control form-control-line">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <label class="col-md-12">Amount</label>
                                                                <div class="col-md-12">
                                                                    <input type="number" placeholder="Type amount user paid" name="amount" class="form-control form-control-line">
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-sm-12">
                                                                    <button class="btn btn-success">Add</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>


@endsection