

<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- .row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">

                        @include('notification')

                        <!-- .row -->
                            <div class="row bg-title" >
                                <div class="col-lg-12">
                                    VEHICLE PARTNER DETAILS
                                </div>
                            </div>

                            <!-- .row -->
                            <div class="row">

                                <!-- from here -->
                                <div class="col-md-12 col-xs-12">
                                    <div class="white-box">
                                        <ul class="nav customtab nav-tabs" role="tablist">

                                            <li role="presentation" class="nav-item"><a href="#basic" class="nav-link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Basic</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#details" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Details</span></a></li>
                                            <li role="presentation" class="nav-item"><a href="#transactions" class="nav-link" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Vehicles</span></a></li>
                                        </ul>
                                        <div class="tab-content">

                                            <!-- start tab 1 -->
                                            <div class="tab-pane active" id="basic">
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="white-box">
                                                        <div class="user-bg"> <img width="100%" alt="user" src="{{url('img/landscape17.jpg')}}">
                                                            <div class="overlay-box">
                                                                <div class="user-content">
                                                                    <a href="javascript:void(0)"><img src="{{$customer->image}}" class="thumb-lg img-circle" alt="img"></a>
                                                                    <h4 class="text-white">{{$customer->fname}} {{$customer->sname}}</h4>
                                                                    <h5 class="text-white">Created {{Carbon::createFromFormat("Y-m-d H:i:s",$customer->created_at)->toFormattedDateString()}} </h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="user-btm-box">
                                                            <div class="col-md-4 col-sm-4 text-center">

                                                            </div>
                                                            <div class="col-md-4 col-sm-4 text-center">

                                                            </div>
                                                            <div class="col-md-4 col-sm-4 text-center">

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-8">
                                                    <div class="row">
                                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Gender</strong>
                                                            <br>
                                                            <p class="text-muted">{{ $customer->gender }}</p>
                                                        </div>
                                                        <div class="col-md-3 col-xs-6 b-r">
                                                            <strong>Joined</strong>
                                                            <br>
                                                            <p class="text-muted">{{Carbon::createFromFormat("Y-m-d H:i:s",$customer->created_at)->diffForHumans()}}</p>
                                                        </div>

                                                        <div class="col-md-3 col-xs-6 b-r">
                                                            <strong>Vehicles Owned</strong>
                                                            <br>
                                                            <p class="text-muted">{{count($customer->Vehicles)}}</p>
                                                        </div>

                                                        <div class="col-md-3 col-xs-6 b-r">
                                                            <strong>Earnings</strong>
                                                            <br>
                                                            <p class="text-muted">₦{{$earnings}}</p>
                                                        </div>

                                                    </div>


                                                    <hr>

                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <th>First Name</th>
                                                            <th>Surname</th>
                                                            <th>Phone</th>
                                                            <th>Email</th>
                                                        </tr>


                                                        <tr>
                                                            <td>{{$customer->fname}}</td>
                                                            <td>{{$customer->sname}}</td>
                                                            <td>{{$customer->phone}}</td>
                                                            <td>{{$customer->email}}</td>
                                                        </tr>

                                                    </table>

                                                    <a href="{{url('lookup-vehicle-partners')}}" class="btn btn-warning">Back</a>

                                                </div>



                                            </div>
                                            <!-- end tab 1 -->

                                            <!-- start tab 2  -->
                                            <div class="tab-pane" id="details">

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Firstname</label>
                                                        <div class="col-md-12">
                                                            <p>  {{$customer->fname}}</p>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-12">Surname</label>
                                                        <div class="col-md-12">
                                                            <p>  {{$customer->sname}}</p>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-12">Email</label>
                                                        <div class="col-md-12">
                                                            <p>  {{$customer->email}}</p>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-12">Phone</label>
                                                        <div class="col-md-12">
                                                            <p>  {{$customer->phone}}</p>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-12">Gender</label>
                                                        <div class="col-md-12">
                                                            <p>  {{$customer->gender}}</p>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-12">Address</label>
                                                        <div class="col-md-12">
                                                            <p>  {{$customer->address}}</p>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Bank Name</label>
                                                        <div class="col-md-12">
                                                            <p>  {{$customer->accountBank}}</p>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-12">Account Name</label>
                                                        <div class="col-md-12">
                                                            <p>  {{$customer->accountName}}</p>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-12">Account Number</label>
                                                        <div class="col-md-12">
                                                            <p>  <?php
                                                                $number = str_split($customer->accountNumber,6);
                                                                echo str_pad($number[count($number)-1],strlen($customer->AccountNumber),"*",STR_PAD_LEFT);
                                                                ?></p>
                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                            <!-- end tab 2 -->

                                            <!-- start tab 3 -->
                                            <div class="tab-pane" id="transactions">
                                                <table id="demo-foo-addrow" class="table table-bordered table-hover toggle-circle" data-page-size="7">
                                                    <thead>
                                                    <tr>
                                                        <th data-sort-initial="true" data-toggle="true">Vehicle ID</th>
                                                        <th data-hide="phone, tablet">Brand</th>
                                                        <th>Model</th>
                                                        <th data-hide="phone, tablet">Color</th>
                                                        <th >Registration Number</th>
                                                        <th >Earnings</th>
                                                        <th data-sort-ignore="true" class="min-width"> </th>
                                                    </tr>
                                                    </thead>
                                                    <div class="form-inline padding-bottom-15">
                                                        <div class="row">

                                                        </div>
                                                        <div class="col-sm-12 text-right m-b-20 pull-right">
                                                            <div class="form-group">
                                                                <input id="demo-input-search2" type="text" placeholder="Search" class="form-control" autocomplete="off">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <tbody>

                                                    @foreach($customer->Vehicles as $vehicle)
                                                        <tr>
                                                            <td>{{$vehicle->vid}}</td>
                                                            <td>{{$vehicle->brand}}</td>
                                                            <td>{{$vehicle->model}}</td>
                                                            <td>{{$vehicle->color}}</td>
                                                            <td>{{$vehicle->regno}}</td>
                                                            <td>{{$vehicle->earnings}}</td>
                                                            <td>
                                                                <a href="{{url('view-vehicles/' . $vehicle->vid)}}">
                                                                    <span class="label label-success">View Vehicle</span>
                                                                </a>
                                                            </td>
                                                        </tr>

                                                    @endforeach

                                                    </tbody>

                                                    <tfoot>
                                                    <tr>
                                                        <td colspan="6">
                                                            <div class="text-right">
                                                                <ul class="pagination">
                                                                </ul>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    </tfoot>
                                                </table>


                                            </div>
                                            <!-- end tab 3 -->


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection