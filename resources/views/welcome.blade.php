<?php use Carbon\Carbon; ?>
@extends('layouts.app')

@section('content')

    <div id="page-wrapper">
        <div class="container-fluid">
            <!-- .row -->
            <div class="row">
                <div class="col-md-12">
                    @include('notification')
                    <div class="white-box">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-inverse">
                                    <img class="card-img" src="{{url('img/c.jpg')}}" alt="Card image">
                                    <div class="card-img-overlay">
                                        <h4 class="card-title">DROPSTER</h4>
                                        <p class="card-text">
                                            Our Company would only succeed if you carry out your task efficiently.You are That Important!
                                        </p>
                                        <p class="card-text"><small class="text-white">Last updated 3 mins ago</small></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <!-- .row -->
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12">
                    <div class="white-box">
                        <div class="row row-in">
                            <div class="col-lg-3 col-sm-6 row-in-br">
                            <a href='{{url('lookup-requests?by=Complete')}}'>
                                <div class="col-in row">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> <i data-icon="E" class="linea-icon linea-basic"></i>
                                        <h5 class="text-muted vb">Total Completed Deliveries</h5>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <h3 class="counter text-right m-t-15 text-danger" id="complete">{{count($deliveries)}}</h3>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>

                            <div class="col-lg-3 col-sm-6 b-0">
                                <a href='{{url('lookup-requests?by=Available')}}'>
                                <div class="col-in row">
                                    <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe016;"></i>
                                        <h5 class="text-muted vb">Available Deliveries</h5>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <h3 class="counter text-right m-t-15 text-success" id="available">{{count($available)}}</h3>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>


                            <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                                <div class="col-in row">
                                <a href='{{url('lookup-requests?by=Pending')}}'>
                                    <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe01b;"></i>
                                        <h5 class="text-muted vb">Deliveries in Progress</h5>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <h3 class="counter text-right m-t-15 text-megna" id="inprogress">{{count($inprogress)}}</h3>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-megna" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-sm-6 row-in-br">
                                <div class="col-in row">
                                <a href='{{url('lookup-requests?by=Issue')}}'>
                                    <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe00b;"></i>
                                        <h5 class="text-muted vb">Deliveries with Issues</h5>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <h3 class="counter text-right m-t-15 text-primary" id="issues">{{count($issues)}}</h3>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                        </div>
                                    </div>
                                    </a>
                                </div>
                            </div>




                        </div>

                        <div id="moreStats" class="collapse">
                            <div class="row row-in">
                                <div class="col-lg-3 col-sm-6  b-0">
                                    <div class="col-in row">
                                        <a href='{{url('lookup-customers')}}'>

                                            <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe016;"></i>
                                                <h5 class="text-muted vb">Registered Customers</h5>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <h3 class="counter text-right m-t-15 text-success" id="customers">{{count($customers)}}</h3>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-6  b-0">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe016;"></i>
                                            <h5 class="text-muted vb">Registered Partners</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h3 class="counter text-right m-t-15 text-success" id="partners">{{count($partners)}}</h3>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-6  b-0">
                                    <div class="col-in row">

                                        <a href='{{url('lookup-droppers')}}'>
                                            <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe016;"></i>
                                                <h5 class="text-muted vb"> Registered Droppers </h5>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <h3 class="counter text-right m-t-15 text-success" id="droppers">{{count($droppers)}}</h3>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="progress">
                                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                                </div>
                                            </div>
                                        </a>

                                    </div>
                                </div>
                                <div class="col-lg-3 col-sm-6  b-0">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe016;"></i>
                                            <h5 class="text-muted vb">Total Vehicles</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h3 class="counter text-right m-t-15 text-success" id="totalVehicles">{{count($vehicles)}}</h3>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row row-in">
                                <div class="col-lg-3 col-sm-6  b-0">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"><h3 class="text-success">₦</h3>
                                            <h5 class="text-muted vb">All Transactions Value</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h3 class="counter text-right m-t-15 text-success" id="allTransactionsValue">{{$allTransactionsValue}}</h3>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-3 col-sm-6  b-0">
                                    <div class="col-in row">
                                        <div class="col-md-6 col-sm-6 col-xs-6"><h3 class="text-success">₦</h3>
                                            <h5 class="text-muted vb">Today's Transactions Value</h5>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <h3 class="counter text-right m-t-15 text-success" id="todaysTransactionsValue">{{$todaysTransactionsValue}}</h3>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                {{--<div class="col-lg-3 col-sm-6  b-0">--}}
                                    {{--<div class="col-in row">--}}
                                        {{--<div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe016;"></i>--}}
                                            {{--<h5 class="text-muted vb"> This week's Transactions Value</h5>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-6 col-sm-6 col-xs-6">--}}
                                            {{--<h3 class="counter text-right m-t-15 text-success">{{count($droppers)}}</h3>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-12 col-sm-12 col-xs-12">--}}
                                            {{--<div class="progress">--}}
                                                {{--<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-lg-3 col-sm-6  b-0">--}}
                                    {{--<div class="col-in row">--}}
                                        {{--<div class="col-md-6 col-sm-6 col-xs-6"> <i class="linea-icon linea-basic" data-icon="&#xe016;"></i>--}}
                                            {{--<h5 class="text-muted vb">Total Vehicles</h5>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-6 col-sm-6 col-xs-6">--}}
                                            {{--<h3 class="counter text-right m-t-15 text-success">{{count($vehicles)}}</h3>--}}
                                        {{--</div>--}}
                                        {{--<div class="col-md-12 col-sm-12 col-xs-12">--}}
                                            {{--<div class="progress">--}}
                                                {{--<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                            </div>
                        </div>

                        <script>

                            $(document).ready(function(){
                                $('#icon').on('click',function(){

                                    console.log('clicked');
                                    console.log(icon.hasClass('fa-angle-double-down');
                                    if(icon.hasClass('fa-angle-double-down')){
                                        icon.removeClass('fa-angle-double-down');
                                        icon.addClass('fa-angle-double-up');
                                    } else {
                                        icon.removeClass('fa-angle-double-up');
                                        icon.addClass('fa-angle-double-down');
                                    }

                                });
                            });
                        </script>

                        <a id="icon" class="btn btn-primary" data-toggle="collapse" data-target="#moreStats">
                            <i class="fa fa-angle-double-down"></i> More
                        </a> <br><br>


                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title">Recent Deliveries</h3>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>

                                    <th>Transaction-ID</th>
                                    <th>User</th>
                                    <th>Order date</th>
                                    <th>Amount</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Delivery Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>

                                    @foreach($recentdeliveries as $delivery)

                                    <td>
                                        <a href="{{url('view-delivery-detail'.'/'. $delivery->did)}} ">
                                            #{{str_pad( $delivery->did, 6, "0", STR_PAD_LEFT)}}
                                        </a>
                                    </td>
                                    <td>{{$delivery->fname}}</td>
                                    <td>
                                        <span class="text-muted">
                                            <i class="fa fa-clock-o"></i>
                                            {{Carbon::createFromFormat("Y-m-d H:i:s",$delivery->created_at)->diffForHumans()}}

                                        </span>
                                    </td>
                                    <td>&#8358  {{number_format($delivery->amount,2)}}</td>
                                    <td class="text-center">
                                        @if($delivery->paymentStatus == "paid")
                                            <label class="label label-success">Paid</label>
                                        @endif

                                        @if($delivery->paymentStatus == "unpaid")
                                            <label class="label label-danger">Un-Paid</label>
                                        @endif

                                    </td>
                                    <td class="text-center">
                                        @if($delivery->status == "Pending")
                                            <label class="label label-warning">Pending</label>
                                        @endif

                                        @if($delivery->status == "Complete")
                                            <label class="label label-success">Complete</label>
                                        @endif

                                        @if($delivery->status == "Available")
                                            <label class="label label-primary">Available</label>
                                        @endif

                                        @if($delivery->status == "Issue")
                                            <label class="label label-danger">Issue</label>
                                        @endif

                                        @if($delivery->status == "Cancelled")
                                            <label class="label label-default" style="background-color: saddlebrown">Cancelled</label>
                                        @endif

                                    </td>
                                </tr>
                                        
                                @endforeach
                                {{$recentdeliveries->links()}}
                                </tbody>
                            </table>
                             
                        </div>
                    </div>
                </div>
            </div>
            <!--/.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="white-box">
                        <h3 class="box-title">Delivery Issues</h3>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Ticket Number</th>
                                    <th>User</th>
                                    <th>Date</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Tracking Number</th>
                                </tr>
                                </thead>
                                <tbody>



                                    @foreach( $recentissues as $issue)
                                        <tr>

                                        <td>
                                            <a href="{{url('view-delivery-detail'.'/'. $issue->did)}} ">
                                                #{{str_pad( $issue->did, 6, "0", STR_PAD_LEFT)}}
                                            </a>
                                        </td>
                                        <td>{{$issue->fname}}</td>
                                        <td><span class="text-muted"><i class="fa fa-clock-o"></i> {{$issue->created_at}}</span></td>

                                        <td class="text-center">
                                            <div class="label label-table label-success">{{$issue->paymentStatus}}</div>
                                        </td>
                                        <td class="text-center">-</td>

                                </tr>


                                    @endforeach

                                    {{$recentissues->links()}}

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>


    <script>
        $(document).ready(function(){

            var previousComplete;
            var previousInProgress;
            var previousAvailable;
            var previousIssue;

            setInterval(function () {
                $.ajax({
                    url:"{{url('status')}}",
                    method: "post",
                    data:{
                        _token : "{{csrf_token()}}"
                    },
                    success: function (response) {

                        {{--if(--}}
                            {{--response.available > previousAvailable--}}
                        {{--){--}}
                            {{--var audioElement = document.createElement('audio');--}}
                            {{--audioElement.setAttribute('src', "{{url('notification.mp3')}}");--}}
                            {{--audioElement.play();--}}

                        {{--}--}}

                        $('#complete').text(response.deliveries);
                        $('#inprogress').text(response.inprogress);
                        $('#available').text(response.available);
                        $('#issues').text(response.issues);
                        $('#droppers').text(response.droppers);
                        $('#totalVehicles').text(response.vehicles);
                        $('#customers').text(response.customers);
                        $('#partners').text(response.partners);
                        $('#allTransactionsValue').text(response.allTransactionsValue);
                        $('#todaysTransactionsValue').text(response.todaysTransactionsValue);


                        previousAvailable  = response.available;
                        previousComplete   = response.complete;
                        previousInProgress = response.inprogress;
                        previousIssue      = response.issues;



                    },
                    error: function (error) {

                    }
                });

            },5000);
        });
    </script>


@endsection