<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//admin

//Route::post('customers','ApiController@customers');

Route::group(['middleware' => 'cors'], function(){


    //customers
    Route::get('delivery-issue/{did}', 'ApiController@deliveryIssue');
    Route::get('delivery-completed/{did}', 'ApiController@deliveryCompleted');
    Route::get('delivery-in-progress/{did}', 'ApiController@deliveryInProgress');
    Route::get('delivery-items','ApiController@deliveryItems');

    Route::get('customer-received-deliveries/{cid}', 'ApiController@customerReceived');
    Route::get('customer-sent-deliveries/{cid}', 'ApiController@customerSent');
    Route::get('customers-updated-details/{cid}','ApiController@customersUpdatedDetails');
    Route::get('customer-payments/{cid}','ApiController@customerPayments');

    Route::get('referred/{cid}','ApiController@referred');
    Route::get('settings/{cid}','ApiController@settings');

    Route::get('favorites/{cid}','ApiController@getFavorites');
    Route::get('customer-locations/{cid}','ApiController@getCustomerLocations');

    Route::post('price','ApiController@calculatePrice');
    Route::post('add-delivery-image','ApiController@addDeliveryImage');
    Route::post('sign-up', 'ApiController@signUp');
    Route::post('social-sign-up', 'ApiController@socialSignUp');
    Route::post('sign-in', 'ApiController@signIn' );
    Route::post('new-delivery', 'ApiController@newDelivery');
    Route::post('cancel-delivery','ApiController@cancelDelivery');
    Route::post('payment-collected','ApiController@paymentCollected');
    Route::post('add-coupon','ApiController@addCoupon');
    Route::post('send-phone-verification','ApiController@sendPhoneVerification');
    Route::post('confirm-phone-verification','ApiController@confirmPhoneVerification');
    Route::post('social-sign-in','ApiController@socialSignIn');
    Route::post('add-favorite','ApiController@addFavorite');
    Route::post('edit-favorite','ApiController@editFavorite');
    Route::post('delete-favorite','ApiController@deleteFavorite');
    Route::post('add-customer-location','ApiController@addCustomerLocation');
    Route::post('change-profile-photo','ApiController@changeProfilePhoto');

    //dropper routes
    Route::get('available-requests','ApiController@availableRequests');
    Route::get('pickedup-delivery/{did}', 'ApiController@pickupDelivery');
    Route::get('droppedoff-delivery/{did}', 'ApiController@dropoffDelivery');
    Route::get('returned-delivery/{did}', 'ApiController@returnDelivery');
    Route::get('dropper-delivery-history/{cid}','ApiController@dropperDeliveryHistory');
    Route::get('vehicle-info/{cid}','ApiController@vehicleInfo'); // dropper current assigned vehicle info
    Route::get('deliveries-in-progress/{cid}','ApiController@deliveriesInProgress');
    Route::get('dropper-location/{cid}','ApiController@getDropperLocation');


    Route::post('accept-delivery', 'ApiController@acceptDelivery');
    Route::post('dropper-location','ApiController@postDropperLocation');

    //updates

    Route::post('update-profile','ApiController@updateUserProfile');
    Route::post('forgot-password','ApiController@forgotPassword');
    Route::post('confirm-verification','ApiController@confirmVerification');
    Route::post('change-password','ApiController@changeUserPassword');


    //vehicle partners
    Route::get('earnings/{cid}','ApiController@earnings');
    Route::get('withdrawals/{cid}','ApiController@withdrawals');

    Route::post('change-payout','ApiController@changePayout');

    //payments
    Route::post('handle-card-payment','ApiController@handleCardPayment');
    Route::post('update-bank-details','ApiController@updateBankDetails');

});