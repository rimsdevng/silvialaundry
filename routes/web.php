<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/','HomeController@index');
Route::get('/logout','HomeController@logout');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//admin
Route::get('/staff', 'AdminController@viewStaff');
Route::get('/staff/{uid}', 'AdminController@viewStaffDetail');
Route::get('/staff/{uid}/edit','AdminController@editStaffDetail');

Route::get('/customer-request','StaffController@customerRequest');


Route::get('/send-notification','AdminController@sendNotification');
Route::get('/sent-notifications','AdminController@sentNotifications');

Route::post('/send-notification','AdminController@postSendNotification');
Route::post('/customer-request','StaffController@postCustomerRequest');

//customers

Route::get('/lookup-customers','StaffController@getLookupCustomers');
Route::get('/view-customer','StaffController@getSelectCustomer');
Route::get('/view-customer/{cid}', 'StaffController@getCustomerDetails');
Route::get('view-delivery-detail/{did}', 'StaffController@getDeliveryDetail');
Route::get('customer-received-deliveries/{cid}', 'StaffController@getCustomerReceived');
Route::get('customer-sent-deliveries/{cid}', 'StaffController@getCustomerSent');


//deliveries

Route::get('lookup-requests', 'StaffController@getRequests');
Route::get('lookup-request/{did}', 'StaffController@requestDetail');

Route::post('/assign-delivery','StaffController@assignDelivery');

Route::post('price-update','StaffController@priceUpdate');
Route::post('/send-updates','StaffController@sendUpdates');









//vehicle partner


Route::get('/add-vehicle','StaffController@getSelectAddVehicle');
Route::get('/add-vehicle-cat', 'StaffController@getAddVehicleCat');
Route::get('/view-vehicle-categories','StaffController@viewVehicleCat');
Route::get('/add-vehicle/{cid}', 'StaffController@getAddVehicle');
Route::get('/lookup-vehicle-partners','StaffController@getLookupVehiclePartners');
Route::get('vehicle-partner/{cid}','StaffController@vehiclePartnerDetails');

Route::get('/add-group-vehicle','StaffController@getSelectAddGroupVehicle');
Route::get('/add-group-vehicle/{gid}','StaffController@getAddGroupVehicle');
Route::get('/add-group-vehicle/{gid}','StaffController@getAddGroupVehicle');

Route::post('add-vehicle', 'StaffController@postVehicle');
Route::post('/add-vehicle-cat', 'StaffController@postVehicleCat');
Route::post('add-vehicle-partner', 'StaffController@postVehiclePartner');
Route::post('post-dropper', 'StaffController@postDropper');


//vehicle info


Route::get('view-vehicles', 'StaffController@getViewVehicles');
Route::get('view-vehicle/{vid}', 'StaffController@getVehicleDetails');
Route::get('edit-vehicle/{vid}', 'StaffController@getEditVehicle');
Route::post('edit-vehicle/{vid}', 'StaffController@postEditVehicle');

Route::post('add-maintenance','StaffController@postAddMaintenance');
Route::post('add-fuel','StaffController@postAddFuel');
Route::post('update-customer-balance','StaffController@postUpdateCustomerBalance');


// groups

Route::get('/create-group','StaffController@getCreateGroup');
Route::get('/manage-group','StaffController@getManageGroup');
Route::get('/manage-group/{gid}','StaffController@getManageSingleGroup');

Route::post('create-group','StaffController@postCreateGroup');
Route::post('add-group-member','StaffController@postAddGroupMember');
Route::post('/add-group-vehicle','StaffController@postAddGroupVehicle');


//dropper
Route::get('/lookup-droppers','StaffController@getLookupDroppers');
Route::get('/dropper/{cid}', 'StaffController@getDropperDetails');
Route::get('/register-dropper','StaffController@getSelectRegisterDropper');
Route::get('/register-dropper/{cid}','StaffController@getRegisterDropper');

Route::post('/add-remittance','StaffController@postAddRemittance');

Route::get('makeDropper/{cid}', 'StaffController@postMakeDropper');

//support
Route::get('/support-portal','StaffController@getSupportPortal');



//settings

Route::get('settings', 'StaffController@settings');
Route::post('post-settings', 'StaffController@postSettings');
Route::get('view-settings', 'StaffController@viewSettings');

Route::get('add-coupon','AdminController@addCoupon');
Route::get('manage-coupons','AdminController@manageCoupons');

Route::get('/confirm-email/{code}/{email}','PublicController@confirmEmail');

Route::post('add-coupon','AdminController@postAddCoupon');

//items cat

Route::get('add-item-cat', 'StaffController@getItemCat');
Route::post('item-cat', 'StaffController@postAddItemCat');
Route::get('view-item-cat', 'StaffController@viewItemCat');
Route::get('edit-item-cat/{icid}', 'StaffController@editItemCat');
Route::post('edit-item-cat','StaffController@postEditItemCat');

//delivery item
Route::get('view-available-deliveries', 'StaffController@getAvailableDeliveries');

Route::get('add-delivery-items', 'StaffController@getDeliveryItem');
Route::post('delivery-items', 'StaffController@postDeliveryItem');
Route::get('view-delivery-items', 'StaffController@viewDeliveryItems');

//admin

Route::get('pay-vehicle-partners','AdminController@payVehiclePartners');
Route::get('staff/delete/{uid}', 'AdminController@deleteStaff');

Route::get('terms','AdminController@terms');
Route::get('add-faq','AdminController@addFAQ');
Route::get('faqs','AdminController@faqs');
Route::get('edit-faq/{fid}','AdminController@editFaq');
Route::get('delete-faq/{fid}','AdminController@deleteFaq');
Route::get('privacy','AdminController@privacyPolicy');
Route::get('vpas','AdminController@vpas');


Route::post('terms','AdminController@postTerms');
Route::post('faqs','AdminController@postFaq');
Route::post('privacy','AdminController@postPrivacyPolicy');
Route::post('vpas','AdminController@postVpas');
Route::post('edit-faq/{fid}','AdminController@postEditFaq');



Route::post('/staff/{uid}/edit','AdminController@updateStaff');
Route::post('view-customer/{cid}', 'AdminController@postVehicle');


//public routes

Route::get('active-terms','PublicController@activeTerms');
Route::get('active-vpas','PublicController@activeVpas');
Route::get('active-privacy','PublicController@activePrivacy');
Route::get('active-faqs','PublicController@activeFaqs');


//dashboard
Route::post('status','HomeController@getStatus');
